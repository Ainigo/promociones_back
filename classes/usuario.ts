

export class Usuario {

    public id: string;
    public nombre: string;
    public pass: string;
    public sala: string;

    constructor( id: string ) { 
        
        this.id = id;
        this.nombre = 'sin-nombre';
        this.pass = '';
        this.sala   = 'sin-sala';

    }

}