import { Usuario } from './usuario';

const ldap = require('activedirectory');
const config = { url: 'ldap://10.92.92.100',
               baseDN: 'DC=aedashomes,DC=local',
               //username: 'apiapppromociones@aedashomes.local',
               //password: 'Aedas2019' 
               username: 'jluna@aedashomes.local',
               password: 'Esquivias2019' 
               };
const ad = new ldap(config);



export class UsuariosLista {

    private lista: Usuario[] = [];


    constructor() { }

    // Agregar un usuario
    public agregar( usuario: Usuario ) {

        this.lista.push( usuario );
        console.log( this.lista );
        return usuario;
        
    }

    public actualizarNombre( id: string, nombre: string ) {

        for( let usuario of this.lista ) {

            if ( usuario.id === id ) {
                usuario.nombre = nombre;
                break;
            }

        }

        console.log('===== Actualizando usuario ====');
        console.log( this.lista );

    }

    // Obtener usuario en una sala en particular
    public setLogin( nombre:string, pass: string ) {
        
        return new Promise ( (resolve,reject) => {
                console.log('2');
                ad.authenticate ( nombre, pass, async (err: any, auth: any) => {
                resolve(auth);                
            });
        });
    }


    // Obtener lista de usuarios
    public getLista() {
        return this.lista.filter( usuario => usuario.nombre !== 'sin-nombre' );
    }

    // Obtener un usuario
    public getUsuario( id: string ) {

        return this.lista.find( usuario => usuario.id === id );

    }

    // Obtener usuario en una sala en particular
    public getUsuariosEnSala( sala: string ) {

        return this.lista.filter( usuario =>usuario.sala === sala );

    }

    // Borrar Usuario
    public borrarUsuario( id: string ) {

        const tempUsuario = this.getUsuario( id );

        this.lista = this.lista.filter( usuario => usuario.id !== id );

        return tempUsuario;
        
    }


}