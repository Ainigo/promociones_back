const authController = require("./controllers/auth");
const categoryController = require("./controllers/category");
const userController = require("./controllers/user");
const studyController = require("./controllers/study");
const masterController = require("./controllers/master");
const secondHandController = require("./controllers/secondHand");
const nextPromotionsController = require("./controllers/nextPromotions");
const sharepointController = require("./controllers/sharepoint");
const promotionsController = require("./controllers/promotions");
const buildingsController = require("./controllers/buildings");
const qualitiesController = require("./controllers/qualities");
const programController = require("./controllers/program");
const financesController = require("./controllers/finances");
const multer = require('multer');
const mkdirp = require('mkdirp');
const config = require("./config/config");


const v1 = "/api/v1";

module.exports.set = app => {
  app.post(v1 + "/login", authController.login);
  app.post(v1 + "/perfil", authController.perfil);
  app.get(v1 + "/perfilUser/:email", authController.perfilUserAPP);
  app.get(v1 + "/pintarTrazas/:emailUsuario", authController.pintarTrazasUser);
  app.get(v1 + "/getcodusersalesforce/:dni", authController.getcodigoUser);

  app.get(v1 + '/auth', authController.authUser);
  app.get(v1 + '/getAToken', authController.getTokenAzure);
  app.get(v1 + '/logout', authController.logout);

  app.get(v1 + '/ip', authController.ip);


  app.get(v1 + "/categorias/:idCategorias", authController.getCategory);
  app.get(v1 + "/productos/:idCategorias", authController.getProducts);

  app.get(v1 + "/allcategorias/", authController.getAllCategories);
  app.get(v1 + "/allusers/", authController.getAllUsers);

  app.get(v1 + "/altaportales/:idusuario", authController.getaltaportales);
  app.get(v1 + "/rutafiletalkdesk/:idTalkdesk", authController.getrutafiletalkdesk);

  app.get(v1 + "/downloadfiletalkdesk/:json", authController.downloadrutafiletalkdesk);

  app.get(v1 + "/mensajesspam/:fecha", authController.getmensajesspam);

  app.get(v1 + "/altaLeads/:idportal", authController.getaltaleads);

  app.post(v1 + "/addusers", authController.addUsers);

  app.post(v1 + "/addusersPortales", authController.addUsersPortales);

  app.post(v1 + "/addproducts", authController.addProducts);

  app.post(v1 + "/addmailing", authController.addMailing);

  app.post(v1 + "/addchanges", authController.addChanges);

  app.post(v1 + "/documentsRemoveBBDD", authController.deleteProducts);

  app.post(v1 + "/updateusers", authController.updateUsers);

  app.post(v1 + "/updatespam", authController.updateSpam);

  app.post(v1 + "/updatesalesforce", authController.updatesalesforce);

  app.post(v1 + "/updateoportunitysalesforce", authController.updateoportunitysalesforce);

  app.post(v1 + "/updatealtaportales", authController.updatealtaportales);

  app.post(v1 + "/updatesalesforceAccount", authController.updatesalesforceAreaPrivada)

  app.get(v1 + "/connectsalesforce/:codigoprinex", authController.connectSalesForceSelects);

  app.get(v1 + "/getcodusersalesforce/:dni", authController.getcodigoUser);

  app.get(v1 + "/getdatosusersalesforce/:id", authController.getdatosUserSalesForce);

  app.get(v1 + "/getIdInmueblessalesforce/:idUser", authController.getcodigoInmueble);

  app.get(v1 + "/getNamePromotion/:idPromotion", authController.getnamePromotion);
  
  app.get(v1 + "/getIdDatosInmueblesalesforce/:idInmueble", authController.getdatosInmueble);

  app.get(v1 + "/getdatosUsuariosOportunidad/:oportunidad", authController.getdatosUsuariosOportunidad);

  app.get(v1 + "/getnamesUsuariosOportunidad/:codUsersOportunidad", authController.getnamesUsuariosOportunidad);

  app.get(v1 + "/getProductCode/:productcode", authController.getProductCode);

  app.get(v1 + "/getsecundariocodigoInmueble/:idUser", authController.getsecundariocodigoInmueble);

  app.get(v1 + "/allproductos/", authController.getAllProductos);


  //app.post(v1 + "/perfil", authController.perfil);
  //app.get(v1 + "/categorias/:idCategorias", authController.getCategory);

  //next endpoints require auth
  //Users Routes
  app.get(v1 + "/studies/:study_id/export", studyController.exportToXLS);

  app.get(v1 + "/users", authController.checkAuth, userController.getUsers);
  app.post(v1 + "/users", authController.checkAuth, authController.register);
  app.get(v1 + "/user/:token", authController.checkAuth, userController.getUserByToken);
  app.get(v1 + "/users/:email", authController.checkAuth, userController.getUser);
  //app.post(v1 + "/users", authController.checkAuth, userController.addUser);
  app.put(v1 + "/users/:user_id", authController.checkAuth, userController.updateUser);
  app.delete(v1 + "/users/:user_id", authController.checkAuth, userController.deleteUser);
  app.get(v1 + "/users/:email/studies", authController.checkAuth, userController.getUsersStudies);
  app.post(v1 + "/users/:user_id/studies", authController.checkAuth, userController.assignUserStudies);
  app.delete(v1 + "/users/:user_id/studies/:study_id", authController.checkAuth, userController.unAssignUserStudies);


  //Basic Data Routes
  app.get(v1 + "/basic_data/:type_value", authController.checkAuth, masterController.getBasicDataByTypeValue);
  
  //Config Routes
  app.get(v1 + "/config/first_levels", authController.checkAuth, masterController.getConfigFirstLevels);
  app.get(v1 + "/config/data_types", authController.checkAuth, masterController.getConfigDataTypes);
  app.post(v1 + "/config/data_types", authController.checkAuth, masterController.createConfigDataTypes);
  app.put(v1 + "/config/data_types", authController.checkAuth, masterController.updateConfigDataTypes);
  app.get(v1 + "/config/data_values/:type_id", authController.checkAuth, masterController.getConfigDataValuesByTypeId);
  app.post(v1 + "/config/data_values", authController.checkAuth, masterController.createConfigDataValues);
  app.put(v1 + "/config/data_values/:value_id", authController.checkAuth, masterController.updateConfigDataValues);
  
  //Studies Routes
  app.get(v1 + "/studies", authController.checkAuth, studyController.getStudiesByToken);
  app.get(v1 + "/studies/:study_id", authController.checkAuth, studyController.getStudyById);
  app.get(v1 + "/export/:study_id", authController.checkAuth, studyController.exportToXLS);
  app.post(v1 + "/studies", authController.checkAuth, studyController.createStudy);
  app.put(v1 + "/studies/:study_id", authController.checkAuth, studyController.updateStudy);
  app.delete(v1 + "/studies/:study_id", authController.checkAuth, studyController.deleteStudy);

  //VersionsStudy Routes
  app.get(v1 + "/studies/:study_id/versions", authController.checkAuth, studyController.getVersionsStudy);
  app.put(v1 + "/studies/changeStudyStatus/:study_id", authController.checkAuth, studyController.changeStudyStatus);
  
  //Promotions Routes
  app.get(v1 + "/promotions", authController.checkAuth, promotionsController.getPromotions);
  app.get(v1 + "/promotions/:promotion_id", authController.checkAuth, promotionsController.getPromotion);
  app.post(v1 + "/promotions", authController.checkAuth, promotionsController.createPromotion);
  app.put(v1 + "/promotions/:promotion_id", authController.checkAuth, promotionsController.updatePromotion);
  app.delete(v1 + "/promotions/:promotion_id", authController.checkAuth, promotionsController.deletePromotion);

  //Program Routes
  app.get(v1 + "/promotions/:promotion_id/program", authController.checkAuth, programController.getProgram);
  app.put(v1 + "/promotions/:promotion_id/program", authController.checkAuth, programController.insertOrUpdatePrograms);
  
  //Buildings Routes
  app.get(v1 + "/promotions/:promotion_id/buildings", authController.checkAuth, buildingsController.getBuildings);
  app.get(v1 + "/promotions/:promotion_id/buildings/:building_id", authController.checkAuth, buildingsController.getBuilding);
  app.post(v1 + "/promotions/:promotion_id/buildings", authController.checkAuth, buildingsController.createBuilding);
  app.put(v1 + "/promotions/:promotion_id/buildings/:building_id", authController.checkAuth, buildingsController.updateBuilding);
  app.delete(v1 + "/promotions/:promotion_id/buildings/:building_id", authController.checkAuth, buildingsController.deleteBuilding);

  //Finances Routes
  app.get(v1 + "/promotions/:promotion_id/finances", authController.checkAuth, financesController.getFinance);
  app.post(v1 + "/promotions/:promotion_id/finances", authController.checkAuth, financesController.createFinance);
  app.put(v1 + "/promotions/:promotion_id/finances", authController.checkAuth, financesController.updateFinance);

  //Qualities Routes
  app.get(v1 + "/promotions/:promotion_id/qualities", authController.checkAuth, qualitiesController.getQualities);
  app.post(v1 + "/promotions/:promotion_id/qualities", authController.checkAuth, qualitiesController.insertOrUpdateQualities);
  app.put(v1 + "/promotions/:promotion_id/qualities", authController.checkAuth, qualitiesController.insertOrUpdateQualities);
    
  //SecondHandRent Routes
  app.get(v1 + "/second_hand_rent", authController.checkAuth, secondHandController.getAllSecondHandRent);
  app.get(v1 + "/second_hand_rent/:shr_id", authController.checkAuth, secondHandController.getSecondHandRent);
  app.post(v1 + "/second_hand_rent", authController.checkAuth, secondHandController.createSecondHandRent);
  app.put(v1 + "/second_hand_rent/:shr_id", authController.checkAuth, secondHandController.updateSecondHandRent);
  app.delete(v1 + "/second_hand_rent/:shr_id", authController.checkAuth, secondHandController.deleteSecondHandRent);

  //NextPromotions Routes
  app.get(v1 + "/next_promotions", authController.checkAuth, nextPromotionsController.getNextPromotions);
  app.get(v1 + "/next_promotions/:next_promotion_id", authController.checkAuth, nextPromotionsController.getNextPromotion);
  app.post(v1 + "/next_promotions", authController.checkAuth, nextPromotionsController.createNextPromotion);
  app.delete(v1 + "/next_promotions/:next_promotion_id", authController.checkAuth, nextPromotionsController.deleteNextPromotion);

  //GETPromotions
  app.get(v1 + "/getPromocionesSalesForce", authController.getpromotionSalesForce);
  app.get(v1 + "/getOportunidadesGanada/:idPromocion", authController.getIDoportunidadesganadas);
  app.get(v1 + "/getOportunidadesByUser/:accountId", authController.getoportunidadesbyuser);
  app.get(v1 + "/getencuestasOportunidad/:accountId/:oportinidadId", authController.getencuestasOportunidad);
  
  app.get(v1 + "/getEmailsSalesForce/:accountId", authController.getEmailsoportunidadesganadas);

  app.post(v1 + "/sendEmail", authController.postEmails);

  app.get(v1 + "/getEmailsUsuarios/:accountId", authController.getEmailUsuariossoportunidadesganadas);
  app.get(v1 + "/getAllOportunidadesGanadas", authController.getAlloportunidadesganadasSalesForce);
  app.get(v1 + "/getAdminAreaPrivada/:email", authController.altaCurl);
  app.get(v1 + "/getCreateLead/:json", authController.createLead);

  app.get(v1 + "/getIdSalesForce/:id", authController.getIdoportunidadesganadas);
  app.get(v1 + "/getcheckEmail/:email", authController.checkEmail);

  app.get(v1 + "/getTokenUser/:token", authController.getUserToken);

  app.get(v1 + "/getUsersSalesForce/:json", authController.getUsersSalesForce);




  
  
  var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const dir = config.sharepoint.urlLocal;
        mkdirp(dir, err => cb(err, dir))
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
  });
  const upload = multer({ storage });

  //var upload = multer({ dest: config.sharepoint.urlLocal});

  //competencia futura
  app.get(v1 + "/documents/:study_id/next_promotions", authController.checkAuth, sharepointController.getNextPromotionsDocuments);
  app.post(v1 + "/documents/:study_id/next_promotions", authController.checkAuth, upload.single('image'), sharepointController.uploadNextPromotionDocument);  

  // Documentación de promociones
  app.get(v1 + "/documents/:study_id/promotions/:promotion_id", authController.checkAuth, sharepointController.getPromotionDocuments);
  app.post(v1 + "/documents/:study_id/promotions/:promotion_id", authController.checkAuth, upload.single('image'), sharepointController.uploadPromotionDocument);  

  // Documentación de living
  app.post(v1 + "/documentsUpload/", upload.single('image'), sharepointController.uploadDocumentFile);
  
  app.post(v1 + "/documentsRemove", sharepointController.removeDocumentFile); 

};
