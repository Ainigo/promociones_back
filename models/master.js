const Sequelize = require("sequelize");
const sequelize = require("../db/db");


// Modelo de las tablas maestras cnt_ y qua_
const FirstLevels = sequelize.define(
  "qua_first_levels",
  {
    first_level_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    value: { type: Sequelize.STRING, allowNull: false },
    label: { type: Sequelize.STRING, allowNull: false },
    enabled: { type: Sequelize.BOOLEAN, allowNull: false }
  },
  {
    timestamps: false
  }
);

const Types = sequelize.define(
  "cnt_types",
  {
    type_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    type_value: { type: Sequelize.STRING, allowNull: false },
    type_label: { type: Sequelize.STRING, allowNull: false },
    first_level_id: {
      type: Sequelize.INTEGER,
      references: {
        model: FirstLevels,
        key: "first_level_id"
      }
    },
    enabled: { type: Sequelize.BOOLEAN, allowNull: false },
    data_type: { type: Sequelize.INTEGER, allowNull: false }
  },
  {
    timestamps: false
  }
);

const Values = sequelize.define(
  "cnt_values",
  {
    value_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    type_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: Types,
        key: "type_id"
      }
    },
    value: { type: Sequelize.STRING, allowNull: false },
    label: { type: Sequelize.STRING, allowNull: false },
    enabled: { type: Sequelize.BOOLEAN, allowNull: false }
  },
  {
    timestamps: false
  }
);


FirstLevels.hasMany(Types, { foreignKey: "first_level_id" });
Types.belongsTo(FirstLevels, { foreignKey: "first_level_id" });

Types.hasMany(Values, { foreignKey: "type_id" });
Values.belongsTo(Types, { foreignKey: "type_id" });

module.exports = {
  Types,
  Values,
  FirstLevels
};
