const sequelize = require("../db/db");

const logger = require("../config/logger");
const attrListToEncript = ['promoter','name','address','coordinates','architect','marketer','web','telephone','url'];

module.exports = {

    encriptData(req){        
        
        if (req.method != 'POST' && req.method != 'PUT'){
            // Solo hago la encriptación para PUT y POST
            return;
        }

        var keys = Object.keys(req.body);
        for (var i = 0; i< keys.length;i++){            
            if (attrListToEncript.includes(keys[i])){
                // El atributo está en la lista de atributos a encriptar
                req.body[keys[i]] = sequelize.fn("PGP_SYM_ENCRYPT", req.body[keys[i]], "AES_KEY");
            }
        }
        logger.debug('Datos encriptados');
    }

};
