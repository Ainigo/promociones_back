const Sequelize = require("sequelize");
const sequelize = require("../db/db");
const valuesModel = require("./master").Values;
const typesModel = require("./master").Types;
const promotionModel = require("./studies").Promotions;
const versionModel = require("./studies").VersionsStudy;

// Modelo de la tablas pro_

const PromotionVersions = sequelize.define(
    "pro_promotion_versions",
    {
        work_status_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: valuesModel,
                key: "value_id"
            }
        },
        delivery_date: {type: Sequelize.DATE, allowNull: false},
        promotion_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: promotionModel,
                key: "promotion_id"
            }
        },
        version_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: versionModel,
                key: "version_id"
            }
        }
    },
    {
        schema: "public"
    }
);

const Buildings = sequelize.define(
    "pro_buildings",
    {
        building_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            unique: true,
            primaryKey: true,
            autoIncrement: true
        },
        rooms: {type: Sequelize.INTEGER, allowNull: false},
        bathrooms: {type: Sequelize.INTEGER, allowNull: false},
        terrace_area: {type: Sequelize.NUMERIC, allowNull: false},
        pvp_commercial: {type: Sequelize.INTEGER, allowNull: false},
        status_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: valuesModel,
                key: "value_id"
            }
        },
        promotion_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: promotionModel,
                key: "promotion_id"
            }
        },
        building_type_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: valuesModel,
                key: "value_id"
            }
        },
        version_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: versionModel,
                key: "version_id"
            }
        },
        observations: Sequelize.STRING
    },
    {
        schema: "public"
    }
);

const BuildingSingle = sequelize.define(
    "pro_building_single",
    {
        plot_area: {type: Sequelize.NUMERIC, allowNull: false},
        sbr: {type: Sequelize.NUMERIC, allowNull: false},
        spb: {type: Sequelize.NUMERIC, allowNull: false},
        spp: {type: Sequelize.NUMERIC, allowNull: false},
        sbc: {type: Sequelize.NUMERIC, allowNull: false},
        sbr_constructed: {type: Sequelize.NUMERIC, allowNull: false},
        spb_constructed: {type: Sequelize.NUMERIC, allowNull: false},
        spp_constructed: {type: Sequelize.NUMERIC, allowNull: false},
        sbc_constructed: {type: Sequelize.NUMERIC, allowNull: false},
        single_type_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: valuesModel,
                key: "value_id"
            }
        },
        building_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: Buildings,
                key: "building_id"
            }
        },
        version_id: {  
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: Buildings,
                key: "version_id"
            }
        }
    },
    {
        schema: "public",
        timestamps: false
    }
);

const BuildingMulti = sequelize.define(
    "pro_building_multi",
    {
        floor_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: valuesModel,
                key: "value_id"
            }
        },
        util_area: {type: Sequelize.NUMERIC, allowNull: true},
        constructed_area: {type: Sequelize.NUMERIC, allowNull: true},
        garage: {type: Sequelize.BOOLEAN, allowNull: false},
        garages_number: Sequelize.INTEGER,
        pvp_garage: Sequelize.NUMERIC,
        storage: {type: Sequelize.BOOLEAN, allowNull: false},
        pvp_storage: Sequelize.NUMERIC,
        building_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: Buildings,
                key: "building_id"
            }
        },
        version_id: {  
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: Buildings,
                key: "version_id"
            }
        }
    },
    {
        schema: "public",
        timestamps: false
    }
);

const Finances = sequelize.define(
    "pro_finances",
    {
        banc: Sequelize.STRING,
        reserve: Sequelize.NUMERIC,
        on_sign: Sequelize.INTEGER,
        postponed_during_construction: Sequelize.INTEGER,
        additional_payment: Sequelize.NUMERIC,
        mortgage: Sequelize.INTEGER,
        endorse: Sequelize.BOOLEAN,
        deferment_payments_id:  {
            type: Sequelize.INTEGER,
            references: {
                model: valuesModel,
                key: "value_id"
            }
        },
        promotion_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: promotionModel,
                key: "promotion_id"
            }
        }
    },
    {
        schema: "public"
    }
);

const Program = sequelize.define(
    "pro_program",
    {
        program_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            unique: true,
            primaryKey: true,
            autoIncrement: true
        },
        initial: Sequelize.INTEGER,
        stock: Sequelize.INTEGER,
        rooms: { type: Sequelize.INTEGER, allowNull: false },
        version_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: versionModel,
                key: "version_id"
            }
        },
        promotion_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: promotionModel,
                key: "promotion_id"
            }
        }
    },
    {
        schema: "public"
    }
);

const Qualities = sequelize.define(
    "pro_qualities",
    {
        promotion_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: promotionModel,
                key: "promotion_id"
            }
        },
        cnt_quality_value_id: {
            type: Sequelize.INTEGER,
            references: {
                model: valuesModel,
                key: "value_id"
            }
        },
        cnt_quality_type_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
                model: typesModel,
                key: "type_id"
            }
        },
        value: Sequelize.BOOLEAN
    },
    {
        schema: "public",
        timestamps: false
    }
);

const RoomsNumber = sequelize.define(
    "pro_rooms_number",
    {
        room_number: { type: Sequelize.INTEGER, allowNull: false },
        room_label: { type: Sequelize.STRING, allowNull: false }
    },
    {
        schema: "public",
        timestamps: false
    }
);
RoomsNumber.removeAttribute("id");

// PromotionVersions Relationships
valuesModel.hasMany(PromotionVersions, {foreignKey: "value_id"});
PromotionVersions.belongsTo(valuesModel, {as: "work_status", foreignKey: "work_status_id"});

promotionModel.hasMany(PromotionVersions, {foreignKey: "promotion_id"});
PromotionVersions.belongsTo(promotionModel, {foreignKey: "promotion_id"});

versionModel.hasMany(PromotionVersions, {foreignKey: "version_id"});
PromotionVersions.belongsTo(versionModel, {foreignKey: "version_id"});

// Buildings Relationships
Buildings.belongsTo(valuesModel, {as: "status", foreignKey: "status_id"});
Buildings.belongsTo(valuesModel, {as: "building_type", foreignKey: "building_type_id"});

promotionModel.hasMany(Buildings, {foreignKey: "promotion_id"});
Buildings.belongsTo(promotionModel, {foreignKey: "promotion_id"});

versionModel.hasMany(Buildings, {foreignKey: "version_id"});
Buildings.belongsTo(versionModel, {foreignKey: "version_id"});

// BuildingSingle Relationships
BuildingSingle.belongsTo(valuesModel, {as: "single_type", foreignKey: "single_type_id"});

Buildings.belongsTo(BuildingSingle, {foreignKey: "building_id"});

// BuildingMulti Relationships
BuildingMulti.belongsTo(valuesModel, {as: "floor", foreignKey: "floor_id"})

Buildings.belongsTo(BuildingMulti, {foreignKey: "building_id"});

// Finances Relationships
promotionModel.hasOne(Finances, {foreignKey: "promotion_id"});

// Program Relationships
promotionModel.hasMany(Program, {foreignKey: "promotion_id"});
Program.belongsTo(promotionModel, {foreignKey: "promotion_id"});

versionModel.hasMany(Program, {foreignKey: "version_id"});
Program.belongsTo(versionModel, {foreignKey: "version_id"});

// Qualities Relationships
promotionModel.hasMany(Qualities, {foreignKey: "promotion_id"});
Qualities.belongsTo(promotionModel, {foreignKey: "promotion_id"});

typesModel.hasOne(Qualities, {foreignKey: "cnt_quality_type_id"});
Qualities.belongsTo(typesModel, {foreignKey: "cnt_quality_type_id"});

Qualities.belongsTo(valuesModel, {foreignKey: "cnt_quality_value_id"});


Finances.belongsTo(valuesModel, {as: "deferment_payments", foreignKey: "deferment_payments_id"});

//RoomsNumber Relationships
RoomsNumber.belongsTo(Program, {foreignKey: "room_number"});

module.exports = {
    PromotionVersions,
    Buildings,
    BuildingSingle,
    BuildingMulti,
    Finances,
    Program,
    Qualities,
    RoomsNumber
}