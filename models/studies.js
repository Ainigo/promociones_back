const Sequelize = require("sequelize");
const sequelize = require("../db/db");
const valuesModel = require("./master").Values;
const studyUsers = require("./index").StudyUsers;

// Modelo de las tablas stu_

const MarketStudies = sequelize.define(
  "stu_market_studies",
  {
    study_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    code: { type: Sequelize.STRING, allowNull: false, unique: true },
    city: Sequelize.STRING,
    zone: Sequelize.STRING,
    status_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    region_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    territorial_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    enabled: {type: Sequelize.BOOLEAN, allowNull: false}
  },
  {
    schema: "public"
  }
);

const Promotions = sequelize.define(
  "stu_promotions",
  {
    promotion_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    promotion_type_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    protection_type_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    promoter: { type: Sequelize.STRING, allowNull: false },
    name: { type: Sequelize.STRING, allowNull: false },
    environment_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    address: Sequelize.STRING,
    coordinates: Sequelize.STRING,
    marketer: Sequelize.STRING,
    sales_start_date: { type: Sequelize.DATE, allowNull: false },
    web: Sequelize.STRING,
    telephone: Sequelize.STRING,
    observations: Sequelize.STRING,
    architect: Sequelize.STRING,
    comparable: Sequelize.BOOLEAN,
    study_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: MarketStudies,
        key: "study_id"
      }
    }
  },
  {
    schema: "public"
  }
);

const NextPromotions = sequelize.define(
  "stu_next_promotions",
  {
    next_promotion_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    promo_type_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    promoter: { type: Sequelize.STRING, allowNull: false },
    name: { type: Sequelize.STRING, allowNull: false },
    environment_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    address: Sequelize.STRING,
    coordinates: Sequelize.STRING,
    study_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: MarketStudies,
        key: "study_id"
      }
    }
  },
  {
    schema: "public"
  }
);

const SecondHand = sequelize.define(
  "stu_second_hand_rent",
  {
    study_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: MarketStudies,
        key: "study_id"
      }
    },
    shr_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    transaction_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    address: Sequelize.STRING,
    coordinates: Sequelize.STRING,
    year: Sequelize.INTEGER,
    second_hand_status_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    floor: { type: Sequelize.INTEGER, allowNull: false },
    rooms: { type: Sequelize.INTEGER, allowNull: false },
    bathrooms: { type: Sequelize.INTEGER, allowNull: false },
    area: { type: Sequelize.NUMERIC, allowNull: false },
    price: { type: Sequelize.NUMERIC, allowNull: false },
    commercial_status_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    url: { type: Sequelize.STRING, allowNull: false },
    comments: Sequelize.STRING,
    registry: Sequelize.STRING
  },
  {
    schema: "public"
  }
);

const VersionsStudy = sequelize.define(
  "stu_versions",
  {
    version_id: { 
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    version_number: {type: Sequelize.INTEGER, allowNull: false},
    study_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: MarketStudies,
        key: "study_id"
      }
    }
  },
  {
    schema: "public"
  }
)

// Vista de estudios de mercado por token de usuario
const VUsersStudies = sequelize.define(
  "v_user_studies",
  {
    token: Sequelize.STRING,
    user_id: { type: Sequelize.INTEGER, primaryKey: true },
    role_id: Sequelize.INTEGER,
    study_id: { type: Sequelize.INTEGER, primaryKey: true },
    code: Sequelize.STRING,
    city: Sequelize.STRING,
    zone: Sequelize.STRING,
    status_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    region_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    },
    enabled: Sequelize.BOOLEAN
  },
  {
    schema: "public"
  }
);

// VUsersStudies Relationships
VUsersStudies.belongsTo(valuesModel, { as: "status", foreignKey: "status_id" });
VUsersStudies.belongsTo(valuesModel, { as: "region", foreignKey: "region_id" });

// MarketStudies Relationships
MarketStudies.belongsTo(valuesModel, { as: "status", foreignKey: "status_id" });
MarketStudies.belongsTo(valuesModel, { as: "region", foreignKey: "region_id" });
MarketStudies.belongsTo(valuesModel, { as: "territorial", foreignKey: "territorial_id" });


// SecondHandRent Relationships
SecondHand.belongsTo(valuesModel, { as: "transaction", foreignKey: "transaction_id"});
SecondHand.belongsTo(valuesModel, { as: "second_hand_status", foreignKey: "second_hand_status_id"});
SecondHand.belongsTo(valuesModel, { as: "commercial_status", foreignKey: "commercial_status_id"});

MarketStudies.hasMany(SecondHand, { foreignKey: "study_id" });
SecondHand.belongsTo(MarketStudies, { foreignKey: "study_id" });

// NextPromotions Relationships
NextPromotions.belongsTo(valuesModel, {as: "promo_type", foreignKey: "promo_type_id"});
NextPromotions.belongsTo(valuesModel, {as: "environment", foreignKey: "environment_id"});

MarketStudies.hasMany(NextPromotions, { foreignKey: "study_id" });
NextPromotions.belongsTo(MarketStudies, { foreignKey: "study_id" });

// Promotions Relationships
Promotions.belongsTo(valuesModel, {as: "promo_type", foreignKey: "promotion_type_id"});
Promotions.belongsTo(valuesModel, {as: "environment", foreignKey: "environment_id"});
Promotions.belongsTo(valuesModel, {as: "protection_type", foreignKey: "protection_type_id"});

MarketStudies.hasMany(Promotions, { foreignKey: "study_id" });
Promotions.belongsTo(MarketStudies, { foreignKey: "study_id" });

// VersionsStudy Relationships
MarketStudies.hasMany(VersionsStudy, {foreignKey: "study_id"});
VersionsStudy.belongsTo(MarketStudies, {foreignKey: "study_id"});

module.exports = {
  MarketStudies,
  Promotions,
  NextPromotions,
  SecondHand,
  VersionsStudy,
  VUsersStudies
};
