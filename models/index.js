const Sequelize = require("sequelize");
const sequelize = require("../db/db");
const marketModel = require("./studies").MarketStudies;
const valuesModel = require("./master").Values;

const Rols = sequelize.define(
  "usr_rols",
  {
    role_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    role_name: Sequelize.STRING
  },
  {
    timestamps: false
  }
);

const Users = sequelize.define(
  "usr_users",
  {
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    email: { type: Sequelize.STRING, allowNull: false, unique: true },
    valid: Sequelize.BOOLEAN,
    name: { type: Sequelize.STRING, allowNull: false, unique: false },
    last_login: { type: Sequelize.DATE, allowNull: true, unique: false },

    // It is possible to create foreign keys:
    role_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        // This is a reference to another model
        model: Rols,
        // This is the column name of the referenced model
        key: "role_id",
        // This declares when to check the foreign key constraint. PostgreSQL only.
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
      }
    },
    territorial_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: valuesModel,
        key: "value_id"
      }
    }
  },
  {
    schema: "public"
  }
);

const UsrTokens = sequelize.define(
  "usr_tokens",
  {
    token_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },
    token: { type: Sequelize.STRING, allowNull: false, unique: true },
    user_id: {
      type: Sequelize.INTEGER,
      references: {
        model: Users,
        key: "user_id"
      }
    },
    expires_on:{type: Sequelize.DATE, allowNull: false, unique: false }
  },
  {
    timestamps: false
  }
);


const StudyUsers = sequelize.define(
  "stu_market_users",
  {
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true      
    },
    study_id:{
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true ,
      references: {
        model: marketModel,
        key: "study_id"
      }     
    },
    created_on: { 
      type: Sequelize.DATE, 
      allowNull: false, 
      unique: false },
  },
  {
    timestamps: false
  }
);

Users.hasMany(UsrTokens, { foreignKey: "user_id" });
UsrTokens.belongsTo(Users, { foreignKey: "user_id" });

Rols.hasMany(Users, { foreignKey: "role_id" });
Users.belongsTo(Rols, { foreignKey: "role_id" });

StudyUsers.hasMany(Users, { foreignKey: "user_id" });
StudyUsers.hasMany(marketModel, { foreignKey: "study_id" });
marketModel.hasMany(StudyUsers, { foreignKey: "study_id" });

Users.belongsTo(valuesModel, { as: "territorial", foreignKey: "territorial_id" });


module.exports = {
  Users,
  Rols,
  UsrTokens,
  StudyUsers
  //,	Order
};
