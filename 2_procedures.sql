
CREATE OR REPLACE PROCEDURE public.delete_building(
	p_building_id integer)
LANGUAGE 'plpgsql'

AS $BODY$
DECLARE
  p_new_version_id integer;
  p_default_version_id integer;

BEGIN

    -- delete pro_building_single 
    delete from pro_building_single where building_id = p_building_id; 
    RAISE NOTICE 'pro_building_single deleted';

    -- delete pro_building_multi
    delete from pro_building_multi where building_id = p_building_id;                                        

    -- delete pro_buildings
    delete from pro_buildings where building_id = p_building_id;                                        
    RAISE NOTICE 'pro_buildins deleted';

    exception 
    when others then        
        rollback;
        RAISE INFO 'Error Name: %',SQLERRM; 

END;
$BODY$;



-- PROCEDURE: public.delete_promotion(integer)

-- DROP PROCEDURE public.delete_promotion(integer);

CREATE OR REPLACE PROCEDURE public.delete_promotion(
	p_promotion_id integer)
LANGUAGE 'plpgsql'

AS $BODY$
DECLARE
  p_new_version_id integer;
  p_default_version_id integer;

BEGIN

    -- delete pro_building_single 
    delete from pro_building_single 
        where building_id in (select bld.building_id 
                                        from pro_buildings bld
                                        where bld.promotion_id = p_promotion_id); 
    RAISE NOTICE 'pro_building_single deleted';

    -- delete pro_building_multi
    delete from pro_building_multi
        where building_id in (select bld.building_id 
                                        from pro_buildings bld
                                        where bld.promotion_id = p_promotion_id);                                        

    -- delete pro_buildings
    delete from pro_buildings where promotion_id = p_promotion_id;    
    RAISE NOTICE 'pro_buildins deleted';

    -- delete pro_promotion_versions
    delete from pro_promotion_versions where promotion_id = p_promotion_id;  

    -- delete pro_qualities
    delete from pro_qualities qua
        where qua.promotion_id = p_promotion_id;     

    -- delete pro_promotion_versions
    delete from stu_promotions where promotion_id = p_promotion_id;  
    RAISE NOTICE 'stu_promotions deleted';

    -- delete pro_finances
    delete from pro_finances where promotion_id = p_promotion_id; 

    delete from pro_qualities where promotion_id = p_promotion_id; 

    exception 
    when others then        
        rollback;
        RAISE INFO 'Error Name: %',SQLERRM; 

END;
$BODY$;


-- PROCEDURE: public.delete_study(integer)

-- DROP PROCEDURE public.delete_study(integer);

CREATE OR REPLACE PROCEDURE public.delete_study(
	p_study_id integer)
LANGUAGE 'plpgsql'

AS $BODY$

BEGIN

    -- delete pro_building_single 
    delete from pro_building_single 
        where building_id in (select bld.building_id 
                                        from stu_promotions pro, pro_buildings bld
                                        where pro.promotion_id = bld.promotion_id and
                                            pro.study_id = p_study_id);

    -- delete pro_building_multi
    delete from pro_building_multi
        where building_id in (select bld.building_id 
                                        from stu_promotions pro, pro_buildings bld
                                        where pro.promotion_id = bld.promotion_id and
                                            pro.study_id = p_study_id);    
    RAISE NOTICE 'pro_building_multi deleted';                                        

    -- delete pro_buildings
    delete from pro_buildings
        where promotion_id in (select promotion_id 
                                        from stu_promotions 
                                        where study_id = p_study_id);   
    RAISE NOTICE 'pro_buildings deleted';                               

    -- delete pro_promotion_versions
    delete from pro_promotion_versions
        where promotion_id in (select promotion_id 
                                        from stu_promotions 
                                        where study_id = p_study_id);  
    RAISE NOTICE 'pro_promotion_versions deleted';                             

    -- delete pro_finances
    delete from pro_finances
        where promotion_id in (select promotion_id 
                                        from stu_promotions 
                                        where study_id = p_study_id);       
    RAISE NOTICE 'pro_finances deleted';

										
    -- delete pro_program
    delete from pro_program
        where promotion_id in (select promotion_id 
                                        from stu_promotions 
                                        where study_id = p_study_id);          
    RAISE NOTICE 'pro_program deleted';

    -- delete pro_qualities
    delete from pro_qualities qua
        where qua.promotion_id in (select promotion_id 
                                        from stu_promotions 
                                        where study_id = p_study_id);       
    RAISE NOTICE 'pro_qualities deleted';

    -- All promotions has been deleted.
    delete from stu_promotions where study_id = p_study_id;  

    delete from stu_second_hand_rent where study_id = p_study_id;  

    delete from stu_next_promotions where study_id = p_study_id;  

    delete from stu_versions where study_id = p_study_id;  

    delete from stu_market_users where study_id = p_study_id;  

    delete from stu_market_studies where study_id = p_study_id;  

END;
$BODY$;


-- PROCEDURE: public.delete_user(integer)

-- DROP PROCEDURE public.delete_user(integer);

CREATE OR REPLACE PROCEDURE public.delete_user(
	in_user_id integer)
LANGUAGE 'plpgsql'

AS $BODY$BEGIN
	DELETE FROM usr_tokens as t 
	WHERE in_user_id = t.user_id;
	RAISE NOTICE 'Delete user tokens';
	
	DELETE FROM stu_market_users as m
	WHERE in_user_id = m.user_id;
	RAISE NOTICE 'Delete user markets';
	
	DELETE FROM usr_users as u
	WHERE in_user_id = u.user_id;
	RAISE NOTICE 'Delete user';
	
	EXCEPTION 
	WHEN OTHERS THEN
		ROLLBACK;
		RAISE INFO 'ERROR %', SQLERRM;
END;$BODY$;



-- PROCEDURE: public.new_study_version(integer)

-- DROP PROCEDURE public.new_study_version(integer);

CREATE OR REPLACE PROCEDURE public.new_study_version(
	p_study_id integer)
LANGUAGE 'plpgsql'

AS $BODY$DECLARE
  p_new_version_id integer;
  p_default_version_id integer;

BEGIN
--    START TRANSACTION;

    select version_id 
        into p_default_version_id
        from stu_versions 
        where version_number = 0 and study_id = p_study_id;
    RAISE NOTICE 'Version_id to copy %', p_default_version_id;

    insert into stu_versions( created_at, version_number, study_id) 
        values (now(), (select max(version_number)+1 from stu_versions where study_id = p_study_id), p_study_id)
        RETURNING version_id
        INTO p_new_version_id;

    RAISE NOTICE 'New version_id %', p_new_version_id;

    -- creo los nuevos programas
    insert into pro_program (program_id, initial, stock, rooms, version_id, promotion_id, created_at)
        select pro.program_id, pro.initial, pro.stock, pro.rooms,p_new_version_id, pro.promotion_id, now()   
            from pro_program pro
            where pro.version_id = p_default_version_id and
            promotion_id in (select promotion_id 
                                        from stu_promotions 
                                        where study_id = p_study_id and 
                                            version_id = p_default_version_id);        
    RAISE NOTICE 'pro_program created';

    -- creao las pro_promotion_versions
    insert into pro_promotion_versions(work_status_id, delivery_date, promotion_id, version_id, created_at)
        select ppv.work_status_id, ppv.delivery_date, ppv.promotion_id, p_new_version_id, now()
            from pro_promotion_versions ppv
            where ppv.version_id = p_default_version_id and
            promotion_id in (select promotion_id 
                                        from stu_promotions 
                                        where study_id = p_study_id and 
                                            version_id = p_default_version_id);
    RAISE NOTICE 'pro_promotion_versions created';

    -- creo los pro_buildings
    insert into pro_buildings (building_id, rooms, bathrooms, terrace_area, pvp_commercial, status_id, promotion_id, building_type_id, version_id, created_at)
        select bld.building_id, bld.rooms, bld.bathrooms, bld.terrace_area, bld.pvp_commercial, bld.status_id, bld.promotion_id, bld.building_type_id, p_new_version_id, now()
            from pro_buildings bld
            where bld.version_id = p_default_version_id and
            bld.promotion_id in (select promotion_id 
                                        from stu_promotions 
                                        where study_id = p_study_id and 
                                            version_id = p_default_version_id);
    RAISE NOTICE 'pro_buildings created';

    -- creo los pro_buildings_multi
    insert into pro_building_multi (floor_id, util_area, constructed_area, garage, garages_number, pvp_garage, storage, pvp_storage, building_id, version_id)
        select pbm.floor_id, pbm.util_area, pbm.constructed_area, pbm.garage, pbm.garages_number, pbm.pvp_garage, pbm.storage, pbm.pvp_storage, pbm.building_id, p_new_version_id
            from pro_building_multi pbm
            where pbm.version_id = p_default_version_id and
                pbm.building_id in (select bld.building_id 
                                        from stu_promotions pro, pro_buildings bld
                                        where pro.promotion_id = bld.promotion_id and
                                            pro.study_id = p_study_id and bld.version_id = p_default_version_id);
    RAISE NOTICE 'pro_building_multi created';

    -- creo los pro_buildings_simple
    insert into pro_building_single (plot_area, sbr, spb, spp, sbc, sbr_constructed, spb_constructed, spp_constructed, sbc_constructed, single_type_id, building_id, version_id)
        select pbs.plot_area, pbs.sbr, pbs.spb, pbs.spp, pbs.sbc, pbs.sbr_constructed, pbs.spb_constructed, pbs.spp_constructed, pbs.sbc_constructed, pbs.single_type_id, pbs.building_id, p_new_version_id
            from pro_building_single pbs
            where pbs.version_id = p_default_version_id and
                pbs.building_id in (select bld.building_id 
                                        from stu_promotions pro, pro_buildings bld
                                        where pro.promotion_id = bld.promotion_id and
                                            pro.study_id = p_study_id and bld.version_id = p_default_version_id);
    RAISE NOTICE 'pro_building_single created';
	update stu_market_studies set status_id = (select value_id from cnt_values where type_id =12 and value = 'abierto')
		where study_id = p_study_id;

    exception 
    when others then        
        rollback;
        RAISE INFO 'Error Name: %',SQLERRM;

END;
$BODY$;
