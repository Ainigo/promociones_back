
function isNullOrUndefined(param){
    return (param == null || param ==undefined);    
}

function isBlankOrNull(param){
    return (param == null || param == undefined || param === '' );
}

/**
 * Funcion que, para un objeto comprueba si tiene un parametro (param) informado.
 * En caso de que lo esté, lo añade al objeto values.
 */
function addIfNotNull(values, object, param){
    if (!isNullOrUndefined(object[param])){
        values[param] = object[param];
    }
}

module.exports = {
    isNullOrUndefined,
    isBlankOrNull,
    addIfNotNull
}