const config = require("./../config/config");

function sendNotifications(users, subject, text) {

    const mailService = require("../services/mail");

    const message = {
        from: process.env.EMAIL_ACCOUNT,
        bcc: users,
        subject: subject,
        text: text
    };
    mailService.sendMessage(message)
        .then(() => {
            return true;
        })
        .catch(error => {
            return false;
        });
}

/**
 * @param {*} users Array con los roles de los usuarios que deben recibir una notificación.
 * @param {*} study_id  Estudio que cambia de estado.
 * @param {*} newStatus  Nuevo estado del estudio.
 */
function sendAssignNotifications(user, username, studyCodes) {
    var email = config.new_user_study_assign_email;
    var texto = email.message.replace('{username}', username).replace('{studyCodes}', studyCodes) + email.footer;
    return sendNotifications(user, email.asunto, texto);
}

module.exports = {
    sendNotifications,
    sendAssignNotifications
}