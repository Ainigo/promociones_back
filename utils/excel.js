const Excel = require("exceljs/modern.nodejs");
const studyService = require("./../services/study");
const programService = require("./../services/program");
const exportQuerys = require("./../config/exportQuerys");
//import Excel from 'exceljs/modern.browser';
const fs = require('fs');
const path = require('path');

var STUDY = {
    code: 'B3',
    region: 'B5',
    city: 'B6',
    created_at: 'B8'
}

// setValue(worksheetObra, currentRow, 1, 'promotion_id');
// setValue(worksheetObra, currentRow, 2, 'habitacion');
// setValue(worksheetObra, currentRow, 3, 'stock');
// setValue(worksheetObra, currentRow, 4, 'inicial');
// setValue(worksheetObra, currentRow, 5, 'selled');

var LABELS = {
    ESTUDIO_DE_MERCADO: 'Información del estudio de mercado',
    PROGRAMAS: 'Programas',
    PROMOCIONES: 'PROMOCIONES',
    CALIDADES: 'CALIDADES',
    SEGUNDA_MANO: 'Inmuebles segunda mano',
    PROGRAMS: {
        PROMOTION_ID: 'promotion_id',
        ROOM: 'habitacion',
        STOCK: 'stock',
        INITIAL: 'inicial',
        SELLED: 'vendida',
    }
}

var STUDY_LABELS = {
    code: 'Código de estudio',
    region: 'Comunidad autónoma',
    city: 'Ciudad',
    created_at: 'Fecha creación'
}

function setValues(worksheet, celdas, values) {
    var keys = Object.keys(celdas);

    for (var j = 0; j < keys.length; j++) {
        var key = keys[j];
        var value = celdas[key];
        //console.log('Clave: '+key + "    Value: "+value);
        setValueNew(worksheet, value, values[key]);
    }
}


function setValuesAndLabels(worksheet, celdas, row, values) {
    var keys = Object.keys(celdas);

    for (var j = 0; j < keys.length; j++) {
        var key = keys[j];
        var label = celdas[key];
        var value = values[key];
        //console.log('Clave: '+key + "    Value: "+value);
        //setValueNew(worksheet, value, values[key]);
        setValue(worksheet, row, 1, label);
        setValue(worksheet, row, 2, value);
        row++;
    }
}

function printQualities(worksheet, initialRow, initialColumn, values) {
    var rowNum = initialRow;
    var colNum = initialColumn;
    var headerPrinted = false;
    var currentFirst = values[0].first_level;
    var currentPromo = values[0].promocion;
    setValue(worksheet, 11, colNum, currentPromo);
    values.forEach(element => {
        if (currentFirst != element.first_level) {
            currentFirst = element.first_level;
            rowNum++;
            // Pongo el valor de la promocion
            setValue(worksheet, 11, colNum, currentPromo);


        }
        // Si cambio de promo, actualizo el colNum y el rowNum
        if (currentPromo != element.promocion) {
            currentPromo = element.promocion;
            rowNum = initialRow;
            // después de la primera iteración, el header ya está pintado.
            headerPrinted = true;
            colNum++;
        }

        // Voy a imprimir los labels
        if (!headerPrinted) {
            setValue(worksheet, rowNum, 1, element.tipo);
        }
        setValue(worksheet, rowNum, colNum, element.valor);
        rowNum++;

    });
}

function printTable(worksheet, initialRow, initialColumn, values, titulo, printHeader) {
    var rowNum = initialRow+1;
    var colNum = initialColumn;
    
    setValue(worksheet, rowNum, 1, titulo);
    rowNum++;
    if (printHeader) {
        rowNum++;
    }

    var headerPrinted = false;
    for (let index = 0; index < values.length; index++) {
        var row = values[index];
        var keys = Object.keys(row);

        for (var j = 0; j < keys.length; j++) {
            var key = keys[j];
            if (!headerPrinted && printHeader) {
                setValue(worksheet, rowNum - 1, colNum, key);
            }
            //console.log('Clave: '+key + "    Value:#"+row[key]+"#     Row: "+rowNum + "   Col: "+colNum);
            setValue(worksheet, rowNum, colNum, row[key]);
            colNum++;
        }
        headerPrinted = true;
        colNum = initialColumn;
        rowNum++;
    }

}

function setValueNew(worksheet, celda, value) {
    var valor = null;
    if (value != null && value.value != null && value.value != undefined) {
        valor = value.value;
    } else {
        valor = value;
    }
    worksheet.getCell(celda).value = valor;
}

function setValue(worksheet, rownum, cellnum, value) {
    var row = worksheet.getRow(rownum);
    var cell = row.getCell(cellnum);
    if (value === 'calcNumber') {
        cell.type = Excel.ValueType.Formula;
        cell.formula = cell.formula;
    } else if (isDate(value)) {
        cell.value = value;
    } else if (value != null && value != undefined && Number.isInteger(value * 1)) {
        cell.value = parseFloat(value, 10);
    } else {
        cell.value = getValue(value);
    }
}

function getValue(value) {
    if (value != null && value.value != null && value.value != undefined) {
        valor = value.value;
    } else {
        valor = value;
    }

    return valor;
}

function isDate(value) {
    var isDate = false;
    try {
        return !isNaN(Date.parse(value));
        // Date.parse(value);
        // isDate = true;
    } catch (error) {

    }
    return isDate;
}

function setStyle(worksheet, rownum, cellnum, ) {
    var row = worksheet.getRow(rownum);
    var cell = row.getCell(cellnum);
    if (value === 'calcNumber') {
        cell.type = Excel.ValueType.Formula;
        cell.formula = cell.formula;
    } else if (value != null && value != undefined && Number.isInteger(value * 1)) {
        console.log(" --> Es un número. ponemos el tipo");
        //cell.value = Number(value);
        cell.value = parseFloat(value, 10);
        //cell.type = Excel.ValueType.Number;        
        //cell.numFmt = '#,#';
    } else {
        cell.value = value;
    }
}

async function exportStudy(req, res, study) {
    var newWorkbook = new Excel.Workbook();
    console.log(JSON.stringify(study));
    var promotions = await studyService.getDataToExport(exportQuerys.promotionFull, study.study.study_id);
    console.log("exportQuerys.promotionFull: "+exportQuerys.promotionFull);
    var shrs = await studyService.getDataToExport(exportQuerys.second_hand_rent, study.study.study_id);
    var qualities = await studyService.getDataToExport(exportQuerys.qualities, study.study.study_id);
    console.log(exportQuerys.qualities);
    
    var currentRow = 1;
    //var worksheetObra = newWorkbook.getWorksheet('OBRA NUEVA');
    var worksheetObra = newWorkbook.addWorksheet('Obra Nueva');
    setValue(worksheetObra, currentRow, 1, LABELS.ESTUDIO_DE_MERCADO);
    currentRow++;
    // console.log("CurrentRow " + currentRow);
    setValuesAndLabels(worksheetObra, STUDY_LABELS, currentRow, study.study.dataValues);
    currentRow = getNextRow(worksheetObra);

    // Pinto las promociones
    printTable(worksheetObra, currentRow, 1, promotions, LABELS.PROMOCIONES, true);

    var programs = [];
    programs.push(LABELS.PROGRAMS);

    for (var i = 0; i < promotions.length; i++) {
        var promotion = promotions[i];
        var program = await programService.getProgram(promotion.promotion_id, study.version[0].dataValues.version_id);
        for (var p = 0; p < program.length; p++) {
            if (program[p].pro_program != null){
                programs.push({
                    promotion_id: promotion.promotion_id,
                    room: program[p].dataValues.room_label,
                    stock: program[p].dataValues.pro_program.dataValues.stock,
                    inicial: program[p].dataValues.pro_program.dataValues.initial,
                    selled: program[p].dataValues.pro_program.dataValues.initial - program[p].dataValues.pro_program.dataValues.stock,
                });
            }
        }
    }

    currentRow = getNextRow(worksheetObra);
    printTable(worksheetObra, currentRow, 1, programs, LABELS.PROGRAMAS, false);

    currentRow = getNextRow(worksheetObra);
    printTable(worksheetObra, currentRow, 1, qualities, LABELS.CALIDADES, true);


    currentRow = 1;
    var worksheetSH = newWorkbook.addWorksheet('Inmuebles segunda mano');
    printTable(worksheetSH, currentRow, 1, shrs, LABELS.SEGUNDA_MANO, true);

    exportCsvToDownload(res, newWorkbook);
}


function getNextRow(worksheetObra) {
    return worksheetObra.lastRow._number + 1;
}
/**
 * 
 * @param {*} workbook 
 * @param {*} filename 
 */
function exportExcel(workbook, filename) {

    // Write to a file
    workbook.xlsx.writeFile(filename)
        .then(function() {
            console.log("El writeFile ha terminado");
        });

    // Write to stream
    workbook.xlsx.write(filename)
        .then(function() {
            console.log("El write to stream ha terminado");
        });


    // write to a new buffer
    workbook.xlsx.writeBuffer()
        .then(function(buffer) {
            console.log("El writeBuffer to stream ha terminado");
            // done
        });


    var buff = workbook.xlsx.writeBuffer().then(function(data) {
        var blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        saveAs(blob, "publications.xlsx");
    });

}

async function exportCsvToDownload(res, workbook) {
    console.log("Exportando csv");
    try {
        res.status(200);
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=studyReport.xls');
        workbook.xlsx.write(res)
            .then(function() {
                console.log("end");
                res.end();
            });
    } catch (error) {
        console.log(error);
    }

}

async function exportToDownload(res, workbook) {
    console.log("Exportando");
    try {
        res.status(200);
        res.setHeader('Content-Type', 'text/xlsx');
        res.setHeader('Content-Disposition', 'attachment; filename=studyReport.xlsx');
        workbook.xlsx.write(res)
            .then(function() {
                console.log("end");
                res.end();
            });
    } catch (error) {
        console.log(error);
    }

}

module.exports = {
    exportStudy
}