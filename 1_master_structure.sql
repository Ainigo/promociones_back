create extension pgcrypto;

create table pro_rooms_number(
 "room_number" Integer NOT NULL,
 "room_label"  Character varying(50)  not null
 );
 
 -- Alters para añadir las restricciones de cnt_values
ALTER TABLE "public"."stu_second_hand_rent" ADD CONSTRAINT "fk_stu_second_hand_status_id" FOREIGN KEY ("second_hand_status_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."stu_second_hand_rent" ADD CONSTRAINT "fk_stu_commercial_status_id" FOREIGN KEY ("commercial_status_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."stu_second_hand_rent" ADD CONSTRAINT "fk_stu_transaction_id" FOREIGN KEY ("transaction_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."stu_market_studies" ADD CONSTRAINT "fk_stu_status_id" FOREIGN KEY ("status_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."stu_market_studies" ADD CONSTRAINT "fk_stu_region_id" FOREIGN KEY ("region_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."stu_next_promotions" ADD CONSTRAINT "fk_stu_promo_type_id" FOREIGN KEY ("promo_type_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."stu_promotions" ADD CONSTRAINT "fk_stu_promotion_type_id" FOREIGN KEY ("promotion_type_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."stu_promotions" ADD CONSTRAINT "fk_stu_protection_type_id" FOREIGN KEY ("protection_type_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."stu_promotions" ADD CONSTRAINT "fk_stu_environment_id" FOREIGN KEY ("environment_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."pro_promotion_versions" ADD CONSTRAINT "fk_pro_work_status_id" FOREIGN KEY ("work_status_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."pro_buildings" ADD CONSTRAINT "fk_pro_status_id" FOREIGN KEY ("status_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."pro_buildings" ADD CONSTRAINT "fk_pro_building_type_id" FOREIGN KEY ("building_type_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."pro_building_single" ADD CONSTRAINT "fk_pro_single_type_id" FOREIGN KEY ("single_type_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."pro_building_multi" ADD CONSTRAINT "fk_pro_floor_id" FOREIGN KEY ("floor_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."usr_users" ADD CONSTRAINT "fk_usr_territorial_id" FOREIGN KEY ("territorial_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."stu_market_studies" ADD CONSTRAINT "fk_stu_territorial_id" FOREIGN KEY ("territorial_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Vistas
-- Vista de todos los estuios de un usuario/token
create view v_user_studies as
select tkn.token, usr.user_id, usr.role_id, sms.*
from usr_tokens tkn, usr_users usr, stu_market_users smu, stu_market_studies sms
where tkn.user_id = usr.user_id and usr.user_id = smu.user_id and smu.study_id = sms.study_id;

-- Vista de tipos y constantes
create view v_qua_salon_values as
 SELECT typ.type_id,
    typ.type_value,
    typ.type_label,
    val.value_id,
    val.value,
    val.label
   FROM cnt_types typ,
    cnt_values val
  WHERE typ.type_id = val.type_id AND typ.type_id = 13 AND val.enabled AND typ.enabled;


-- Vista con las promociones y sus versiones
create view v_promotion_versions as
select pro.*, prover.work_status_id, prover.delivery_date, ver.version_id, ver.version_number
from stu_promotions pro join pro_promotion_versions prover on (pro.promotion_id = prover.promotion_id)
	join stu_versions ver on (prover.version_id = ver.version_id);

-- Vista para quality
create or replace view v_pro_qualities as
SELECT
    qua.promotion_id,
    frt.label                  first_label,
    frt.value                  first_value,
    typ.type_id                type_id,
    typ.type_label             type_label,
    typ.type_value             type_value,
    typ.data_type,
    qua.value                  boolean_value,
    qua.cnt_quality_value_id   value_id    
FROM
    qua_first_levels   frt
    JOIN cnt_types          typ ON ( frt.first_level_id = typ.first_level_id )
    LEFT OUTER JOIN pro_qualities qua ON ( qua.cnt_quality_type_id = typ.type_id)    
where frt.enabled AND typ.enabled 
ORDER BY
      frt.first_level_id asc,
      typ.type_id asc;  


-- Vista de stado de programa
create or replace view v_program_status as
select promotion_id, 
    version_id,
    sum(initial - stock) as vendidas, 
    sum(stock) as stock,
    sum(initial) as total
from pro_program 
group by promotion_id, version_id


