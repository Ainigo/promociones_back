import { Socket } from 'socket.io';
import socketIO from 'socket.io';
import { UsuariosLista } from '../dist/classes/usuarios-lista';
import { Usuario } from '../dist/classes/usuario';

const { base64encode, base64decode } = require('nodejs-base64');



export const usuariosConectados = new UsuariosLista();


export const conectarCliente = ( cliente: Socket, io: socketIO.Server ) => {

    const usuario = new Usuario( cliente.id );
    usuariosConectados.agregar( usuario );

}


export const desconectar = ( cliente: Socket, io: socketIO.Server ) => {

    cliente.on('disconnect', () => {
        console.log('Cliente desconectado');

        usuariosConectados.borrarUsuario( cliente.id );

        io.emit('usuarios-activos', usuariosConectados.getLista()  );

    });

}


// Escuchar mensajes
export const mensaje = ( cliente: Socket, io: socketIO.Server ) => {

    cliente.on('mensaje', (  payload: { de: string, cuerpo: string }  ) => {

        console.log('Mensaje recibido', payload );

        io.emit('mensaje-nuevo', payload );

    });

}

// Configurar usuario
export const configurarUsuario = ( cliente: Socket, io: socketIO.Server ) => {

    cliente.on('configurar-usuario', (  payload: { nombre: string, pass: string }, callback: Function  ) => {
        
        usuariosConectados.actualizarNombre( cliente.id, payload.nombre );    
        io.emit( 'usuarios-activos', usuariosConectados.getLista() );
        

        if ( payload.pass ) {
            let decoded:any = base64decode( payload.pass );

            const prom =  new Promise((resolve,rej) => {               
                resolve(usuariosConectados.setLogin( payload.nombre, decoded ));                
            }); 
 
            Promise.all([prom]).then(values => {

                if ( values[0] ) {
                    usuariosConectados.actualizarNombre( cliente.id, payload.nombre );
    
                    io.emit('usuarios-activos', usuariosConectados.getLista()  );
            
                    callback({
                        ok: true,
                        mensaje: `Usuario ${ payload.nombre }, configurado`
                    });
    
                } else {
                    callback({
                        ok: false
                    });
                } 
            });  
        }
    });

}


// Obtener Usuarios
export const obtenerUsuarios = ( cliente: Socket, io: socketIO.Server ) => {

    cliente.on('obtener-usuarios', () => {

        io.to( cliente.id ).emit('usuarios-activos', usuariosConectados.getLista() );
        
    });

}
