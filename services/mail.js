"use strict";

var nodemailer = require("nodemailer");
require("dotenv").config();

let smtpConfig = {
  host: process.env.EMAIL_HOST,
  port: process.env.EMAIL_PORT,
  secureConnection: process.env.EMAIL_SECURE,
  auth: {
    user: process.env.EMAIL_ACCOUNT,
    pass: process.env.EMAIL_PASSWORD
  },
  tls: {
    ciphers: "SSLv3"
  }
};

let transporter = nodemailer.createTransport(smtpConfig);

module.exports = {
  sendMessage(message) {
    return new Promise((resolve, reject) => {
      transporter.sendMail(message, (error, success) => {
        if (error) {
          reject(error);
        } else {
          resolve(success);
        }
      });
    });
  }
};
