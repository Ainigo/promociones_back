const sequelize = require("../db/db");
const usersModel = require("../models").Users;
const studyUsersModel = require("../models").StudyUsers
const rolsModel = require("../models").Rols;
const valuesModel = require("../models/master").Values;
const usrTokensModel = require("../models").UsrTokens;
const logger = require("./../config/logger");
const messageService = require("../services/messagesManager");
const message = require("../config/messages");
const config = require("./../config/config");
const masterService = require("./../services/master");
const studyService = require("./../services/study");
const mail = require("./../utils/mail");

const emailSeq = [
    sequelize.fn(
        "PGP_SYM_DECRYPT",
        sequelize.cast(sequelize.col("email"), "bytea"),
        "AES_KEY"
    ),
    "email"
];
const nameSeq = [
    sequelize.fn(
        "PGP_SYM_DECRYPT",
        sequelize.cast(sequelize.col("name"), "bytea"),
        "AES_KEY"
    ),
    "name"
];
const USER_FIELDS = ["user_id", nameSeq, emailSeq, "valid", "last_login","role_id"];

const getUser = email =>
    usersModel.findOne({
        attributes: USER_FIELDS,
        include: [
            { model: rolsModel },
            { model: valuesModel, as: "territorial", attributes: masterService.MASTER_TABLE_FIELDS}
        ],
        where: sequelize.where(
            sequelize.fn(
                "PGP_SYM_DECRYPT",
                sequelize.cast(sequelize.col("email"), "bytea"),
                "AES_KEY"
            ),
            sequelize.Op.iLike,
            email
        )
    });


const getUserWithToken = email =>
    usersModel.findOne({
        attributes: USER_FIELDS,
        include: [
            { model: rolsModel },
            { model: usrTokensModel,
                limit: 1 // <--- LIMIT
             },
             { model: valuesModel, as: "territorial", attributes: masterService.MASTER_TABLE_FIELDS}
        ],
        where: sequelize.where(
            sequelize.fn(
                "PGP_SYM_DECRYPT",
                sequelize.cast(sequelize.col("email"), "bytea"),
                "AES_KEY"
            ),
            sequelize.Op.iLike,
            email
        )
    });    

const getUsers = () =>
    usersModel.findAll({
        attributes: USER_FIELDS,
        include: [
            { model: rolsModel },
            { model: valuesModel, as: "territorial", attributes: masterService.MASTER_TABLE_FIELDS}
        ]
    });


const deleteAllUserTokens = user_id =>
    usrTokensModel.destroy({
        where: { user_id }
    });

async function getUsersByRoleId(role_id){
    return await usersModel.findAll({
        attributes: USER_FIELDS,
        include: [
            { 
                model: rolsModel,
                where: {role_id},
                required: true
            },
            { 
                model: valuesModel, as: "territorial", 
                attributes: masterService.MASTER_TABLE_FIELDS,
                required: false
            }
        ]
    });
}

const addUser = user =>
    usersModel.create({
        role_id: user.role_id,
        valid: true,       
        territorial_id: user.territorial_id, 
        email: sequelize.fn("PGP_SYM_ENCRYPT", user.email, "AES_KEY"),
        name: sequelize.fn("PGP_SYM_ENCRYPT", 
                            sequelize.fn("PGP_SYM_DECRYPT",sequelize.cast(user.name, "bytea"),"AES_KEY"),
                         "AES_KEY"),
    })

function addParam(values, obj, param){
    if (obj[param]!=null && obj[param]!=undefined){
        values[param] = obj[param];
    }
}

async function updateUser(user, user_id){
    var values = {};
    addParam(values, user, 'name');
    if (user.email != null && user.email != undefined){
        values.email = sequelize.fn("PGP_SYM_ENCRYPT", user.email, "AES_KEY");
    }    
    addParam(values, user, 'valid');
    addParam(values, user, 'territorial_id');
    addParam(values, user, 'role_id');
    var update = await usersModel.update(
        values,
        { where: { user_id } 
    });
    if (update){
        logger.debug('Usuario actualizado ' + user_id);
        // Si se han revocado sus permisos, le borro los tokens
        if (user.valid=== false){
            deleteAllUserTokens(user_id).then(res=>{
                logger.debug("Tokens borrados al usuario "+user_id);
            });
        }
    } else {
        logger.warn('Usuario no actualizado ' + user_id);
    }
    return update;
}

const getUserByToken = token =>
    usrTokensModel.findOne({
        where: { 
            token,
            expires_on: {
                [sequelize.Op.gt]: new Date()
            }
         },
        include: [
            {
                model: usersModel,
                attributes: USER_FIELDS,
                required: true,
                where: { valid:true },
                include:[
                    {model: rolsModel, required: true},
                    {model: valuesModel, as: "territorial", attributes: masterService.MASTER_TABLE_FIELDS}
                ]
            }
        ]
    });


async function getUserByTokenAsync(token){
    return await usrTokensModel.findOne({
        where: { token },
        include: [
            {
                model: usersModel,
                attributes: USER_FIELDS,
                required: true,
                include:[
                    {model: rolsModel, required: true}
                ]
            }
        ]
    });
}

async function getStudiesForUser(user_id){
    return await studyUsersModel.count({
        where: { user_id }
    });
}


const removeToken = token => {
    usrTokensModel.destroy({
        where: { token }
    }).then(res => {
        logger.info("Token borrado: " + JSON.stringify(res));
    });
};


function removeExpiredTokens() {
    usrTokensModel.destroy({
        where: { 
            expires_on: {
                [sequelize.Op.lt]: new Date()
            } 
        }
    }).then(res => {
        logger.info("Token borrados: " + JSON.stringify(res));
    });
};


async function deleteUser(user_id){
    await masterService.callStoreProcedure(masterService.storeProcedures.PROCEDURE_DELETE_USER, user_id);
}



const checkAuth = (url, method, token) => {
    return sequelize.query('select true as isAuth from url_rights rights, usr_rols roles, url_urls urls, usr_users users, usr_tokens tokens where rights.role_id = roles.role_id and rights.url_id = urls.url_id and users.role_id = rights.role_id and users.user_id = tokens.user_id and ' +
        'urls.url like :requestedUrl and rights.methods like :requestedMethod and tokens.token = :userToken	 ', { replacements: { requestedUrl: url + '%', requestedMethod: '%' + method + '%', userToken: token }, type: sequelize.QueryTypes.SELECT }
    )
};

const checkAuthContent = (token, params, userRoleId) => {
    if (!requestWithoutParams(params)){
        // Si la petición viene sin parámetros, será la pantlla de studies. Le dejamos ver.
        return new Promise(function(resolve, reject) {
            // Do async job     
            resolve([true, {rowCount:1}]);            
        })        
    }
    var queryForParams = getQueryForParams(params, userRoleId);
    queryForParams += " and token = '" + token + "'";

    return sequelize.query(queryForParams);

}

function getQueryForParams(params, userRoleId) {
    var queryBase = "select true as valid ";
    var from = null;
    var where = null;
    if (userRoleId == config.users.territorial){
        from = "from usr_users usr, stu_market_studies sms";
        where = "where usr.territorial_id = sms.territorial_id";
    }else {
        from = "from usr_users usr, usr_tokens tkn, stu_market_users smu, stu_market_studies sms";
        where = " where usr.user_id = tkn.user_id and smu.user_id = usr.user_id and smu.study_id = sms.study_id ";    
    }
    
    if (params.study_id != null && params.study_id != undefined) {
        where += "and sms.study_id = " + params.study_id;
    } else if (params.promotion_id != null && params.promotion_id != undefined) {
        from += ', stu_promotions pro';
        where += "and sms.study_id = pro.study_id  and pro.promotion_id = " + params.promotion_id;
    } else if (params.building_id != null && params.building_id != undefined) {
        from += ', stu_promotions pro, pro_buildings bld';
        where += "and sms.study_id = pro.study_id and pro.promotion_id = bld.promotion_id and bld.building_id = " + params.building_id;
    } else if (params.shr_id != null && params.shr_id != undefined) {
        from += ", stu_second_hand_rent shd";
        where += "and sms.study_id = shd.study_id and shd.shr_id = " + params.shr_id;
    } else if (params.next_promotion_id != null && params.next_promotion_id != undefined) {
        from += ', stu_next_promotions snp';
        where += "AND sms.study_id = snp.study_id and snp.next_promotion_id = " + params.next_promotion_id;
    }
    return queryBase + from + where;
}

function requestWithoutParams(params){
    return (params.study_id == null && 
            params.promotion_id == null && 
            params.shr_id != null &&
            params.next_promotion_id == null);
}

const getUserByLogin = email => usersModel.findOne({
    where: sequelize.where(
        sequelize.fn(
            "PGP_SYM_DECRYPT",
            sequelize.cast(sequelize.col("email"), "bytea"),
            "AES_KEY"
        ),
        sequelize.Op.iLike,
        email
    )
});

async function assignStudies(res, user_id, studies){

    var current_date = new Date();
    var transaction = null;
    var user = null;
    try {
        messageService.checkParamInformed(user_id);
        messageService.checkParamInformed(studies);
        user = await usersModel.findOne({
                attributes: USER_FIELDS,
                where: {user_id},
                include: [
                    { model: rolsModel }
                ]
            });
        
        if (user.usr_rol.role_id > 2){
            messageService.throwMessageError(message.ERROR_USUARIO_NO_PUEDE_ASIGNAR_ESTUDIOS);
        }else if( user.usr_rol.role_name == config.users.basico){
            // Solo puede tener un estudio asignado.
            messageService.throwIfNecessaryMessageError(studies.length == 1 , message.ERROR_USUARIO_BASICO_UN_ESTUDIO);
            var userStudies = await getStudiesForUser(user_id);
            messageService.throwIfNecessaryMessageError(studies.length + userStudies, message.ERROR_USUARIO_BASICO_UN_ESTUDIO);
        }
        
        transaction = await sequelize.transaction();
        var studyCodes = null;
        for (var i = 0; i < studies.length; i++){
            var userStudy = studyUsersModel.build({
                user_id: user_id,        
                study_id: studies[i].study_id,
                created_on: current_date            
            });
            var study = await studyService.getStudy(studies[i].study_id);
            if (studyCodes){
                studyCodes = studyCodes + ", "+ study.code;
            }else{
                studyCodes = study.code;
            }
            await userStudy.save({transaction});
        }
        await transaction.commit();

        // Una vez que todo ha ido bien, enviamos las notificaciones
        logger.debug("Envio al usuario "+ user.email + " una notificación de asignación de los estudios " + studyCodes);
        mail.sendAssignNotifications(user.email, user.name, studyCodes);

        messageService.createMessage(res, message.INFO_OK);        
    } catch (error) {
        logger.error("[UsersController - assignStudies]: " + JSON.stringify(error));
        if (transaction){
            await transaction.rollback();
        }
        messageService.buildMessageError(res, (error.type == 'custom')? error: message.ERROR_ASIGNAR_ESTUDIO);        
    }
}

function unAssignStudies(res, user_id, study_id){
    try {        
        messageService.checkParamInformed(user_id);
        messageService.checkParamInformed(study_id);
        
        return studyUsersModel.destroy({
            where: { user_id, study_id }
        });
    } catch (error) {
        messageService.buildMessageError(res, (error.type == 'custom')? error: message.ERROR_ASIGNAR_ESTUDIO);          
    }
}


const addToken = token => usrTokensModel.create(token);

const updateLastLoginDate = user => usersModel.update({ last_login: Date.now() }, { returning: true, where: { user_id: user.user_id } }).then((res) => {
    if (res) {
        logger.debug('Login date updated for user ' + user.user_id);
    } else {
        logger.warn('Unable to update de last login date for user ' + user.user_id);
    }
});


  
module.exports = {
    addUser,
    getUser,
    getUserWithToken,
    getUsers,
    getUsersByRoleId,
    getUserByToken,
    getUserByTokenAsync,
    getUserByLogin,
    assignStudies,
    unAssignStudies,
    checkAuth,
    removeToken,
    removeExpiredTokens,
    deleteAllUserTokens,
    deleteUser,
    addToken,
    updateLastLoginDate,
    updateUser,
    checkAuthContent
};