const sequelize = require("../db/db");
const nextPromotionsModel = require("../models/studies").NextPromotions;
const promoTypeModel = require("../models/master").Values;
const environmentModel = require("../models/master").Values;

const promoterSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("promoter"), "bytea"),
    "AES_KEY"
  ),
  "promoter"
];

const nameSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("name"), "bytea"),
    "AES_KEY"
  ),
  "name"
];

const addressSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("address"), "bytea"),
    "AES_KEY"
  ),
  "address"
];

const coordinatesSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("coordinates"), "bytea"),
    "AES_KEY"
  ),
  "coordinates"
];

const NEXT_PROMOTION_FIELDS = ["next_promotion_id", promoterSeq, nameSeq, addressSeq, coordinatesSeq, "study_id"];
const MASTER_TABLE_FIELDS = ["value_id", "value", "label"];

const getNextPromotion = next_promotion_id =>
  nextPromotionsModel.findOne({
    where: { next_promotion_id },
    attributes: NEXT_PROMOTION_FIELDS,
    include: [
      { model: promoTypeModel, as: "promo_type", attributes: MASTER_TABLE_FIELDS },
      { model: environmentModel, as: "environment", attributes: MASTER_TABLE_FIELDS }
    ]
  });


const getNextPromotions = study_id =>
  nextPromotionsModel.findAll({
    where: { study_id },
    attributes: NEXT_PROMOTION_FIELDS,
    include: [
      { model: promoTypeModel, as: "promo_type", attributes: MASTER_TABLE_FIELDS },
      { model: environmentModel, as: "environment", attributes: MASTER_TABLE_FIELDS }
    ]
  });

const createNextPromotion = nextPromotion =>
  nextPromotion.save();

const deleteNextPromotion = next_promotion_id =>
  nextPromotionsModel.destroy({ where: { next_promotion_id } });

module.exports = {
  getNextPromotion,
  getNextPromotions,
  createNextPromotion,
  deleteNextPromotion
}