const sequelize = require("../db/db");
const studyService = require("../services/study");
const masterService = require("../services/master");
const qualitiesService = require("../services/qualities");
const promotionModel = require("../models/studies").Promotions;
const promotionVersionsModel = require("../models/promotions").PromotionVersions;
const buildingsModel = require("../models/promotions").Buildings;
const buildingMultiModel = require("../models/promotions").BuildingMulti;
const buildingSingleModel = require("../models/promotions").BuildingSingle;
const financesService = require("./finances");
const programModel = require("../models/promotions").Program;
const roomsModel = require("../models/promotions").RoomsNumber;
const promoTypeModel = require("../models/master").Values;
const environmentModel = require("../models/master").Values;
const protectionTypeModel = require("../models/master").Values;
const workStatusModel = require("../models/master").Values;
const statusBuildingModel = require("../models/master").Values;
const floorModel = require("../models/master").Values;
const singleTypeModel = require("../models/master").Values;

const logger = require("../config/logger");

const promoterSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("promoter"), "bytea"),
    "AES_KEY"
  ),
  "promoter"
];
const nameSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("name"), "bytea"),
    "AES_KEY"
  ),
  "name"
];
const addressSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("address"), "bytea"),
    "AES_KEY"
  ),
  "address"
];
const coordinatesSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("coordinates"), "bytea"),
    "AES_KEY"
  ),
  "coordinates"
];
const marketerSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("marketer"), "bytea"),
    "AES_KEY"
  ),
  "marketer"
];
const webSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("web"), "bytea"),
    "AES_KEY"
  ),
  "web"
];
const telephoneSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("telephone"), "bytea"),
    "AES_KEY"
  ),
  "telephone"
];

const architectSeq = [
  sequelize.fn(
    "PGP_SYM_DECRYPT",
    sequelize.cast(sequelize.col("architect"), "bytea"),
    "AES_KEY"
  ),
  "architect"
];

const PROMOTION_FIELDS = ["promotion_id", promoterSeq, nameSeq, addressSeq, coordinatesSeq, marketerSeq,
  "sales_start_date", webSeq, telephoneSeq, architectSeq, "comparable", "observations", "study_id"];
const BUILDING_FIELDS = ["building_id", "rooms", "pvp_commercial", "version_id"];
const SINGLE_FIELDS = ["sbr","spb","spp","sbc"]

const MASTER_TABLE_FIELDS = ["value_id", "value", "label"];


async function getPromotion(promotion_id, version_id) {
  let json = null
  try {
    
    
    let promotion = await getPromotionById(promotion_id, version_id);
    let building = await getBuildings(promotion_id, version_id);
    let finance = await financesService.getFinance(promotion_id);
    let program = await getProgram(promotion_id, version_id);
    let quality = await qualitiesService.getQualities(promotion_id);

    var studyId = await getStudyId(promotion_id);

    let study = await studyService.getStudy(studyId.study_id);

    json = { promotion, building, finance, program, quality, study };

  } catch (error) {
    logger.error("ERRORRRR: "+JSON.stringify(error));
  }
  return json;
}

async function getPromotionById(promotion_id, version_id) {
  return await promotionModel.findOne({
    where: { promotion_id },
    attributes: PROMOTION_FIELDS,
    include: [
      { model: promoTypeModel, as: "promo_type", attributes: MASTER_TABLE_FIELDS },
      { model: environmentModel, as: "environment", attributes: MASTER_TABLE_FIELDS },
      { model: protectionTypeModel, as: "protection_type", attributes: MASTER_TABLE_FIELDS },
      {
        model: promotionVersionsModel,
        attributes: ["delivery_date", "version_id"],
        where: { version_id },
        include: { model: workStatusModel, as: "work_status", attributes: MASTER_TABLE_FIELDS }
      }
    ]
  })
};

//TDOO: esto es una basura

async function getPromotionByStudyId(study_id, version_id) {
  return await promotionModel.findAll({
    attributes: PROMOTION_FIELDS,
    include: [
      { model: promoTypeModel, as: "promo_type", attributes: MASTER_TABLE_FIELDS },
      { model: environmentModel, as: "environment", attributes: MASTER_TABLE_FIELDS },
      { model: protectionTypeModel, as: "protection_type", attributes: MASTER_TABLE_FIELDS },
      {
        model: promotionVersionsModel,
        attributes: ["delivery_date", "version_id"],
        where: { version_id },
        include: [{ model: workStatusModel, as: "work_status", attributes: MASTER_TABLE_FIELDS }]
      }
    ],
    where: { study_id },
    order: [
      ["created_at", "DESC"]
  ]
    
  })
};

async function getBuildings(promotion_id, version_id) {
  return await buildingsModel.findAll({
    where: { promotion_id, version_id },
    attributes: BUILDING_FIELDS,
    include: [
      { model: statusBuildingModel, as: "status", attributes: MASTER_TABLE_FIELDS },
      {
        model: buildingMultiModel,
        on: {
          col1: sequelize.where(sequelize.col("pro_buildings.building_id"), "=", sequelize.col("pro_building_multi.building_id")),
          col2: sequelize.where(sequelize.col("pro_buildings.version_id"), "=", sequelize.col("pro_building_multi.version_id"))
        },
        attributes: ["util_area"],
        include: { model: floorModel, as: "floor", attributes: MASTER_TABLE_FIELDS }
      },
      {
        model: buildingSingleModel,
        on: {
          col1: sequelize.where(sequelize.col("pro_buildings.building_id"), "=", sequelize.col("pro_building_single.building_id")),
          col2: sequelize.where(sequelize.col("pro_buildings.version_id"), "=", sequelize.col("pro_building_single.version_id"))
        },
        attributes: SINGLE_FIELDS,
        include: { model: singleTypeModel, as: "single_type", attributes: MASTER_TABLE_FIELDS }
      }
    ]
  })  
};


async function getProgram(promotion_id, version_id) {
  return await roomsModel.findAll({
    include: [
      {
        model: programModel,
        on: {
          col1: sequelize.where(sequelize.col("pro_rooms_number.room_number"), "=", sequelize.col("pro_program.rooms")),
          col2: sequelize.where(sequelize.col("pro_program.promotion_id"), "=", promotion_id),
          col3: sequelize.where(sequelize.col("pro_program.version_id"), "=", version_id)
        }
      }
    ]
  })
};


async function createPromotion(promotion, transaction) {
  await promotion.save({ transaction });
}

async function createPromotionVersion(promotionVersion, transaction) {
  await promotionVersion.save({ transaction });
}

async function updatePromotion(promotion, promotion_id, transaction) {
  await promotionModel.update(
    promotion,
    {
      where: { promotion_id },
      transaction
    }
  )
}

async function updatePromotionSold(transaction, promotion_id, version_id){
  try {   
    
    await programModel.update(
        {stock: 0},    
        {
          where: { promotion_id, version_id },
          transaction
        });
    let value_id_vendido = await masterService.getCntValueId('Vendido','8');
    await buildingsModel.update(
        {status_id : value_id_vendido.value_id},
        {
          where: { promotion_id, version_id },
          transaction
        }
    );  
  } catch (error) {
    logger.error("Error en en updatePromotionSold: "+JSON.stringify(error));
    throw error;
  }
}

async function updatePromotionVersion(promotionVersion, promotion_id, version_id, transaction) {
  await promotionVersionsModel.update(
    promotionVersion,
    {
      where: { promotion_id, version_id },
      transaction
    }
  )
}


async function getStudyId(promotion_id) {
  return await promotionModel.findOne({
    attributes: ["study_id"],
    where: { promotion_id }
  })
}

module.exports = {
  getPromotion,
  getPromotionByStudyId,
  getBuildings,
  createPromotion,
  createPromotionVersion,
  updatePromotionSold,
  updatePromotion,
  updatePromotionVersion
}