const sequelize = require("../db/db");

const logger = require("../config/logger");
const studiesModel = require("../models/studies").VUsersStudies;
const studyModel = require("../models/studies").MarketStudies;
const studyUsersModel = require("../models/index").StudyUsers;
const secondHandModel = require("../models/studies").SecondHand;
const nextPromotionsModel = require("../models/studies").NextPromotions;
const promotionsModel = require("../models/studies").Promotions;
const versionsStudyModel = require("../models/studies").VersionsStudy;

const statusStudyModel = require("../models/master").Values;
const regionsStudyModel = require("../models/master").Values;
const territorialStudyModel = require("../models/master").Values;
const transactionSecondHandModel = require("../models/master").Values;
const environmentModel = require("../models/master").Values;
const promoTypeModel = require("../models/master").Values;
const usersModel = require("../models/index").Users
const valuesModel = require("../models/master").Values;

const masterService = require("./../services/master");
const config = require("./../config/config");
const exportQuerys = require("./../config/exportQuerys");

const addressSeq = [
    sequelize.fn(
        "PGP_SYM_DECRYPT",
        sequelize.cast(sequelize.col("address"), "bytea"),
        "AES_KEY"
    ),
    "address"
];
const coordinatesSeq = [
    sequelize.fn(
        "PGP_SYM_DECRYPT",
        sequelize.cast(sequelize.col("coordinates"), "bytea"),
        "AES_KEY"
    ),
    "coordinates"
];
const promoterSeq = [
    sequelize.fn(
        "PGP_SYM_DECRYPT",
        sequelize.cast(sequelize.col("promoter"), "bytea"),
        "AES_KEY"
    ),
    "promoter"
];
const nameSeq = [
    sequelize.fn(
        "PGP_SYM_DECRYPT",
        sequelize.cast(sequelize.col("name"), "bytea"),
        "AES_KEY"
    ),
    "name"
];

const emailSeq = [
    sequelize.fn(
        "PGP_SYM_DECRYPT",
        sequelize.cast(sequelize.col("email"), "bytea"),
        "AES_KEY"
    ),
    "email"
];

const STUDY_FIELDS = ["study_id", "code", "city", "zone", "enabled", "created_at"];

const STUDY_USERS_FIELDS = ["user_id", "study_id"]
const STUDY_USERS_II_FIELDS = [emailSeq, "role_id"]

const MASTER_TABLE_FIELDS = ["value_id", "value", "label"];
const SECOND_HAND_FIELDS = ["shr_id", addressSeq, coordinatesSeq, "rooms", "price"];
const NEXT_PROMOTIONS_FIELDS = ["next_promotion_id", promoterSeq, nameSeq, addressSeq, coordinatesSeq];
const PROMOTIONS_FIELDS = ["promotion_id", promoterSeq, nameSeq, addressSeq, coordinatesSeq];

const getStudiesByToken = token =>
    studiesModel.findAll({
        attributes: STUDY_FIELDS,
        include: [
            { model: statusStudyModel, as: "status", attributes: MASTER_TABLE_FIELDS },
            { model: regionsStudyModel, as: "region", attributes: MASTER_TABLE_FIELDS }
            //{ model: territorialStudyModel, as: "territorial", attributes: MASTER_TABLE_FIELDS }
        ],
        where: { token, enabled: true },
        order: [
            ["created_at", "DESC"]
        ]
    });


const getStudiesByUserId = user_id =>
    studyModel.findAll({
        attributes: STUDY_FIELDS,
        include: [{
                model: studyUsersModel,
                attributes: ["user_id"],
                required: true,
                where: { user_id }
            },
            { model: statusStudyModel, as: "status", attributes: MASTER_TABLE_FIELDS },
            { model: regionsStudyModel, as: "region", attributes: MASTER_TABLE_FIELDS }
            //    { model: territorialStudyModel, as: "territorial", attributes: MASTER_TABLE_FIELDS }
        ],
        where: { enabled: true },
        order: [
            ["created_at", "DESC"]
        ]
    });



const getStudiesExcept = studiesArray =>
    studyModel.findAll({
        attributes: STUDY_FIELDS,
        distinct: true,
        include: [
            { model: statusStudyModel, as: "status", attributes: MASTER_TABLE_FIELDS },
            { model: regionsStudyModel, as: "region", attributes: MASTER_TABLE_FIELDS },
            { model: territorialStudyModel, as: "territorial", attributes: MASTER_TABLE_FIELDS }
        ],
        where: {
            study_id: {
                [sequelize.Op.notIn]: studiesArray
            }
        },
        order: [
            ["created_at", "DESC"]
        ]
    });

const getAllStudies = () =>
    studyModel.findAll({
        attributes: STUDY_FIELDS,
        distinct: true,
        include: [
            { model: statusStudyModel, as: "status", attributes: MASTER_TABLE_FIELDS },
            { model: regionsStudyModel, as: "region", attributes: MASTER_TABLE_FIELDS },
            { model: territorialStudyModel, as: "territorial", attributes: MASTER_TABLE_FIELDS }
        ],
        order: [
            ["created_at", "DESC"]
        ]
    });

const getStudiesForTerritorial = (territorial_id) =>
    studyModel.findAll({
        attributes: STUDY_FIELDS,
        distinct: true,
        include: [
            { model: statusStudyModel, as: "status", attributes: MASTER_TABLE_FIELDS },
            { model: regionsStudyModel, as: "region", attributes: MASTER_TABLE_FIELDS },
            { model: territorialStudyModel, as: "territorial", attributes: MASTER_TABLE_FIELDS }
        ],
        where: { territorial_id },
        order: [
            ["created_at", "DESC"]
        ]
    });

async function getStudiesById(study_id) {
    let json = null;
    try {
        let study = await getStudy(study_id);
        if (study == null) {
            return null;
        }
        let study_status = await getNextPreviousStudyStatus(study.status.value);
        let second_hand_rent = await getSecondHand(study_id);
        let next_promotions = await getNextPromotions(study_id);
        let promotions = await getPromotions(study_id);
        let version = await getVersionsStudy(study_id);

        json = { study, study_status, second_hand_rent, next_promotions, promotions, version };

    } catch (error) {
        logger.error("Error en el getStudiesById: " + JSON.stringify(error));
        throw error;
    }
    return json;
}

async function getNextPreviousStudyStatus(actual) {
    var next = null;
    var previous = null;

    if (actual == config.studyStatus.abierto) {
        next = config.studyStatus.enRevision;
    } else if (actual == config.studyStatus.enRevision) {
        previous = config.studyStatus.abierto;
        next = config.studyStatus.cerrado;
    } else {
        next = config.studyStatus.nuevaVersion;
    }
    return { previous, actual, next };
}

async function getSecondHand(study_id) {
    return await secondHandModel.findAll({
        where: { study_id },
        attributes: SECOND_HAND_FIELDS,
        include: [
            { model: transactionSecondHandModel, as: "transaction", attributes: MASTER_TABLE_FIELDS },
        ]
    });
}


async function getStudyByCode(study_code) {
    return await studyModel.findOne({
        where: { code: study_code }
    });
}

async function getStudy(study_id) {
    return await studyModel.findByPk(study_id, {
        attributes: STUDY_FIELDS,
        include: [
            { model: statusStudyModel, as: "status", attributes: MASTER_TABLE_FIELDS },
            { model: regionsStudyModel, as: "region", attributes: MASTER_TABLE_FIELDS },
            { model: territorialStudyModel, as: "territorial", attributes: MASTER_TABLE_FIELDS }
        ]
    });
}

async function getStudyToExport(study_id) {
    return await studyModel.findByPk(study_id, {
        attributes: STUDY_FIELDS
    });
}


async function getUsersStudy(study_id) {
    return await studyUsersModel.findAll({
        attributes: STUDY_USERS_FIELDS,
        where: { study_id },
        include: [{
                attributes: ["code"],
                model: studyModel,
                on: {
                    col1: sequelize.where(sequelize.col("stu_market_studies.study_id"), "=", sequelize.col("stu_market_users.study_id"))
                },
            },
            {
                attributes: STUDY_USERS_II_FIELDS,
                model: usersModel,
                on: {
                    col1: sequelize.where(sequelize.col("usr_users.user_id"), "=", sequelize.col("stu_market_users.user_id"))
                },
            }
        ]
    });
}

async function getNextPromotions(study_id) {
    return await nextPromotionsModel.findAll({
        where: { study_id },
        attributes: NEXT_PROMOTIONS_FIELDS,
        include: [
            { model: promoTypeModel, as: "promo_type", attributes: MASTER_TABLE_FIELDS },
            { model: environmentModel, as: "environment", attributes: MASTER_TABLE_FIELDS }
        ]
    });
}

async function getPromotions(study_id) {
    return await promotionsModel.findAll({
        where: { study_id },
        attributes: PROMOTIONS_FIELDS,
        include: [
            { model: promoTypeModel, as: "promo_type", attributes: MASTER_TABLE_FIELDS }
        ],order: [
            ["created_at", "DESC"]
        ]
    });
}

async function createStudy(study, transaction) {
    return await study.save({ transaction });
}

async function createStudyVersion(studyVersion, transaction) {
    return await studyVersion.save({ transaction });
}


async function createNewVersion(study_id) {
    await masterService.callStoreProcedure(masterService.storeProcedures.PROCEDURE_CREATE_NEW_VERSION, study_id);
    //await sequelize.query('CALL new_study_version(:study_id);', { replacements: { study_id: study_id } });
}

const updateStudy = (study, study_id) =>
    studyModel.update(study, { where: { study_id } });

const updateStudyStatus = (study_id, new_status_id) =>
    studyModel.update({ status_id: new_status_id }, { where: { study_id } });

const getVersionStudy = (study_id, version_number) =>
    versionsStudyModel.findAll({
        where: { study_id, version_number },
        attributes: { exclude: "updated_at" }
    });

const getVersionsStudy = study_id =>
    versionsStudyModel.findAll({
        where: { study_id },
        attributes: { exclude: "updated_at" }
    });

async function getDefaultVersion(study_id, version_number = 0) {
    return await versionsStudyModel.findOne({
        where: { study_id, version_number },
        attributes: ["version_id"]
    })
};

async function getVersionStudyByPromotionId(promotion_id, version_number = 0) {
    return await sequelize.query('select ver.version_id from stu_versions ver, stu_promotions pro where ver.study_id = pro.study_id and ver.version_number = :version_number ' +
        'and pro.promotion_id = :promotion_id;', { replacements: { version_number: version_number, promotion_id: promotion_id }, type: sequelize.QueryTypes.SELECT })
}

/**
 * Function that search for a version_id and version_number. If there are any row, the data are not correct.
 * @param {*} version_id 
 * @param {*} version_number 
 */
async function isValidVersion(version_id, version_number) {
    var valid = false;
    await versionsStudyModel.findAll({
        where: { version_id, version_number }
    }).then(data => {
        valid = data != null && data != undefined && data.length > 0;
    });
    return valid;
};

async function getStudyStatus(study_id) {
    return studyModel.findOne({
        attributes: ["status_id"],
        where: { study_id }
    })
}

async function getCurrentStatus(study_id) {
    return studyModel.findOne({
        attributes: [],
        where: { study_id },
        include: {
            model: valuesModel,
            attributes: ["value"],
            as: "status"
        }
    })
}


async function getDataToExport(query, study_id) {
    return await sequelize.query(query, {
        replacements: {
            estudio: study_id
        },
        type: sequelize.QueryTypes.SELECT
    })
}


module.exports = {
    getStudiesByToken,
    getStudiesByUserId,
    getStudiesExcept,
    getAllStudies,
    getStudiesForTerritorial,
    getStudiesById,
    getStudy,
    getStudyToExport,
    getStudyByCode,
    createStudy,
    createStudyVersion,
    createNewVersion,
    updateStudy,
    updateStudyStatus,
    getVersionStudy,
    getVersionsStudy,
    getDefaultVersion,
    getVersionStudyByPromotionId,
    isValidVersion,
    getStudyStatus,
    getSecondHand,
    getCurrentStatus,
    getUsersStudy,
    getDataToExport
};