const sequelize = require("../db/db");
const buildingsModel = require("../models/promotions").Buildings;
const buildingMultiModel = require("../models/promotions").BuildingMulti;
const buildingSingleModel = require("../models/promotions").BuildingSingle;
const buildingTypeModel = require("../models/master").Values;
const statusBuildingModel = require("../models/master").Values;
const singleTypeModel = require("../models/master").Values;
const floorModel = require("../models/master").Values;


const BUILDINGS_FIELDS = ["building_id","rooms","pvp_commercial","created_at","updated_at"];
const BUILDING_FIELDS = ["building_id","rooms", "bathrooms","terrace_area","pvp_commercial", "observations", "created_at","updated_at"];
const MASTER_TABLE_FIELDS = ["value_id", "value", "label"];

/*
const getBuildings = (promotion_id, version_id) =>
buildingsModel.findAll({
    where: { promotion_id, version_id },
    attributes: BUILDINGS_FIELDS,
    include: [
        {model: statusBuildingModel, as: "status", attributes: MASTER_TABLE_FIELDS},
        {model: buildingTypeModel, as: "building_type", attributes: MASTER_TABLE_FIELDS},
        {
            model: buildingMultiModel,
            on: {
                col1: sequelize.where(sequelize.col("pro_buildings.building_id"), "=", sequelize.col("pro_building_multi.building_id")),
                col2: sequelize.where(sequelize.col("pro_buildings.version_id"), "=", sequelize.col("pro_building_multi.version_id"))
            },
            attributes: ["util_area"],
            include: {model: floorModel, as: "floor", attributes: MASTER_TABLE_FIELDS}
        },
        {   model: buildingSingleModel,
            attributes: {exclude: "single_type_id"},
            include: {model: singleTypeModel, as: "single_type", attributes: MASTER_TABLE_FIELDS},
            on: {
                col1: sequelize.where(sequelize.col("pro_buildings.building_id"), "=", sequelize.col("pro_building_single.building_id")),
                col2: sequelize.where(sequelize.col("pro_buildings.version_id"), "=", sequelize.col("pro_building_single.version_id"))
            }
        }
    ]
});
*/
const getBuilding = (promotion_id, building_id, version_id) =>
buildingsModel.findOne({
    where: {promotion_id, building_id, version_id},
    attributes: BUILDING_FIELDS,
    include: [
        {model: statusBuildingModel, as: "status", attributes: MASTER_TABLE_FIELDS},
        {model: buildingTypeModel, as: "building_type", attributes: MASTER_TABLE_FIELDS},
        {   model: buildingMultiModel,
            attributes: {exclude: "floor_id"},
            include: {model: floorModel, as: "floor", attributes: MASTER_TABLE_FIELDS},
            on: {
                col1: sequelize.where(sequelize.col("pro_buildings.building_id"), "=", sequelize.col("pro_building_multi.building_id")),
                col2: sequelize.where(sequelize.col("pro_buildings.version_id"), "=", sequelize.col("pro_building_multi.version_id"))
            }
        },
        {   model: buildingSingleModel,
            attributes: {exclude: "single_type_id"},
            include: {model: singleTypeModel, as: "single_type", attributes: MASTER_TABLE_FIELDS},
            on: {
                col1: sequelize.where(sequelize.col("pro_buildings.building_id"), "=", sequelize.col("pro_building_single.building_id")),
                col2: sequelize.where(sequelize.col("pro_buildings.version_id"), "=", sequelize.col("pro_building_single.version_id"))
            }
        }
    ]
});

async function createBuilding(building, transaction){
    await building.save({transaction});
}

async function createUniBuilding(uniBuilding, transaction){
    await uniBuilding.save({transaction});
}

async function createPluriBuilding(pluriBuilding, transaction){
    await pluriBuilding.save({transaction});
}

async function updatePluriBuilding(buildingPluri, building_id, version_id, transaction){
  await buildingMultiModel.update(
    buildingPluri,
    {
      where: {building_id, version_id},
      transaction
    }
  )
}

async function updateUniBuilding(buildingUni, building_id, version_id, transaction){
  await buildingSingleModel.update(
    buildingUni,
    {
      where: {building_id, version_id},
      transaction
    }
  )
}

async function updateBuilding(building, building_id, version_id, transaction){
  await buildingsModel.update(
    building,
    {
      where: {building_id, version_id},
      transaction
    }
  )
}

async function getBuildingType(building_id, version_id){
    return await buildingsModel.findOne({
        attributes: ["building_type_id"],
        where: {building_id, version_id}
    })
}


module.exports = {
 //   getBuildings,
    getBuilding,
    createBuilding,
    createUniBuilding,
    createPluriBuilding,
    updatePluriBuilding,
    updateUniBuilding,
    updateBuilding,
    getBuildingType
}