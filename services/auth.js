const config = require("../config/config");
const logger = require("../config/logger");
const CustomError = require("../customError");
// const jwt = require("jsonwebtoken");
const userService = require("../services/user");
const message = require("../config/messages");
const azure = require("./authAzure");
const Configbbdd = require("../global/config");
const jsforce = require('jsforce');
const nodemailer = require("nodemailer");
// const fs = require('fs')
// const AWS = require('aws-sdk');
// const path = require('path');


class productosCat {

  constructor (nombre_producto, url_producto, id_producto, id_categoria, nombrePDF, name, orden) {
    this.nombre_producto = nombre_producto;
    this.url_producto = url_producto;
    this.id_producto = id_producto;
    this.id_categoria = id_categoria;
    this.nombrePDF = nombrePDF;
    this.name = name;
    this.orden = orden;
  }
}

class datosLLamada { 
  constructor (Destino_file__c, CreatedDate, talkdesk__CallDuration__c) {
    this.Destino_file__c = Destino_file__c;
    this.CreatedDate = CreatedDate;
    this.talkdesk__CallDuration__c = talkdesk__CallDuration__c;
  }
}

class promociones {
  constructor (id, name, promocion__c, estado_inmuebles__c, estadodecontratacion__c, interes__c, stagename) {
    this.id = id;
    this.name = name;
    this.promocion__c = promocion__c;
    this.estado_inmuebles__c = estado_inmuebles__c;
    this.estadodecontratacion__c = estadodecontratacion__c;
    this.interes__c = interes__c;
    this.stagename = stagename;
  }
}

class inmueblesSalesForce {
  constructor (idInmueble, nameInmueble, pvp, nombrePromocion, estadoComercial, estadoComercialSalesForce, Fecha_de_entrega__c) {
    this.idInmueble = idInmueble;
    this.nameInmueble = nameInmueble;
    this.pvp = pvp;
    this.nombrePromocion = nombrePromocion;
    this.estadoComercial = estadoComercial;
    this.estadoComercialSalesForce = estadoComercialSalesForce;
    this.Fecha_de_entrega__c = Fecha_de_entrega__c;
  }
}

class promotionSalesForce {
  constructor (name, id , IdentificadorPrinex__c){
    this.name = name;
    this.id = id;
    this.IdentificadorPrinex__c = IdentificadorPrinex__c;
  }
}

class usuarioSalesForce {
  constructor (name, email, dni, registrado,  telefono1, telefono2, fechaareaprivada, Id, idioma) {
    this.name = name;
    this.email = email;
    this.telefono1 = telefono1;
    this.telefono2 = telefono2;
    this.fechaareaprivada = fechaareaprivada;
    this.registrado = registrado;
    this.dni = dni;
    this.Id = Id;
    this.Idioma = idioma;
  }
}

class clienteSalesForce {
  constructor (cliente) {
    this.cliente = cliente;
  }
}

class clienteNamesSalesForce {
  constructor (name) {
    this.name = name;
  }
}

class clienteEmailsSalesForce {
  constructor (email, registrado, id, fecha) {
    this.email = email;
    this.registrado = registrado;
    this.id = id;
    this.fecha = fecha;
  }
}

class usersSalesForce {
  constructor (email, id, name) {
    this.email = email;
    this.name = name;
    this.id = id;
  }
}

class clienteOportunidadesSalesForce {
  constructor (id, account_id) {
    this.id = id;
    this.account_id = account_id;
  }
}

class oportunidadesUserSalesForce {
  constructor (id, name, promocionName, satage) {
    this.id = id;
    this.name = name;
    this.promocionName = promocionName;
    this.satage = satage;
  }
}

class encuestasOportunidadSalesForce {
  constructor (id, idUrl, name, createdDate, completionDate) {
    this.id = id;
    this.idUrl = idUrl;
    this.name = name;
    this.createdDate = createdDate;
    this.completionDate = completionDate;
    if (completionDate !== undefined) { 
       this.filled = true;
    } else {
      this.filled = false;
    }
  }
}

class allCategories {

  constructor (nombre_category, id_category) {
    this.nombre_category = nombre_category;
    this.id_category = id_category;
  }
}

class allUsers {

  constructor (id_user, user_mail, estado, fecha_creacion, user_name, user_perfil) {
    this.id_user = id_user;
    this.user_mail = user_mail;
    this.estado = estado;
    this.fecha_creacion = fecha_creacion;
    this.user_name = user_name;
    this.user_perfil = user_perfil;
  }
}

class UserAltaPortales {

  constructor (id, id_portal, portal, id_salesforce, nombre_promocion, fecha_creacion,user, nombre, email, mensaje, referencia, fecha_procesado, fecha_lead, ip, navegador, validacion_spam, idioma, url_portal) {
    this.id = id;
    this.id_portal = id_portal;
    this.portal = portal;
    this.id_salesforce = id_salesforce;
    this.nombre_promocion = nombre_promocion;
    this.fecha_creacion = fecha_creacion;
    this.user = user;
    this.nombre = nombre;
    this.email = email;
    this.mensaje = mensaje;
    this.referencia = referencia;
    this.fecha_procesado = fecha_procesado;
    this.fecha_lead = fecha_lead;
    this.ip = ip;
    this.navegador = navegador;
    this.validacion_spam = validacion_spam;
    this.idioma = idioma;
    this.url_portal = url_portal;
  }
}

const databaseConfig = {
  connectionLimit : Configbbdd.bbdd.connectionLimit,
  host: Configbbdd.bbdd.host,
  user: Configbbdd.bbdd.user,
  password: Configbbdd.bbdd.password,
  database: Configbbdd.bbdd.database
}

const databaseConfigTracking = {
  connectionLimit : Configbbdd.bbddTracking.connectionLimit,
  host: Configbbdd.bbddTracking.host,
  user: Configbbdd.bbddTracking.user,
  password: Configbbdd.bbddTracking.password,
  database: Configbbdd.bbddTracking.database
}

const saleforeceConfig = {
  hostnamelibreriaSalesForceConnect:Configbbdd.salesforce.hostnamelibreriaSalesForceConnect,
  hostnamelibreriaJSForceConnect: Configbbdd.salesforce.hostnamelibreriaJSForceConnect,
  username: Configbbdd.salesforce.username, //'apiuser@aedashomes.com';
  password: Configbbdd.salesforce.password, //'6SuTG9CsNpBKpcWb';
  securityToken: Configbbdd.salesforce.securityToken, //'vfUy1YgyPEpTCsrhJd3tkmLF0';

}

const mysql = require('mysql');
const { promisify } = require('util')

// async function authenticate(params){
//   logger.debug("Autenticamos");
//   logger.debug(params.email);
//   logger.debug(params.password);
//   var res = await azure.authenticateAzure(params.email, params.password);  
//   if (!res) {
//     logger.debug("El usuario no es correcto al validar en azure");
//     throw new CustomError(message.ERROR_USER_PASSWORD);
//   }else{
//     logger.debug("El usuario es correcto al validar en azure");
//         const pool = mysql.createPool(databaseConfig);
//         const promiseQuery = promisify(pool.query).bind(pool);
//         const promisePoolEnd = promisify(pool.end).bind(pool);
        
//         logger.debug("Comprobando si el usuario está activo");
//         const query = "SELECT USER_MAIL FROM USERS_LIVING WHERE ESTADO = 'ACTIVO' AND USER_MAIL = '"+params.email+"'";
//         const result = await promiseQuery(query)
//         logger.debug("el resultado es: " +result[0].USER_MAIL);
//         if(result[0].USER_MAIL === params.email)
//         {
//             logger.debug("Valido");
//             valido = true;
//             res = valido;              
//         }
//         else
//            res = falso;
//         pool.end(); 
//         promisePoolEnd();
//   }

//   return res;
// };


function authenticateUserEM(params){

  logger.debug("Autenticamos en azure: "+JSON.stringify(params));  
  return userService.getUser(params.userId).then(user => {
    // Chequeamos que el usuario existe y la contraseña sea correcta
    if (!user) {
      logger.debug("El usuario no se encuentra en la bbdd de Estudios de mercado");
      throw new CustomError(message.ERROR_USER_PASSWORD);
      //return new CustomError(message.ERROR_USER_PASSWORD);
    }
    userService.updateLastLoginDate(user);
    
    const ret = {
      token: params.accessToken,
      rol: user.usr_rol,
      email: user.email
    }

    //Guardamos el Token en BBDD en la tabla usr_tokens
    var usrToken = {
      token: params.accessToken,
      user_id: user.user_id,
      tkn_type_id: 1,
      expires_on: params.expiresOn
    };
    userService.addToken(usrToken);

    return ret;
  });

}

async function authenticate(params){
  logger.debug("Autenticamos");
  var ret = {};
  // logger.debug(params.email);
  // logger.debug(params.password);
  // var res = await azure.authenticateAzure(params.email, params.password);  
  // if (!res) {
  //   logger.debug("El usuario no es correcto al validar en azure");
  //   throw new CustomError(message.ERROR_USER_PASSWORD);
  // }else{
    logger.debug("El usuario es correcto al validar en azure");
        logger.debug(JSON.stringify(params))
        const pool = mysql.createPool(databaseConfig);
        const promiseQuery = promisify(pool.query).bind(pool);
        const promisePoolEnd = promisify(pool.end).bind(pool);

        
        
        logger.debug("Comprobando si el usuario está activo");
        const query = "SELECT USER_MAIL, PERFIL FROM USERS_GOTHAM WHERE ESTADO = 'ACTIVO' AND USER_MAIL = '"+params.userId+"'";
        const result = await promiseQuery(query)
        logger.debug("el resultado es: " +result[0].PERFIL);
        if(result[0].USER_MAIL.toLowerCase() === params.userId.toLowerCase())
        {
            logger.debug("Valido");
            valido = true;
             ret = {
              username: result[0].USER_MAIL,
              token: params.accessToken, 
              role_name: result[0].PERFIL,
            }
            
        }
        pool.end();
        promisePoolEnd();

  return ret;
};



async function perfil(params){
  logger.debug("Consulta Perfil");
  logger.debug(params.email);
  
  var valido = false;
  
    
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
    
  const query = "Select PERFIL from USERS_GOTHAM where USER_MAIL = '"+params.email+"'";
  const result = await promiseQuery(query) // use in async function
  logger.debug('el perfil es:' +result[0].PERFIL);
  if(result[0].PERFIL == 'ADMIN' || result[0].PERFIL == 'admin')
  {
    valido = true;               
  }
  
  pool.end();
  promisePoolEnd();


   return valido;
};

async function sendEmail(params){
  logger.debug("SEND EMAIL");
  

  let mailOptions = params;
  let token = "OK";

  console.log("LAS OPCIONES" +mailOptions);
  const transporter = nodemailer.createTransport({
    host: Configbbdd.email.host, 
    port: Configbbdd.email.port,
    secure: Configbbdd.email.secure,
    auth: {
        user: Configbbdd.email.user,
        pass: Configbbdd.email.pass
    },
    tls: { rejectUnauthorized: false }
});


console.log(transporter);

transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
        console.log(error);
        token = error;
        return token;
    } else {
        console.log("E-mail enviado com sucesso!" +token);
        return token;
    }
});
}



async function perfilUser(params){
  logger.debug("Consulta Perfil nuevo");
  logger.debug(params);
  
  var user;
  
    
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);

  const query = "Select PERFIL from USERS_GOTHAM where USER_MAIL = '"+params+"'";
  console.log(query);
  const result = await promiseQuery(query) // use in async function
  user = result[0].PERFIL;
  logger.debug('el perfil es:' +result[0].PERFIL);
    
  pool.end();
  promisePoolEnd();


   return user;
};

async function pintarLogs(params){
  logger.debug("Consulta Log Traza");
  logger.debug(params);
  
  var user;

  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
  
  try {

    
  
    const query = "Select Is_LOG from USERS_GOTHAM where USER_MAIL = '"+params+"'";
    console.log(query);
    const result = await promiseQuery(query) // use in async function
    user = result[0].Is_LOG;
    logger.debug('el dato de pintar traza es:' +result[0].Is_LOG);
      
    pool.end();
    promisePoolEnd();

  }
  catch (error) {
  
  pool.end();
  promisePoolEnd();
  console.log("ERRR en pintarLogs: " + error);
  messageService.buildMessageError(res, error);
}
   /* 
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);

  const query = "Select Is_LOG from USERS_GOTHAM where USER_MAIL = '"+params+"'";
  console.log(query);
  const result = await promiseQuery(query) // use in async function
  user = result[0].Is_LOG;
  logger.debug('el dato de pintar traza es:' +result[0].Is_LOG);
    
  pool.end();
  promisePoolEnd();
*/

   return user;
};

async function nameCategory(params){

  logger.debug("Consulta la categoria"); 
  
    
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
    
  const query = "Select NOMBRE_CATEGORIA from CATEGORIAS_LIVING where ID_CATEGORIA = "+params+"";
  const result = await promiseQuery(query) // use in async function
  if(result[0].NOMBRE_CATEGORIA === undefined)
  {
    valido = "SIN CATEGORIA";
  }
  else
  {
    valido = result[0].NOMBRE_CATEGORIA;
  }

  pool.end();
  promisePoolEnd();


   return valido;

};

async function nameProducts(params){

  logger.debug("Consulta los productos"); 

   let productos = [];
    
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
    

  const query = "Select NOMBRE_PRODUCTO, NOMBREPDF, NAME, RUTA_FICHERO, ORDEN from PRODUCTOS_LIVING where ID_CATEGORIA = "+params+" ORDER BY ORDEN";
  const result = await promiseQuery(query) // use in async function

  result.forEach(element => {
    const ProductosCategoria = new productosCat();
    ProductosCategoria.url_producto = element['RUTA_FICHERO'];
    ProductosCategoria.nombre_producto = element['NOMBRE_PRODUCTO'];
    ProductosCategoria.nombrePDF = element['NOMBREPDF'];
    ProductosCategoria.name = element['NAME'];
    ProductosCategoria.orden = element['ORDEN'];
    productos.push(ProductosCategoria);
  });

  logger.debug('los productos ahora son:' +productos);

  pool.end();
  promisePoolEnd();


   return productos;

};

async function nameAllProducts(params){

  logger.debug("Consulta todos los productos"); 

   let productos = [];
    
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
    

  const query = "Select ID_PRODUCTO, NOMBRE_PRODUCTO, ID_CATEGORIA, RUTA_FICHERO, NOMBREPDF, NAME, ORDEN from PRODUCTOS_LIVING ORDER BY ORDEN";
  const result = await promiseQuery(query) // use in async function

  result.forEach(element => {
    const ProductosCategoria = new productosCat();
    ProductosCategoria.url_producto = element['RUTA_FICHERO'];
    ProductosCategoria.nombre_producto = element['NOMBRE_PRODUCTO'];
    ProductosCategoria.id_producto = element['ID_PRODUCTO'];
    ProductosCategoria.id_categoria = element['ID_CATEGORIA'];
    ProductosCategoria.name = element['NAME'];
    ProductosCategoria.nombrePDF = element['NOMBREPDF'];
    ProductosCategoria.orden = element['ORDEN'];
    productos.push(ProductosCategoria);
  });

  logger.debug('todos los productos ahora son:' +productos);

  pool.end();
  promisePoolEnd();


   return productos;

};



async function allnameCategory(){

  logger.debug("Consulta el nombre de todas las categorias"); 
   let categorias = [];
    
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
    
  const query = "Select NOMBRE_CATEGORIA, ID_CATEGORIA from CATEGORIAS_LIVING";
  const result = await promiseQuery(query) // use in async function

  result.forEach(element => {
    const Categories = new allCategories();
    Categories.nombre_category = element['NOMBRE_CATEGORIA'];
    Categories.id_category = element['ID_CATEGORIA'];
    categorias.push(Categories);
  });

  logger.debug('Los nombres de las categorias son:' +categorias);

  pool.end();
  promisePoolEnd();


   return categorias;
};

async function allusersBBDD(){

  logger.debug("Consulta el nombre de todos los usuarios"); 
   let usersBBDD = [];
    
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
    
  const query = "Select ID_USER, USER_MAIL, ESTADO, FECHA_CREACION, USER_NAME, PERFIL from USERS_GOTHAM";
  const result = await promiseQuery(query) // use in async function

  result.forEach(element => {
    const Users = new allUsers();
    Users.id_user = element['ID_USER'];
    Users.user_mail = element['USER_MAIL'];
    Users.estado = element['ESTADO'];
    Users.fecha_creacion = element['FECHA_CREACION'];
    Users.user_name = element['USER_NAME'];
    Users.perfil = element['PERFIL'];
    usersBBDD.push(Users);
  });

  pool.end();
  promisePoolEnd();


   return usersBBDD;
};

async function altaportalesBBDD(params){
  logger.debug("Consulta los datos de un usuario"); 
   let usersBBDD = [];
    
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
    
  const query = "Select ID, ID_PORTAL, PORTAL, ID_SALESFORCE, NOMBRE_PROMOCION, FECHA_CREACION, USER, NOMBRE, EMAIL, MENSAJE, REFERENCIA, FECHA_PROCESADO, FECHA_LEAD, IP, NAVEGADOR, VALIDACION_SPAM, IDIOMA, URL_PORTAL from PORTALES_LEADS where ID = '"+params+"'";
  const result = await promiseQuery(query) // use in async function

  console.log('LA QUERY ES:' +query)

  result.forEach(element => {
    console.log('EL ID ES:' +element['ID']);
    const Users = new UserAltaPortales();
    Users.id = element['ID'];
    Users.id_portal = element['ID_PORTAL'];
    Users.portal = element['PORTAL'];
    Users.id_salesforce = element['ID_SALESFORCE'];
    Users.nombre_promocion = element['NOMBRE_PROMOCION'];
    Users.fecha_creacion = element['FECHA_CREACION'];
    Users.user = element['USER'];
    Users.nombre = element['NOMBRE'];
    Users.email = element['EMAIL'];
    Users.mensaje = element['MENSAJE'];
    Users.referencia = element['REFERENCIA'];
    Users.fecha_procesado = element['FECHA_PROCESADO'];
    Users.fecha_lead = element['FECHA_LEAD'];
    Users.ip = element['IP'];
    Users.navegador = element['NAVEGADOR'];
    Users.validacion_spam = element['VALIDACION_SPAM'];
    Users.idioma = element['IDIOMA'];
    Users.url_portal = element['URL_PORTAL']
    usersBBDD.push(Users);
  });

  pool.end();
  promisePoolEnd();


   return usersBBDD;
}

async function rutafiletalkdeskBBDD(params){
  logger.debug("Consulta los datos de rutas de ficheros de Talkdesk"); 
  
  let datosllamada = [];
    
  const pool = mysql.createPool(databaseConfigTracking);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
    
  const query = "select Destino_file__c, CreatedDate, talkdesk__CallDuration__c from SFDC_talkdesk__Talkdesk_Activity__c where Id = '"+params+"'";
  const result = await promiseQuery(query) // use in async function

  console.log('LA QUERY ES:' +query)

  const llamadaDatos = new datosLLamada();
  llamadaDatos.Destino_file__c = result[0].Destino_file__c;
  llamadaDatos.CreatedDate = result[0].CreatedDate;
  llamadaDatos.talkdesk__CallDuration__c = result[0].talkdesk__CallDuration__c;
  datosllamada.push(llamadaDatos);

  pool.end();
  promisePoolEnd();

  
   console.log("LA RUTA ES: "+datosllamada)
   return datosllamada;
}




async function mensajesspamalta(params){
  logger.debug("Consulta los datos de un usuario"); 
   let usersBBDD = [];
    
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
    
  const query = "Select ID, ID_PORTAL, PORTAL, ID_SALESFORCE, NOMBRE_PROMOCION, FECHA_CREACION, USER, NOMBRE, EMAIL, MENSAJE, REFERENCIA, FECHA_PROCESADO, FECHA_LEAD, IP, NAVEGADOR, VALIDACION_SPAM, IDIOMA from PORTALES_LEADS where FECHA_LEAD LIKE '%"+params+"%' AND VALIDACION_SPAM = 'NoValidadoPorMaquina'";
  const result = await promiseQuery(query) // use in async function

  console.log('LA QUERY ES:' +query)

  result.forEach(element => {
    console.log('EL ID ES:' +element['ID']);
    const Users = new UserAltaPortales();
    Users.id = element['ID'];
    Users.id_portal = element['ID_PORTAL'];
    Users.portal = element['PORTAL'];
    Users.id_salesforce = element['ID_SALESFORCE'];
    Users.nombre_promocion = element['NOMBRE_PROMOCION'];
    Users.fecha_creacion = element['FECHA_CREACION'];
    Users.user = element['USER'];
    Users.nombre = element['NOMBRE'];
    Users.email = element['EMAIL'];
    Users.mensaje = element['MENSAJE'];
    Users.referencia = element['REFERENCIA'];
    Users.fecha_procesado = element['FECHA_PROCESADO'];
    Users.fecha_lead = element['FECHA_LEAD'];
    Users.ip = element['IP'];
    Users.navegador = element['NAVEGADOR'];
    Users.validacion_spam = element['VALIDACION_SPAM'];
    Users.idioma = element['IDIOMA'];
    console.log(Users);
    usersBBDD.push(Users);
  });

  pool.end();
  promisePoolEnd();


   return usersBBDD;
}

async function altaLeadsBBDD(params){
  logger.debug("Consulta los datos de un usuario"); 
   let usersLeadsBBDD = [];

    
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
    
  const query = "Select ID, ID_PORTAL, PORTAL, ID_SALESFORCE, NOMBRE_PROMOCION, FECHA_CREACION, USER, NOMBRE, EMAIL, MENSAJE, REFERENCIA, FECHA_PROCESADO, FECHA_LEAD, IP, NAVEGADOR, VALIDACION_SPAM, IDIOMA from PORTALES_LEADS where ID_PORTAL = '"+params+"'";
  const result = await promiseQuery(query) // use in async function

  console.log('LA QUERY ES:' +query)

  result.forEach(element => {
    console.log('EL ID ES:' +element['ID']);
    const Users = new UserAltaPortales();
    Users.id = element['ID'];
    Users.id_portal = element['ID_PORTAL'];
    Users.portal = element['PORTAL'];
    Users.id_salesforce = element['ID_SALESFORCE'];
    Users.nombre_promocion = element['NOMBRE_PROMOCION'];
    Users.fecha_creacion = element['FECHA_CREACION'];
    Users.user = element['USER'];
    Users.nombre = element['NOMBRE'];
    Users.email = element['EMAIL'];
    Users.mensaje = element['MENSAJE'];
    Users.referencia = element['REFERENCIA'];
    Users.fecha_procesado = element['FECHA_PROCESADO'];
    Users.fecha_lead = element['FECHA_LEAD'];
    Users.ip = element['IP'];
    Users.navegador = element['NAVEGADOR'];
    Users.validacion_spam = element['VALIDACION_SPAM'];
    Users.idioma = element['IDIOMA'];
    usersLeadsBBDD.push(Users);
  });

  pool.end();
  promisePoolEnd();


   return usersLeadsBBDD;
}

async function addUsersBBDD(params){
  logger.debug("Añadiendo Usuario en BBDD");
  var ret = {};
        logger.debug(JSON.stringify(params))
        const pool = mysql.createPool(databaseConfig);
        const promiseQuery = promisify(pool.query).bind(pool);
        const promisePoolEnd = promisify(pool.end).bind(pool);

        const query = "INSERT INTO USERS_GOTHAM (USER_MAIL, ESTADO, USER_NAME, PERFIL) VALUES ('"+params.emailUser+"','"+params.estadoUser+"','"+params.nameUser+"','"+params.perfilUser+"')";
        const result = await promiseQuery(query)
        
        pool.end();
        promisePoolEnd();

  return result;
};

async function addUsersPortalesBBDD(params){
  logger.debug("Añadiendo Usuario en BBDD");
  var ret = {};
        logger.debug(JSON.stringify(params))
        const pool = mysql.createPool(databaseConfig);
        const promiseQuery = promisify(pool.query).bind(pool);
        const promisePoolEnd = promisify(pool.end).bind(pool);

        const query = "INSERT INTO PORTALES_PROMOCIONES (ID_PORTAL, PORTAL, ID_SALESFORCE, NOMBRE_PROMOCION, USER, IP, NAVEGADOR, URL_PORTAL) VALUES ('"+params.idPortal+"','"+params.portal+"','"+params.idSalesForce+"','"+params.nombrePromocion+"','"+params.user+"','"+params.currentIp+"','"+params.navigator+"','"+params.url_portal+"')";
        const result = await promiseQuery(query)
        
        pool.end();
        promisePoolEnd();

  return result;
}

async function addProductsBBDD(params){
  logger.debug("Añadiendo Producto en BBDD");
  var ret = {};
        logger.debug(JSON.stringify(params))
        const pool = mysql.createPool(databaseConfig);
        const promiseQuery = promisify(pool.query).bind(pool);
        const promisePoolEnd = promisify(pool.end).bind(pool);

        const query = "INSERT INTO PRODUCTOS_LIVING (NOMBRE_PRODUCTO, ID_CATEGORIA, RUTA_FICHERO, NOMBREPDF, NAME, ORDEN) VALUES ('"+params.nombreproducto+"',"+params.idcategoria+",'"+params.rutafichero+"','"+params.nombrepdf+"','"+params.nameproduct+"',"+params.ordenproduct+")";
        const result = await promiseQuery(query)
        
        pool.end();
        promisePoolEnd();

  return result;
};

async function addMailingBBDD(params){
  logger.debug("Añadiendo Mailing en BBDD");
  var ret = {};
        logger.debug(JSON.stringify(params))
        const pool = mysql.createPool(databaseConfig);
        const promiseQuery = promisify(pool.query).bind(pool);
        const promisePoolEnd = promisify(pool.end).bind(pool);

        const query = "INSERT INTO MAILINGS (ABIERTO, FECHA_ABIERTO, CIUDAD, IP, NAVEGADOR, ID_CORREO, ORIGEN) VALUES ('"+params.abierto+"','"+params.today+"','"+params.ciudad+"','"+params.currentIp+"','"+params.navigator+"','"+params.id_correo+"','"+params.origen+"')";
        console.log(query);
        const result = await promiseQuery(query)
        
        pool.end();
        promisePoolEnd();

  return result;
};



async function addChangesBBDD(params){
  logger.debug("Añadiendo Cambios en BBDD");
  var ret = {};
        logger.debug(JSON.stringify(params))
        const pool = mysql.createPool(databaseConfig);
        const promiseQuery = promisify(pool.query).bind(pool);
        const promisePoolEnd = promisify(pool.end).bind(pool);

        const query = "INSERT INTO CAMBIOS_RPA_PRINEX (ID_PRINEX, CAMPO_PRINEX, VALOR_NUEVO_PRINEX, VALOR_ANTIGUO_PRINEX, USER_MODIFICACION, APLICACION) VALUES ("+params.idPrinex+",'"+params.campoPrinex+"','"+params.valornuevo+"','"+params.valorantiguo+"','"+params.user+"','"+params.aplicacion+"')";
        const result = await promiseQuery(query)
        
        pool.end();
        promisePoolEnd();

  return result;
};

async function deleteProductsBBDD(params){
  logger.debug("Eliminando Producto en BBDD");
  var ret = {};
        logger.debug(JSON.stringify(params))
        const pool = mysql.createPool(databaseConfig);
        const promiseQuery = promisify(pool.query).bind(pool);
        const promisePoolEnd = promisify(pool.end).bind(pool);

        const query = "DELETE FROM `PRODUCTOS_LIVING` WHERE RUTA_FICHERO =  '"+params.urlRemove+"'";
        const result = await promiseQuery(query)
        
        pool.end();
        promisePoolEnd();

  return result;
};





async function updateUsersBBDD(params){
  logger.debug("Actualizando Usuario en BBDD");
  var ret = {};
        logger.debug(JSON.stringify(params))
        const pool = mysql.createPool(databaseConfig);
        const promiseQuery = promisify(pool.query).bind(pool);
        const promisePoolEnd = promisify(pool.end).bind(pool);

        const query = "UPDATE USERS_GOTHAM SET USER_MAIL='"+params.emailUser+"', ESTADO='"+params.estadoUser+"', USER_NAME='"+params.nameUser+"', PERFIL='"+params.perfilUser+"' WHERE ID_USER = "+params.idUser+""

        const result = await promiseQuery(query)
        
        pool.end();
        promisePoolEnd();

  return result;
}

async function updateSpamBBDD(params){
  logger.debug("Actualizando Spam en BBDD");
  var ret = {};
        logger.debug(JSON.stringify(params))
        const pool = mysql.createPool(databaseConfig);
        const promiseQuery = promisify(pool.query).bind(pool);
        const promisePoolEnd = promisify(pool.end).bind(pool);
        let valorSpam = '';

        console.log("El user es"+params.user);

        if(params.valueSpam == 'No Validado Por Maquina')
        {
          valorSpam = "NoValidadoPorMaquina";
        }
        if(params.valueSpam == 'Spam')
        {
          valorSpam = "NoValidadoPorUsuario";
        }
        if(params.valueSpam == 'Validado Por Usuario')
        {
          valorSpam = "ValidadoPorUsuario";
        }

        const query = "UPDATE PORTALES_LEADS SET USER='"+params.user+"', IP='"+params.currentIp+"', NAVEGADOR='"+params.navigator+"',  VALIDACION_SPAM='"+valorSpam+"' WHERE ID = "+params.idSpam+""

        const result = await promiseQuery(query)
        
        pool.end();
        promisePoolEnd();

  return result;
}

async function updatesalesforceBBDD(params){
let res = true;
let usersSALESFORCE = [];

const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
let values = JSON.stringify(params);
console.log("LOS VALUES SON:" +values);
let estado = params.name.args[0];
let fechaactual = params.fechaactual;
let id = params.id;
console.log('El estado es '+estado);
console.log('El Id es '+id);

const conn = new jsforce.Connection();
await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 

    
     conn.sobject("Product2").update({ 
       Id : id,
       Estado_Comercial_Salesforce__c : estado,
       Fecha_de_entrega__c : fechaactual,
     }, function(err, ret) {
       if (err || !ret.success) { return console.error(err, ret); }
       console.log('Updated Successfully : ' + ret.id);
      res = ret.success;
       console.log('el res correcto' +res);
     })
    


)));

  return res;  
}

async function updateoportunitysalesforceBBDD(params){
  let res = true;
  let usersSALESFORCE = [];
  
  const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
  const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
  const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
  let values = JSON.stringify(params);
  console.log("LOS VALUES SON:" +values);
  let id = params.id;
  console.log('El Id es '+id);
  
  const conn = new jsforce.Connection();
  await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
  
      
       conn.sobject("Opportunity").update({ 
         Id : id,
         StageName : "Ganada",
         FechaGanada__c : new Date().toISOString().split('T')[0],
       }, function(err, ret) {
         if (err || !ret.success) { return console.error(err, ret); }
         console.log('Updated Successfully : ' + ret.id);
        res = ret.success;
         console.log('el res correcto' +res);
       })
      
  
  
  )));
  
    return res;  
  }








async function updateAltaPortalesBBDD(params){

  logger.debug("Actualizando Usuario en BBDD");
  var ret = {};
        logger.debug(JSON.stringify(params))
        const pool = mysql.createPool(databaseConfig);
        const promiseQuery = promisify(pool.query).bind(pool);
        const promisePoolEnd = promisify(pool.end).bind(pool);

        let values = JSON.stringify(params);
        console.log("LOS VALUES DE LA ACTUALIZACION SON: "+values);
        let idSalesForce = params.idSalesForce;
        let idUserPortal = params.id;
        let nombrepromocion = params.nombrePromocion;
        let user = params.user;
        let currentIp = params.currentIp;
        let navigator = params.navigator;
        let urlportal = params.url_site;

        const query = "UPDATE PORTALES_LEADS SET ID_SALESFORCE='"+idSalesForce+"', NOMBRE_PROMOCION='"+nombrepromocion+"', USER='"+user+"',IP='"+currentIp+"',NAVEGADOR='"+navigator+"'  WHERE ID = "+idUserPortal+""

        const result = await promiseQuery(query)
        
        pool.end();
        promisePoolEnd();
      

  return result;
}



// let res = true;
// let usersSALESFORCE = [];

// const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
// const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
// const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
// let values = JSON.stringify(params);
// console.log("LOS VALUES DE LA ACTUALIZACION SON: "+values);
// let idSalesForce = params.idSalesForce;
// let idUserPortal = "'2186'";//params.id;
// let nombrepromocion = params.nombrePromocion;
// let user = params.user;


// const conn = new jsforce.Connection();
// await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 

    
//      conn.sobject("PORTALES_LEADS").update({ 
//        ID : idUserPortal,
//        ID_SALESFORCE : idSalesForce,
//        NOMBRE_PROMOCION : nombrepromocion,
//        USER : user
//      }, function(err, ret) {
//        if (err || !ret.success) { return console.error(err, ret); }
//        console.log('Updated Successfully : ' + ret.id);
//        res = ret.success;
//        console.log('el res correcto' +res);
//      })
    


// )));

//   return res;  


async function updatesalesforceAreaPrivadaBBDD(params){
  let res = true;
  let usersSALESFORCE = [];
  
  const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
  const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
  const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
  let values = JSON.stringify(params);
  console.log(values);
  let id = params.id;
  console.log('El Id es '+id);
  
  const conn = new jsforce.Connection();
  await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
  
      
       conn.sobject("Account").update({ 
         Id : id,
         Registrado_Area_Privada__c : true,
         Fecha_alta_en_area_privada__c : new Date().toISOString(),
       }, function(err, ret) {
         if (err || !ret.success) { return console.error(err, ret); }
         console.log('Updated Successfully : ' + ret.id);
        res = ret.success;
         console.log('el res correcto' +res);
       })
      
  
  
  )));
  
    return res;  
  }

async function connectSalesForce(params){
  
let result;
let usersSALESFORCE = [];

const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';

const conn = new jsforce.Connection({
  loginUrl: saleforeceConfig.hostnamelibreriaJSForceConnect
})
await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 

    conn.query("SELECT Id, Name, Phone, PersonMobilePhone, Registrado_Area_Privada__C, PersonEmail, DNI__c, Fecha_alta_en_area_privada__c, Idioma__c FROM Account where Cod_Prinex__c = '"+params+"'", function(err, res) {
      if (err) { return console.error(err); }

      if(res.records.length > 0)
      {
        console.log(res.records[0].Name);
        console.log(res.records[0].PersonEmail);
        console.log(res.records[0].Fecha_alta_en_area_privada__c);
        console.log(res.records[0].Id);
  
        const userNew = new usuarioSalesForce();
        userNew.name = res.records[0].Name;
        userNew.dni = res.records[0].DNI__c;
        userNew.email = res.records[0].PersonEmail;
        userNew.fechaareaprivada = res.records[0].Fecha_alta_en_area_privada__c;
        userNew.Id = res.records[0].Id;
        userNew.telefono1 = res.records[0].Phone;
        userNew.telefono2 = res.records[0].PersonMobilePhone;
        userNew.Idioma = res.records[0].Idioma__c;
        usersSALESFORCE.push(userNew);
  
        console.log(usersSALESFORCE);
      }
     
      return usersSALESFORCE; 
    })


)));

  return usersSALESFORCE;  

}

async function getcodigoUserSalesForce(params){
  
  let result;
  let usersSALESFORCE = [];
  
  const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
  const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
  const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
  
  const conn = new jsforce.Connection();
  await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
  
      conn.query("SELECT Id, Name, Phone, PersonMobilePhone,  Registrado_Area_Privada__C, PersonEmail, DNI__c, Fecha_alta_en_area_privada__c, Idioma__c FROM Account where DNI__c = '"+params+"'", function(err, res) {
        if (err) { return console.error("EL ERROR DE CONNECT ES" +err); }
        console.log(res.records[0]);
        if(res.records.length === 0){
          result = usersSALESFORCE;
        }
        else
        {
          // console.log(res.record[0]);
            const userNew = new usuarioSalesForce();
            userNew.name = res.records[0].Name;
            userNew.email = res.records[0].PersonEmail;
            userNew.fechaareaprivada = res.records[0].Fecha_alta_en_area_privada__c;
            userNew.telefono1 = res.records[0].Phone;
            userNew.telefono2 = res.records[0].PersonMobilePhone;
            userNew.Id = res.records[0].Id;
            userNew.Idioma = res.records[0].Idioma__c;
            usersSALESFORCE.push(userNew);

          result = usersSALESFORCE;
        }
      
  
        console.log(result);
        return result; 
      })
  
  
  )));
  
    return result;  
  
}

async function getidDatosUserSalesForce(params){
  
  let result;
  let usersSALESFORCE = [];
  
  const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
  const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
  const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
  
  const conn = new jsforce.Connection();
  await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
  
      conn.query("SELECT Id, Name, Phone, PersonMobilePhone,  Registrado_Area_Privada__c, PersonEmail, DNI__c, Fecha_alta_en_area_privada__c, Idioma__c FROM Account where Id = '"+params+"'", function(err, res) {
        if (err) { return console.error(err); }
  
        if(res.records.length === 0){
          result = usersSALESFORCE;
        }
        else
        {
          for(let i = 0; i < res.records.length; i++)
          {
            const userNew = new usuarioSalesForce();
            userNew.name = res.records[0].Name;
            userNew.email = res.records[0].PersonEmail;
            userNew.fechaareaprivada = res.records[0].Fecha_alta_en_area_privada__c;
            userNew.registrado = res.records[0].Registrado_Area_Privada__c;
            console.log("EL ESTADO ES: "+userNew.registrado);
            userNew.telefono1 = res.records[0].Phone;
            userNew.telefono2 = res.records[0].PersonMobilePhone;
            userNew.Id = res.records[0].Id;
            userNew.Idioma = res.records[0].Idioma__c;
            usersSALESFORCE.push(userNew);
          }

          result = usersSALESFORCE;
        }
      
  
        console.log(result);
        return result; 
      })
  
  
  )));
  
    return result;  
  
}



  async function getcodigoInmuebleSalesForce(params){
  
    let result = [];
    
    const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
    const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
    const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
    console.log("select Id, Name, Promocion__c, Estado_inmuebles__c, EstadoDeContratacion__c, Interes__c, StageName from Opportunity where (StageName = 'Ganada' or StageName = 'Reserva')  and AccountId = '"+params+"'");
    const conn = new jsforce.Connection();
    await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
      
        conn.query("select Id, Name, Promocion__c, Estado_inmuebles__c, EstadoDeContratacion__c, Interes__c, StageName from Opportunity where (StageName = 'Ganada' or StageName = 'Reserva')  and AccountId = '"+params+"'", function(err, res) {
          if (err) { return console.error(err); }

          if(res.records.length == 0)
          {
            result.push(0);
          }
          else
          {
            for(let i = 0; i<res.records.length; i++)
            {
              let datospromociones = new promociones();
              datospromociones.id = res.records[i].Id;
              datospromociones.name = res.records[i].Name;
              datospromociones.promocion__c = res.records[i].Promocion__c;
              datospromociones.estado_inmuebles__c = res.records[i].Estado_inmuebles__c;
              datospromociones.estadodecontratacion__c = res.records[i].EstadoDeContratacion__c;
              datospromociones.interes__c = res.records[i].Interes__c;
              datospromociones.stagename = res.records[i].StageName;
              result.push(datospromociones);
            }           
          }   
          console.log(result);
          return result; 
        })
    
    
    )));
    
      return result;  
    
  }

  async function getPromotionName(params){
    
    let name = "";
    
    const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
    const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
    const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
    console.log("select Name from Promocion__c where Id = '"+params+"'");
    const conn = new jsforce.Connection();
    await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
      
        conn.query("select Name from Promocion__c where Id = '"+params+"'", function(err, res) {
          if (err) { return console.error(err); }

          if(res.records.length == 0)
          {
            name = "";
          }
          else
          {
            for(let i = 0; i<res.records.length; i++)
            {
              name = res.records[i].Name;
            }           
          }   
          console.log(name);
          return name; 
        })
    
    
    )));
    
      return name; 

  }

  async function getcodigoInmuebleSecundarioSalesForce(params){
    
    let result;
    
    const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
    const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
    const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
    
    const conn = new jsforce.Connection();
    await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
    
        conn.query("SELECT Id, AccountID FROM Opportunity WHERE StageName IN ('Reserva', 'Ganada') and Id in (SELECT Oportunidad__c FROM Interes__c where Cliente__c = '"+params+"')", function(err, res) {
          if (err) { return console.error(err); }

          if(res.records.length == 0)
          {
            result = 0;
          }
          else
          {
            result = res.records[0].Id;
          }
    
          
    
          console.log(result);
          return result; 
        })
    
    
    )));
    
      return result;

  }

  async function getProductCodeSalesForce(params){
  
    let result;
    
    const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
    const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
    const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
    
    const conn = new jsforce.Connection();
    await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
    
        conn.query("SELECT Oportunidad__c FROM Product2 where productcode = '"+params+"'", function(err, res) {
          if (err) { return console.error(err); }

            result = res.records[0].Oportunidad__c;
            
          console.log(result);
          return result; 
        })
    
    
    )));
    
      return result;  
    
  }

 

async function getdatosInmuebleSalesForce(params){
  
    let result;
    let datosInmueblesSALESFORCE = [];
    
    const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
    const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
    const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
    console.log("select Id, Name, PVP__c, Nombre_Promocion__c, Estado_Comercial__c, Estado_Comercial_Salesforce__c, Fecha_de_entrega__c from Product2 where Oportunidad__c in ('"+params+"')");
    const conn = new jsforce.Connection();
    await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
    
        conn.query("select Id, Name, PVP__c, Nombre_Promocion__c, Estado_Comercial__c, Estado_Comercial_Salesforce__c, Fecha_de_entrega__c  from Product2 where Oportunidad__c = '"+params+"'", function(err, res) {
          if (err) { return console.error(err); }

          for(let i = 0; i < res.records.length; i++)
          {
            let datosSalesForce = new inmueblesSalesForce();
            datosSalesForce.idInmueble = res.records[i].Id;
            datosSalesForce.nameInmueble = res.records[i].Name;
            datosSalesForce.pvp = res.records[i].PVP__c;
            datosSalesForce.nombrePromocion = res.records[i].Nombre_Promocion__c;
            datosSalesForce.estadoComercial = res.records[i].Estado_Comercial__c;
            datosSalesForce.estadoComercialSalesForce = res.records[i].Estado_Comercial_Salesforce__c;
            datosSalesForce.Fecha_de_entrega__c = res.records[i].Fecha_de_entrega__c;
            datosInmueblesSALESFORCE.push(datosSalesForce);
          }
    
          result = datosInmueblesSALESFORCE;
    
          console.log(res);
          return result; 
        })
    
    
    )));
    
      return result;  
    
}

async function getdatosUsuariosOportunidadSalesForce(params){
  
    let result;
    let usersSALESFORCE = [];
    
    const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
    const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
    const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
    
    const conn = new jsforce.Connection();
    await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
    
        conn.query("SELECT Cliente__c,Oportunidad__c FROM Interes__c where Oportunidad__c = '"+params+"'", function(err, res) {
          if (err) { return console.error(err); }
    
          if(res.records.length === 0){
            result = usersSALESFORCE;
          }
          else
          {
            for(let i = 0; i < res.records.length; i++)
            {
              const clienteNew = new clienteSalesForce();
              clienteNew.cliente = res.records[i].Cliente__c;
              usersSALESFORCE.push(clienteNew);
            }
  
            result = usersSALESFORCE;
          }
        
    
          console.log(result);
          return result; 
        })
    
    
    )));
    
      return result;  
    
}

async function getnamesUsuariosOportunidadSalesForce(params){
  
      let result;
      let usersSALESFORCE = [];
      
      const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
      const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
      const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
      
      const conn = new jsforce.Connection();
      await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
      
          conn.query("SELECT Name,Registrado_Area_Privada__c FROM Account where Id in("+params+")", function(err, res) {
            if (err) { return console.error(err); }
      
            if(res.records.length === 0){
              result = usersSALESFORCE;
            }
            else
            {
              for(let i = 0; i < res.records.length; i++)
              {
                const clienteNameNew = new clienteNamesSalesForce();
                clienteNameNew.name = res.records[i].Name;
                usersSALESFORCE.push(clienteNameNew);
              }
    
              result = usersSALESFORCE;
            }
          
      
            console.log(result);
            return result; 
          })
      
      
      )));
      
        return result;
}

async function getoportunidadesganadasSalesForce(params){
  let result;
      let usersSALESFORCE = [];
      
      const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
      const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
      const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
      console.log("Select Id, AccountId, StageName FROM Opportunity where Promocion__c IN ('"+params+"') AND (StageName = 'GANADA' OR StageName = 'RESERVA')");
      
      const conn = new jsforce.Connection();
      await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 

          conn.query("Select Id, AccountId, StageName FROM Opportunity where Promocion__c IN ('"+params+"') AND (StageName = 'GANADA' OR StageName = 'RESERVA')", function(err, res) {

            if (err) { return console.error(err); }
      
            if(res.records.length === 0){
              result = usersSALESFORCE;
            }
            else
            {
              for(let i = 0; i < res.records.length; i++)
              {
                const clienteNameNew = new clienteOportunidadesSalesForce();
                clienteNameNew.id = res.records[i].Id;
                clienteNameNew.account_id = res.records[i].AccountId;
                usersSALESFORCE.push(clienteNameNew);
              }
    
              result = usersSALESFORCE;
            }
          
      
            console.log(result);
            return result; 
          })
      
      
      )));
      
  return result;
}

async function getoportunidadesbyuserSalesForce(params){
  let result;
      let usersSALESFORCE = [];
      
      const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
      const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
      const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
      const query = "select Id, Name, AccountId, Promocion__r.Name, StageName from Opportunity where AccountId = '" + params + "' AND (StageName = 'GANADA' OR StageName = 'RESERVA')";
      console.log(query);
      
      const conn = new jsforce.Connection();
      await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
          conn.query(query, function(err, res) {
            if (err) { 
              return console.error(err); 
            }
            if(res.records.length === 0){
              result = usersSALESFORCE;
            }
            else
            {
              for(let i = 0; i < res.records.length; i++)
              {
                const oportunidad = new oportunidadesUserSalesForce();
                oportunidad.id = res.records[i].Id;
                oportunidad.name = res.records[i].Name;
                oportunidad.promocionName = res.records[i].Promocion__r.Name;
                oportunidad.StageName = res.records[i].StageName;
                usersSALESFORCE.push(oportunidad);
              }
    
              result = usersSALESFORCE;
            }
      
            console.log(result);
            return result; 
          })
      )));
      
  return result;
}

async function getencuestasoportunidadSalesForce(accountId, opportunityId) {
  let result;
      let usersSALESFORCE = [];
      
      const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
      const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
      const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
      const query = `select Id, GetFeedback_Aut__SurveyId__c, GetFeedback_Aut__Survey__r.Name, CreatedDate, GetFeedback_Aut__CompletionDate__c
                     from GetFeedback_Aut__Response__c
                     where GetFeedback_Aut__Account__c = '` + accountId + `'
                            and GetFeedback_Aut__Opportunity__c = '` + opportunityId + `'
                            and GetFeedback_Aut__SurveyId__c  in (695203, 711087, 695205, 711089)`;

      console.log(query);
      
      const conn = new jsforce.Connection();
      await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
          conn.query(query, function(err, res) {
            if (err) { 
              return console.error(err); 
            }
            if(res.records.length === 0){
              result = usersSALESFORCE;
            }
            else
            {
              for(let i = 0; i < res.records.length; i++)
              {
                const encuesta = new encuestasOportunidadSalesForce(res.records[i].Id, res.records[i].GetFeedback_Aut__SurveyId__c, res.records[i].GetFeedback_Aut__Survey__r.Name, res.records[i].CreatedDate, res.records[i].GetFeedback_Aut__CompletionDate__c);
                usersSALESFORCE.push(encuesta);
              }
    
              result = usersSALESFORCE;
            }
      
            console.log(result);
            return result; 
          })
      )));
      
  return result;
}

async function getIdgetAlloportunidadesganadasSalesForce(){
  let result;
  let usersSALESFORCE = [];
      
      const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
      const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
      const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
      
      const conn = new jsforce.Connection();
      await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 

        

      
          //conn.query("Select Id, AccountId FROM Opportunity where StageName = 'GANADA' OR StageName = 'RESERVA'", function(err, res) {
            conn.query("Select Id, AccountId, StageName FROM Opportunity where Promocion__c NOT IN ('a0P0Y00000kchipUAA','a0P1v00000qILXcEAO','a0P0Y00000mO8dsUAC','a0P1v00000pkx6pEAA','a0P1v00000tCldPEAS','a0P0Y000006F2eoUAC','a0P0Y00000o5YCxUAM','a0P1v00000t081lEAA','a0P1v00000t082PEAQ','a0P0Y000006GQSoUAO','a0P1v00000taAlLEAU','a0P0Y000002vhoRUAQ','a0P0Y000002vhoCUAQ','a0P1v00000qwIdwEAE','a0P0Y00000lYSmJUAW','a0P0Y000006GQTtUAO') AND (StageName = 'GANADA' OR StageName = 'RESERVA')", function(err, res) {
            if (err) { return console.error(err); }
      
            if(res.records.length === 0){
              result = usersSALESFORCE;
            }
            else
            {
              
              for(let i = 0; i < res.records.length; i++)
              {
                const clienteNameNew = new clienteOportunidadesSalesForce();
                clienteNameNew.id = res.records[i].Id;
                clienteNameNew.account_id = res.records[i].AccountId;
                usersSALESFORCE.push(clienteNameNew);
              }
              result = usersSALESFORCE;
            }
          
      
            console.log(result);
            return result; 
          })
      
      
      )));
      
  return result;

}

async function getEmailsSalesForce(params){
  let result;
  let usersSALESFORCE = [];
  
  const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
  const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
  const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
  
  const conn = new jsforce.Connection();
  await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 

    

  
      conn.query("Select Id, PersonEmail, Registrado_Area_Privada__c from Account Where Id IN ('"+params+"') and Registrado_Area_Privada__c = false", function(err, res) {
        if (err) { return console.error(err); }
  
        if(res.records.length === 0){
          result = 0;
        }
        else
        {
          for(let i = 0; i < res.records.length; i++)
          {
            const clienteNameNew = new clienteEmailsSalesForce();
            clienteNameNew.id = res.records[i].Id;
            clienteNameNew.email = res.records[i].PersonEmail;
            clienteNameNew.registrado = res.records[i].Registrado_Area_Privada__c;
            usersSALESFORCE.push(clienteNameNew);
          }

          result = usersSALESFORCE;
        }
      
  
        console.log(result);
        return result; 
      })
  
  
  )));
  
return result;

}



async function getEmailsUsuariosSalesForce(params){
  let result;
  let usersSALESFORCE = [];
  
  const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
  const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
  const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
  
  const conn = new jsforce.Connection();
  await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 

    

  
      conn.query("Select Id, PersonEmail, Registrado_Area_Privada__c, Fecha_alta_en_area_privada__c from Account Where Id IN ('"+params+"') and Registrado_Area_Privada__c = true", function(err, res) {
        if (err) { return console.error(err); }
  
        if(res.records.length === 0){
          result = 0;
        }
        else
        {
          for(let i = 0; i < res.records.length; i++)
          {
            const clienteNameNew = new clienteEmailsSalesForce();
            clienteNameNew.id = res.records[i].Id;
            clienteNameNew.email = res.records[i].PersonEmail;
            clienteNameNew.registrado = res.records[i].Registrado_Area_Privada__c;
            clienteNameNew.fecha = res.records[i].Fecha_alta_en_area_privada__c;
            usersSALESFORCE.push(clienteNameNew);
          }

          result = usersSALESFORCE;
        }
      
  
        console.log(result);
        return result; 
      })
  
  
  )));
  
return result;

}



async function getIdsSalesForce(params){
  let result;
  let usersSALESFORCE = [];
  
  const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
  const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
  const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
  
  const conn = new jsforce.Connection();
  await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 

    
      
  
      conn.query("SELECT Id, Cliente__C  FROM Interes__c WHERE Oportunidad__c IN ('"+params+"')", function(err, res) {
        if (err) { return console.error(err); }
  
        if(res.records.length === 0){
          result = 0;
        }
        else
        {
          for(let i = 0; i < res.records.length; i++)
          {
            const clienteNameNew = new clienteEmailsSalesForce();
            let a = JSON.stringify(res.records[i])            
            clienteNameNew.id = res.records[i].Cliente__c;
            usersSALESFORCE.push(clienteNameNew);
          }

          result = usersSALESFORCE;
        }
      
  
        console.log(result);
        return result; 
      })
  
  
  )));
  
return result;

}

async function getValuePromotions(){
  
  let result;
  let datosPromocionesSALESFORCE = [];
  
  const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
  const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
  const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
  console.log('AAAAA');
  const conn = new jsforce.Connection();
  await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 
  
      conn.query("Select Name, Id, IdentificadorPrinex__c from Promocion__C order by Name", function(err, res) {
        if (err) { return console.error("ATENTOS EL ERROR ES" +err); }

        for(let i = 0; i < res.records.length; i++)
        {
          let datosSalesForce = new promotionSalesForce();
          datosSalesForce.name = res.records[i].Name;
          datosSalesForce.id = res.records[i].Id;
          datosSalesForce.IdentificadorPrinex__c = res.records[i].IdentificadorPrinex__c;
          console.log('EL RESULTADO ES: ' +datosSalesForce.name);
          datosPromocionesSALESFORCE.push(datosSalesForce);
        }
  
        result = datosPromocionesSALESFORCE;
  
        
        return result; 
      })
  
  
  )));
  
    return result;  
  
}


async function getUserSALESFORCE(fechainicial, fechafinal){
  let result;
  let usersSALESFORCE = [];
  
  const username = saleforeceConfig.username; //'apiuser@aedashomes.com';
  const password = saleforeceConfig.password; //'6SuTG9CsNpBKpcWb';
  const securityToken = saleforeceConfig.securityToken; //'vfUy1YgyPEpTCsrhJd3tkmLF0';
  
  const conn = new jsforce.Connection();
  await new Promise((resolve, reject) => conn.login(username, password, (e, r) => e ? reject(r) : resolve( 

    

  
      conn.query("SELECT Id,Name,PersonEmail FROM Account where (Fecha_alta_en_area_privada__c  > "+fechainicial+"T00:00:00Z and Fecha_alta_en_area_privada__c  < "+fechafinal+"T00:00:00Z)", function(err, res) {
        if (err) { return console.error(err); }
  
        if(res.records.length === 0){
          result = 0;
        }
        else
        {
          for(let i = 0; i < res.records.length; i++)
          {
            const clienteNameNew = new usersSalesForce();
            clienteNameNew.id = res.records[i].Id;
            clienteNameNew.email = res.records[i].PersonEmail;
            clienteNameNew.name = res.records[i].Name;
            usersSALESFORCE.push(clienteNameNew);
          }

          result = usersSALESFORCE;
        }
      
  
        console.log(result);
        return result; 
      })
  
  
  )));
  
return result;

}
  











module.exports = {
  authenticateUserEM,
  authenticate,
  nameProducts,
  nameAllProducts,
  perfil,
  sendEmail,
  nameCategory,
  allnameCategory,
  allusersBBDD,
  addUsersBBDD,
  addUsersPortalesBBDD,
  updateUsersBBDD,
  updateSpamBBDD,
  addProductsBBDD,
  addChangesBBDD,
  deleteProductsBBDD,
  connectSalesForce,
  updatesalesforceBBDD,
  updatesalesforceAreaPrivadaBBDD,
  getcodigoUserSalesForce,
  getidDatosUserSalesForce,
  getcodigoInmuebleSalesForce,
  getdatosInmuebleSalesForce,
  getProductCodeSalesForce,
  getcodigoInmuebleSecundarioSalesForce,
  getdatosUsuariosOportunidadSalesForce,
  getnamesUsuariosOportunidadSalesForce,
  getValuePromotions,
  getoportunidadesganadasSalesForce,
  getoportunidadesbyuserSalesForce,
  getencuestasoportunidadSalesForce,
  getIdgetAlloportunidadesganadasSalesForce,
  getEmailsSalesForce,
  getEmailsUsuariosSalesForce,
  getIdsSalesForce,
  perfilUser,
  getUserSALESFORCE,
  pintarLogs,
  altaportalesBBDD,
  mensajesspamalta,
  updateAltaPortalesBBDD,
  altaLeadsBBDD,
  addMailingBBDD,
  getPromotionName,
  rutafiletalkdeskBBDD,
  updateoportunitysalesforceBBDD
};
