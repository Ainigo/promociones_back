const sequelize = require("../db/db");
const programModel = require("../models/promotions").Program;
const roomsModels = require("../models/promotions").RoomsNumber;

const PROGRAM_FIELDS = ["program_id", "initial", "stock"]

function getProgram(promotion_id, version_id){
    return roomsModels.findAll({
        include: [{
            model: programModel,
            attributes: PROGRAM_FIELDS,
            on: {
                col1: sequelize.where(sequelize.col("pro_rooms_number.room_number"), "=", sequelize.col("pro_program.rooms")),
                col2: sequelize.where(sequelize.col("pro_program.promotion_id"), "=", promotion_id),
                col3: sequelize.where(sequelize.col("pro_program.version_id"), "=", version_id)
            }
        }],
        order:[['room_number', 'ASC']]
    })
}

async function createProgram(program, transaction){
    await program.save({transaction});
}


async function updateProgram(program, condition, transaction){
    await programModel.update(
        {initial: program.initial, stock: program.stock},
        {
          where: condition,
          transaction
        })
};


async function insertOrUpdatePrograms(program, version_id, transaction){
    var condition = { program_id: program.program_id, version_id: version_id };

    var res = await programModel.findOne({ where: condition });
    if (res){
        await updateProgram(program, condition, transaction);
    }
    else {        
        program.version_id = version_id;
        await createProgram(program, transaction);
    }
};

async function getNumberOfPrograms(programsIds, promotion_id, version_id){
    return await programModel.count({
        where: {
            promotion_id,
            version_id,
            program_id: {
                [sequelize.Op.in]: programsIds
            }
        }
    });
};

async function getNumberOfRooms(rooms, promotion_id, version_id){
    return await programModel.count({
        where: {
            promotion_id,
            version_id,
            rooms: {
                [sequelize.Op.in]: rooms
            }
        }
    })
};

module.exports = {
    getProgram,
    insertOrUpdatePrograms,
    getNumberOfPrograms,
    getNumberOfRooms
}