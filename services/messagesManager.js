const messages = require("../config/messages");
const logger = require("../config/logger");
const CustomError = require("../customError");

function createMessage(res, err){
    if (err == messages.INFO_OK){
        res.status(200).send(messages.INFO_OK);
    }else if (err.code == 0){
        res.status(200).send(err);
    }
    else if (err != null && err.type != undefined && err.type === 'custom') {
        logger.error("[MessageServices - CustomError] " + JSON.stringify(err));
        res.status(400).send(err.message);
    } else {
        logger.error("[MessageServices - GenericError] " + JSON.stringify(err));
        res.status(400).send(err); //err.code undefined (Hay que pasarle el mensaje customizado en vez del err de sequelize)
    }
}

function createMessageWithErrorMsg(res, result, errorMsg){
    if (!result ||  (typeof result == 'number' && result > 0)){
        createMessage(res,errorMsg);
    }else{
        createMessage(res, messages.INFO_OK);
    }
}

function buildMessageError(res, error){
    if (error != null && ((error.type != undefined && error.type == 'custom') || (error.code!= null && error.message != null))){
        logger.error("Error: "+JSON.stringify(error));
        return createMessage(res, error);
    }
    else {
        return createMessage(res, messages.ERROR_GENERICO);
    }
}
/**
 * Function that throw a CustomError if:
 *  Result is boolean and is false
 *  Result is number and is > 0
 * @param {*} result 
 * @param {*} errorMsg 
 */
function throwIfNecessaryMessageError(result, errorMsg){
    if (result === false || (typeof result == 'number' && result > 0)){
        throw new CustomError(errorMsg); 
    }
}

function checkParamInformed(param){
    if (param == null || param == undefined){
        throw new CustomError(messages.ERROR_PARAMETROS_INCORRECTOS); 
    }
}


function throwMessageError(errorMsg){
    throw new CustomError(errorMsg);    
}

module.exports = {
    createMessage,
    createMessageWithErrorMsg,
    buildMessageError,
    throwIfNecessaryMessageError,
    checkParamInformed,
    throwMessageError
}