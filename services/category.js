const config = require("../config/config");
const logger = require("../config/logger");
const CustomError = require("../customError");
const jwt = require("jsonwebtoken");
const userService = require("./user");
const message = require("../config/messages");
const azure = require("./authAzure");
const Configbbdd = require("../global/config");

const databaseConfig = {
  connectionLimit : Configbbdd.bbdd.connectionLimit,
  host: Configbbdd.bbdd.host,
  user: Configbbdd.bbdd.user,
  password: Configbbdd.bbdd.password,
  database: Configbbdd.bbdd.database
}

const mysql = require('mysql');
const { promisify } = require('util')

async function nameCategory(params){
  var nameCategory;
  logger.debug("Obteniendo el nombre de la categoría");
  logger.debug(params.idCategorias);
  const pool = mysql.createPool(databaseConfig);
  const promiseQuery = promisify(pool.query).bind(pool);
  const promisePoolEnd = promisify(pool.end).bind(pool);
    
  const query = "Select NOMBRE_CATEGORIA from CATEGORIAS_LIVING where ID_CATEGORIA = '"+params.idCategorias+"'";
  const result = await promiseQuery(query) // use in async function
  logger.debug('El Nombre de la Categoría es:' +result[0].NOMBRE_CATEGORIA );
  nameCategory = result[0].NOMBRE_CATEGORIA;
  logger.debug('El Nombre de la Categoría2 es:' +nameCategory );
  pool.end();
  promisePoolEnd();


   return nameCategory;

};


module.exports = {
  nameCategory
};
