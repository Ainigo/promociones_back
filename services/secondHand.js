const sequelize = require("../db/db");
const secondHandModel = require("../models/studies").SecondHand;
const transactionModel = require("../models/master").Values;
const secondHandStatusModel = require("../models/master").Values;
const commercialStatusModel = require("../models/master").Values;

const addressSeq = [
    sequelize.fn(
      "PGP_SYM_DECRYPT",
      sequelize.cast(sequelize.col("address"), "bytea"),
      "AES_KEY"
    ),
    "address"
];
const coordinatesSeq = [
    sequelize.fn(
      "PGP_SYM_DECRYPT",
      sequelize.cast(sequelize.col("coordinates"), "bytea"),
      "AES_KEY"
    ),
    "coordinates"
];
const urlSeq = [
    sequelize.fn(
      "PGP_SYM_DECRYPT",
      sequelize.cast(sequelize.col("url"), "bytea"),
      "AES_KEY"
    ),
    "url"
];

const SECOND_HAND_FIELDS = ["study_id", "shr_id", addressSeq, coordinatesSeq, "year", "floor", "rooms", "bathrooms", "area", "price", urlSeq, "comments", "registry"];
const MASTER_TABLE_FIELDS = ["value_id", "value", "label"];

const getSecondHandRent = shr_id =>
secondHandModel.findOne({
    where: { shr_id },
    attributes: SECOND_HAND_FIELDS,
    include: [
        {model: transactionModel, as: "transaction", attributes: MASTER_TABLE_FIELDS},
        {model: secondHandStatusModel, as: "second_hand_status", attributes: MASTER_TABLE_FIELDS},
        {model: commercialStatusModel, as: "commercial_status", attributes: MASTER_TABLE_FIELDS}
    ]
});

const createSecondHandRent = secondHand =>
secondHand.save();

const updateSecondHandRent = (secondHand, shr_id) =>
secondHandModel.update(secondHand,
    { where: {shr_id} }
);

const deleteSecondHandRent = shr_id =>
secondHandModel.destroy({where: {shr_id}});

module.exports = {
    getSecondHandRent,
    createSecondHandRent,
    updateSecondHandRent,
    deleteSecondHandRent
}