const financeModel = require("../models/promotions").Finances;
const valuesModel = require("./../models/master").Values;
const MASTER_TABLE_FIELDS = require("./../services/master").BASIC_DATA_VALUES_FIELDS;

async function getFinance(promotion_id){
    return await financeModel.findOne({
        attributes: {exclude: ["created_at", "updated_at", "deferment_payments_id"]},
        where: {promotion_id},
        include: { model: valuesModel, as: "deferment_payments", attributes: MASTER_TABLE_FIELDS }
    });
}

const createFinance = finance =>
    finance.save();

const updateFinance = (finance, promotion_id) =>
    financeModel.update(finance,
       { where: {promotion_id} }
);

module.exports = {
    getFinance,
    createFinance,
    updateFinance
}