const config = require("../config/config");
const msRestAzure = require('ms-rest-azure');
const request = require('request-promise');
const logger = require("../config/logger");


async function executeRequest(requestedMethod, targetUrl, token, body){
    logger.debug("targetUrl: "+targetUrl);
     // Set the headers
     var headers = null;
     if(token){         
        headers = {
            'User-Agent':   'Super Agent/0.0.1',
            json:true,        
            'Authorization':'Bearer ' + token,
        }    
    }else{
        headers = {
            'User-Agent':   'Super Agent/0.0.1',
            json:true            
        }    
    }
    // Configure the request
    var options = {
        url: targetUrl,
        method: requestedMethod,
        headers: headers
    };
    if (requestedMethod == 'POST'){
        options.headers['Content-Type'] = 'application/json';
    }else if (requestedMethod == 'PUT'){
        options.headers['Content-Type'] = 'application/octet-stream';
    }else{
        options.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    }

    if (body != null && body != undefined){
        options.body = body;          
    }
    return await request(options);
    
}

// async function getToken(scope){
    async function getToken(){
    var headers = {
        'User-Agent':       'Super Agent/0.0.1',
        'Content-Type':     'application/x-www-form-urlencoded'
    }

    // Configure the request
    var options = {
        url: config.azure.LoginUrl.replace("{{directoryId}}", config.auth.tenant),
        method: 'POST',
        headers: headers,
        form: {
            grant_type: 'client_credentials',
            client_id: config.auth.clientId,
            //scope: scope,
            scope: 'https://graph.microsoft.com/.default',
            client_secret: config.auth.secret
        }
    }
    var res = await request(options);
    return JSON.parse(res).access_token;
}

// async function getPromocionaToken(){
//     return getToken(); //scope de promociona
// }


// async function getAzureToken(){
//     //'https://graph.microsoft.com/.default'
// return getToken('https://graph.microsoft.com/.default');
    
// }

async function exitsUserOnAzure(usuario){
    var exist = false;
    // Set the headers
    var headers = {
        'User-Agent':       'Super Agent/0.0.1',
        'Content-Type':     'application/x-www-form-urlencoded'
    }

    // Configure the request
    var options = {
        url: config.azure.LoginUrl.replace("{{directoryId}}", config.auth.tenant),
        method: 'POST',
        headers: headers,
        form: {
            grant_type: 'client_credentials',
            client_id: config.auth.clientId,
            scope: 'https://graph.microsoft.com/.default',
            client_secret: config.auth.secret
        }
    }
    // Start the request
    var result = await request(options);    
    var token = JSON.parse(result).access_token;
    var userUrl = config.azure.getUser + usuario;
    result = await executeRequest('GET', userUrl, token, null);
    var azureUser = JSON.parse(result).userPrincipalName;
    
    if (usuario.toString().toUpperCase() == azureUser.toString().toUpperCase()){
        exist = true;
    }
    return exist;
}


async function authenticateAzure(usuario, password){
    logger.debug("el usuario es: " +usuario);
    logger.debug("la pass es: " +password);
    var valido = false;
    return await msRestAzure.loginWithUsernamePasswordWithAuthResponse(usuario, password, { 
        tokenAudience: 'graph', 
        domain: config.auth.tenant 
    }).then(res =>{
        valido = true;
        return valido;       
    });
     
}




module.exports = {
    //getAzureToken,
    authenticateAzure,
    exitsUserOnAzure,
    executeRequest,
    //getPromocionaToken
  };
  