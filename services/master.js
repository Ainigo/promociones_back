const validator = require("./../utils/validators");
const config = require("../config/config");
const valuesModel = require("../models/master").Values;
const typesModel = require("../models/master").Types;
const firstLevelsModel = require("../models/master").FirstLevels;
const sequelize = require("../db/db");

const BASIC_DATA_VALUES_FIELDS = ["value_id", "value", "label"];
const BASIC_DATA_TYPES_FIELDS = ["type_id", "type_value", "type_label"];

const logger = require("../config/logger");

const storeProcedures = {
    PROCEDURE_CREATE_NEW_VERSION: 'new_study_version',
    PROCEDURE_DELETE_USER: 'delete_user',
    PROCEDURE_DELETE_STUDY: 'delete_study',
    PROCEDURE_DELETE_PROMOTION: 'delete_promotion',
    PROCEDURE_DELETE_BUILDING: 'delete_building'  
  }
  

const getBasicDataByTypeValue = type_value =>
    valuesModel.findAll({
        attributes: BASIC_DATA_VALUES_FIELDS,
        where: { "enabled": true },
        include: { model: typesModel, attributes: BASIC_DATA_TYPES_FIELDS, where: { type_value } }
    })

const getConfigDataTypes = () =>
    typesModel.findAll(
        //{attributes: { exclude: "first_level_id" }}
    );

const getConfigDataValuesByTypeId = type_id =>
    valuesModel.findAll({
        where: { type_id },
        attributes: { exclude: "type_id" }
    });

const createConfigDataValues = dataValue =>
    dataValue.save();


const updateConfigDataValues = dataValue => {
    var values = {};
    validator.addIfNotNull(values, dataValue, 'label');
    validator.addIfNotNull(values, dataValue, 'enabled');
    return valuesModel.update(
        values,
        { where: { value_id: dataValue.value_id } },
    );
}


async function createConfigDataType(dataType) {
    await dataType.save();
}

async function updateConfigDataType(dataType) {
    var values = {};
    validator.addIfNotNull(values, dataType, 'enabled');
    validator.addIfNotNull(values, dataType, 'type_label');

    await typesModel.update(
        values,
        { where: { type_id: dataType.type_id, first_level_id: dataType.first_level_id } }
    )
};

async function getStudyStatus(status) {
    return valuesModel.findOne({
        attributes: ["value_id"],
        where: {
            enabled: "true",
            value: status
        },
        include: [
            { model: typesModel, attributes: [], where: { type_value: config.defaultStudyStatusType } }
        ]
    })
}

async function getDefaultStudyStatus(value_id) {
    return valuesModel.findOne({
        attributes: ["value_id"],
        where: {
            enabled: "true",
            value: config.defaultStudyStatusValue
        },
        include: [
            { model: typesModel, attributes: [], where: { type_value: config.defaultStudyStatusType } }
        ]
    })
}

async function getNameStudyStatus(value) {
    return await valuesModel.findOne({
        attributes: ["label"],
        where: { value }        
    })
}

async function getCntValue(value_id) {
    return await valuesModel.findOne({
        attributes: ["value"],
        where: { value_id }
    })
}


async function getCntValueId(value, type_id) {
    return await valuesModel.findOne({        
        where: { value, type_id }
    })
}

async function getDataType(type_id) {
    return await typesModel.findOne({
        attributes: ["data_type"],
        where: { type_id }
    })
}

async function getTypeByValue(value_id) {
    return await valuesModel.findOne({
        attributes: ["type_id"],
        where: { value_id }
    })
}

function getFirstLevels() {
    return firstLevelsModel.findAll({
        where: { enabled: true },
        include: [
            { model: typesModel, required: true }
        ],
        order: [["first_level_id", "ASC"]]
    })
}


async function callStoreProcedure(procedure, param_id) {
    try {
        await sequelize.query('CALL '+procedure+'(:param);', { replacements: { param: param_id } });    
    } catch (error) {
        logger.error("Error en la ejecución de : Call "+procedure+ " con el parámetro: "+ param_id);
        logger.error(error);
        throw error;            
    }    
}

module.exports = {
    BASIC_DATA_VALUES_FIELDS,
    getBasicDataByTypeValue,
    getConfigDataTypes,
    getConfigDataValuesByTypeId,
    createConfigDataValues,
    updateConfigDataValues,
    createConfigDataType,
    updateConfigDataType,
    getStudyStatus,
    getDefaultStudyStatus,
    getNameStudyStatus,
    getCntValueId,
    getCntValue,
    getDataType,
    getTypeByValue,
    getFirstLevels,
    callStoreProcedure,
    storeProcedures
}