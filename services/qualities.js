const qualitiesModel = require("../models/promotions").Qualities;
const firstLevelsModel = require("../models/master").FirstLevels;
const typesModel = require("../models/master").Types;
const valuesModel = require("../models/master").Values;

const FIRST_LEVELS_FIELDS = ["label", "value"];

async function getQualities(promotion_id) {
    //const getQualities = promotion_id =>
    return await firstLevelsModel.findAll({
        attributes: FIRST_LEVELS_FIELDS,
        order: [["first_level_id", "ASC"],[typesModel, "type_id","ASC"]],
        include: {
            model: typesModel,
            attributes: { exclude: ["first_level_id", "enabled"] },
            required: true,
            where: { 'enabled': true },
            include: {
                model: qualitiesModel,
                attributes: ["value"],
                where: { promotion_id },
                required: false,
                include: { model: valuesModel, attributes: { exclude: ["type_id", "enabled"] } }
            }
        }
    });
}

async function createQualities(qualities, transaction){
    await qualities.save({transaction});
};

async function updateQualities(quality, transaction){
    var promotion_id = quality.promotion_id;
    await qualitiesModel.update(
        {cnt_quality_value_id: quality.cnt_quality_value_id, value:quality.value},
        {where: {promotion_id, cnt_quality_type_id: quality.cnt_quality_type_id}},
        {transaction}
    )
};



async function insertOrUpdateQualities(quality, transaction){
    var promotion_id = quality.promotion_id;
    var condition = {promotion_id, cnt_quality_type_id: quality.cnt_quality_type_id};
    var res = await qualitiesModel.findOne({ where: condition });
    if (res){
        await updateQualities(quality, transaction);
    }
    else {
        await createQualities(quality, transaction);
    }    
};


module.exports = {
    getQualities,
    createQualities,
    updateQualities,
    insertOrUpdateQualities
}