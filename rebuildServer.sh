#!/bin/bash
imageName=blissful_volhard
containerName=4ed9a5701d09

docker build -t $imageName -f Dockerfile  .

echo Delete old container...
docker rm -f $containerName

echo Run new container...
docker run -d -p 3000:3000 --name $containerName $imageName
