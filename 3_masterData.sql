SELECT setval('cnt_types_type_id_seq', 1);
SELECT setval('cnt_values_value_id_seq', 1);
SELECT setval('qua_first_levels_first_level_id_seq', 1);

-- Labels de room numbers
delete from url_rights;
delete from url_urls;
delete from pro_rooms_number;
delete from usr_rols;
delete from usr_users;
delete from cnt_values;
delete from cnt_types;
delete from qua_first_levels;

insert into pro_rooms_number values (0, '0');
insert into pro_rooms_number values (1, '1');
insert into pro_rooms_number values (2, '2');
insert into pro_rooms_number values (3, '3');
insert into pro_rooms_number values (4, '4');
insert into pro_rooms_number values (5, '5');
insert into pro_rooms_number values (6, '6+');

INSERT INTO "public".usr_rols(role_name, role_id) VALUES ('usuario básico',1);	
INSERT INTO "public".usr_rols(role_name, role_id) VALUES ('usuario avanzado',2);	
INSERT INTO "public".usr_rols(role_name, role_id) VALUES ('administrador',3);	
INSERT INTO "public".usr_rols(role_name, role_id) VALUES ('D. Territorial',4);	

-- AEDAS
INSERT INTO "public"."usr_users" ("user_id","email","valid","name","role_id","created_at","updated_at") VALUES (DEFAULT,PGP_SYM_ENCRYPT('mcerda@abast.es', 'AES_KEY'),true,PGP_SYM_ENCRYPT('Marcos', 'AES_KEY'),'3',now(),null);
INSERT INTO "public"."usr_tokens" ("token_id","token","user_id") VALUES (DEFAULT,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwidGltZSI6IjIwMTktMDQtMDFUMTQ6MDg6NDQuOTUyWiIsImlhdCI6MTU1NDEyNzcyNCwiZXhwIjoxNTc1NzI3NzI0fQ.E3pYSi5umdmScoFI9bJBS5c8F73Kdleg6WUN9STPHMg',
    (select user_id from "public"."usr_users" where PGP_SYM_DECRYPT(CAST("email" AS BYTEA), 'AES_KEY') = 'mcerda@abast.es'));
INSERT INTO "public"."usr_users" ("user_id","email","valid","name","role_id","created_at","updated_at") VALUES (DEFAULT,PGP_SYM_ENCRYPT('vgimeno@abast.es', 'AES_KEY'),true,PGP_SYM_ENCRYPT('Victor', 'AES_KEY'),'1',now(),null);
INSERT INTO "public"."usr_tokens" ("token_id","token","user_id") VALUES (DEFAULT,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OSwidGltZSI6IjIwMTktMDQtMDJUMDc6MTk6NDYuNDI0WiIsImlhdCI6MTU1NDE4OTU4NiwiZXhwIjoxNzcwMTg5NTg2fQ.oBMPR1y0wpilkMLFmRuOV_Pg7gBI4K9EL5MaO9r7mjw',
    (select user_id from "public"."usr_users" where PGP_SYM_DECRYPT(CAST("email" AS BYTEA), 'AES_KEY') = 'vgimeno@abast.es'));

-- Insert CNT_TYPES
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (1,'comunidad_autonoma','Comunidad autónoma',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (2,'Tipo de promoción','Tipo de promoción',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (3,'Tipo de protección','Tipo de protección',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (4,'Entorno','Entorno',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (5,'Estado obra','Estado obra',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (6,'Planta','Planta',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (7,'Tipo de unifamiliar','Tipo de unifamiliar',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (8,'Estado inmueble','Estado inmueble',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (9,'Estado segunda mano','Estado segunda mano',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (10,'Operación','Operación',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (11,'Estado comercial','Estado comercial',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (12,'Estado estudio','Estado estudio',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (13,'tipo_vivienda','Tipo de vivienda',null, true,1);
INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (52,'Territorial','Territorial',null, true,1);

INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (1,1,'Andalucía','Andalucía', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (2,1,'Aragón','Aragón', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (3,1,'Asturias','Asturias', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (4,1,'Balears','Balears', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (5,1,'Canarias','Canarias', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (6,1,'Cantabria','Cantabria', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (7,1,'Castilla y León','Castilla y León', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (8,1,'Castilla-La Mancha','Castilla-La Mancha', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (9,1,'Cataluña','Cataluña', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (10,1,'Comunitat Valenciana','Comunitat Valenciana', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (11,1,'Extremadura','Extremadura', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (12,1,'Galicia','Galicia', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (13,1,'Madrid','Madrid', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (14,1,'Murcia','Murcia', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (15,1,'Navarra','Navarra', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (16,1,'País Vasco','País Vasco', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (17,1,'La Rioja','La Rioja', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (18,2,'Unifamiliar','Unifamiliar', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (19,2,'Plurifamiliar','Plurifamiliar', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (20,3,'Libre','Libre', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (21,3,'VPO','VPO', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (22,4,'Area próxima','Area próxima', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (23,4,'Entorno inmediato','Entorno inmediato', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (24,5,'Solar','Solar', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (25,5,'Solar vaciado','Solar vaciado', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (26,5,'Cimentación','Cimentación', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (27,5,'Estructura','Estructura', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (28,5,'Cerramiento exterior','Cerramiento exterior', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (29,5,'Cerramiento interior','Cerramiento interior', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (30,5,'Llave en mano','Llave en mano', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (31,6,'Bajo sin jardin','Bajo sin jardin', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (32,6,'Bajo con jardin','Bajo con jardin', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (33,6,'Planta intermedia','Planta intermedia', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (34,6,'Atico con terraza','Atico con terraza', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (35,6,'Última planta','Última planta', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (36,7,'Adosado','Adosado', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (37,7,'Pareado','Pareado', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (38,7,'Individual','Individual', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (39,8,'Disponible','Disponible', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (40,8,'Vendido','Vendido', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (41,9,'Muy buen estado','Muy buen estado', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (42,9,'Buen estado','Buen estado', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (43,9,'Reformas parciales','Reformas parciales', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (44,9,'Reforma total','Reforma total', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (45,10,'Venta','Venta', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (46,10,'Alquiler','Alquiler', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (47,11,'Disponible','Disponible', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (48,11,'No disponible','No disponible', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (49,12,'abierto','En progreso', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (50,12,'revision','En revisión', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (51,12,'cerrado','Finalizado', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (52,13,'unifamiliar','Unifamiliar', true);	
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (53,13,'plurifamiliar','Plurifamiliar', true);	


-- Calidades
INSERT INTO "public".qua_first_levels(first_level_id, "order", value, label, enabled) VALUES (1,1,'edificio','Edificio',true);
INSERT INTO "public".qua_first_levels(first_level_id, "order", value, label, enabled) VALUES (2,2,'suelos','Suelos',true);
INSERT INTO "public".qua_first_levels(first_level_id, "order", value, label, enabled) VALUES (3,3,'carpintería_exterior','Carpintería Exterior',true);
INSERT INTO "public".qua_first_levels(first_level_id, "order", value, label, enabled) VALUES (4,4,'carpintería_interior','Carpintería Interior',true);
INSERT INTO "public".qua_first_levels(first_level_id, "order", value, label, enabled) VALUES (5,5,'climatización_acs','Climatización ACS',true);
INSERT INTO "public".qua_first_levels(first_level_id, "order", value, label, enabled) VALUES (6,6,'especiales','Especiales',true);
INSERT INTO "public".qua_first_levels(first_level_id, "order", value, label, enabled) VALUES (7,7,'zonas_comunes','Zonas Comunes',true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (14, 1, 'fachada','Fachada',1,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (15, 1, 'salon','Salon',2,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (16, 1, 'dormitorio','Dormitorio',2,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (17, 1, 'suelos_cocina','Cocina',2,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (18, 1, 'suelos_baño','Baños',2,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (19, 1, 'ventanas','Ventanas',3,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (20, 1, 'persianas','Persianas',3,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (21, 1, 'tipo_persiana','Tipo Persiana',3,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (22, 2, 'rotura_puente_termico','Rotura de puente térmico',3,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (23, 2, 'acristalamiento_doble_con_camara_de_aire','Acristalamiento doble con camara de aire',3,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (24, 2, 'climalit_calidad_superior','Climalit calidad superior',3,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (25, 1, 'puerta_de_entrada','Puerta de entrada',4,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (26, 1, 'puertas_de_paso','Puertas de paso',4,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (27, 2, 'armarios_empotrados','Armarios empotrados',4,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (28, 2, 'vestidos_completos','Vestidos completos (cajonera/baldas)',4,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (29, 2, 'vestidor','Vestidor',4,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (30, 1, 'a_a','A/A',5,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (31, 1, 'metodo_climal','Método Climat.',5,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (32, 1, 'sistema','Sistema',5,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (33, 2, 'radiador_toallero','Radiador Toallero',5,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (34, 1, 'cocina','Cocina',6,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (35, 1, 'encimera','Encimera',6,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (36, 1, 'baños','Baños',6,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (37, 1, 'domotica','Domótica',6,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (38, 1, 'entrada','Entrada',6,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (39, 1, 'alarma','Alarma',6,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (40, 2, 'piscina_comunitaria','Piscina Comunitaria',7,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (41, 2, 'piscina_cubierta_climatizada','Piscina Cubierta Climatizada',7,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (42, 2, 'zonas_verdes_ajardinadas','Zonas verdes ajardinadas',7,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (43, 2, 'local_comunitario','Local comunitario',7,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (44, 2, 'sala_de_proyección','Sala de proyección',7,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (45, 2, 'paddel','Paddel',7,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (46, 2, 'pista_polideportiva','Pista polideportiva',7,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (47, 2, 'zona_infantil','Zona Infantil',7,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (48, 2, 'gimnasio','Gimnasio',7,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (49, 2, 'sauna','Sauna',7,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (50, 2, 'aparcamiento_para_bicicletas','Aparcamiento para bicicletas',7,true);
INSERT INTO "public".cnt_types(type_id, data_type, type_value, type_label, first_level_id, enabled) VALUES (51, 2, 'carga_elec_vehiculos','Carga eléctrica de vehículos',7,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (54,'monocapa','Monocapa',14,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (55,'hormigon_prefabricado','Hormigón prefabricado',14,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (56,'ventilada_cerámica','Ventilada Cerámica',14,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (57,'piedra','Piedra',14,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (58,'ladrillo_visto','Ladrillo Visto',14,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (59,'marmol','Mármol',14,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (60,'mixta','Mixta',14,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (61,'sate','Sate',14,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (62,'parquet_laminado','Parquet laminado',15,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (63,'marmol','Mármol',15,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (64,'tarima_flotante','Tarima flotante',15,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (65,'ceramico','Cerámico',15,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (66,'gres_porcelanico','Gres porcelánico',15,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (67,'laminado_sintetico','Laminado sintético',15,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (68,'piedra_natural','Piedra Natural',15,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (69,'parquet_laminado','Parquet laminado',16,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (70,'marmol','Mármol',16,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (71,'tarima_flotante','Tarima flotante',16,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (72,'ceramico','Cerámico',16,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (73,'gres_porcelanico','Gres porcelánico',16,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (74,'laminado_sintetico','Laminado sintético',16,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (75,'piedra_natural','Piedra Natural',16,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (76,'gres_porcelanico','Gres porcelánico',17,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (77,'ceramico','Cerámico',17,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (78,'marmol','Mármol',17,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (79,'laminado_sintetico','Laminado sintético',17,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (80,'gres_porcelanico','Gres porcelánico',18,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (81,'ceramico','Cerámico',18,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (82,'marmol','Mármol',18,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (83,'laminado_sintetico','Laminado sintético',18,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (84,'p.v.c.','P.V.C.',19,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (85,'aluminio_anodizado','Aluminio Anodizado',19,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (86,'aluminio_lacado','Aluminio Lacado',19,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (87,'madera_lacada','Madera Lacada',19,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (88,'aluminio_termico','Aluminio térmico',20,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (89,'pvc','PVC',20,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (90,'plastico','Plástico',20,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (91,'persianas_enrollables','Persianas enrollables',21,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (92,'persianas_motorizadas','Persianas motorizadas',21,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (93,'blindada','Blindada',25,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (94,'acorazada','Acorazada',25,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (95,'madera_maciza','Madera Maciza',26,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (96,'madera_barnizada','Madera Barnizada',26,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (97,'madera_chapada','Madera Chapada',26,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (98,'madera_lacada','Madera Lacada',26,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (99,'instalado','Instalado',30,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (100,'preinstalado','Preinstalado',30,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (101,'bomba_frio_calor','Bomba Frío-Calor',31,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (102,'suelo_radiante','Suelo radiante',31,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (103,'suelo_refrigerante','Suelo refrigerante',31,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (104,'suelo_radiante_y_refrigerante','Suelo radiante y refrigerante',31,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (105,'caldera_y_radiadores','Caldera y Radiadores',31,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (106,'bomba_frio','Bomba Frío',31,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (107,'aerotermia','Aerotermia',32,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (108,'geotermia','Geotermia',32,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (109,'gas_natural','Gas Natural',32,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (110,'electrica','Eléctrica',32,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (111,'amueblada','Amueblada',34,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (112,'amueblada_con_elect','Amueblada con electrodomésticos',34,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (113,'compacta','Compacta',35,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (114,'silestone','Silestone',35,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (115,'mueble_completo','Mueble completo',36,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (116,'espejo_encastrado','Espejo encastrado',36,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (117,'mampara','Mámpara',36,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (118,'instalado','Instalado',37,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (119,'preinstalado','Preinstalado',37,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (120,'portero_automatico','Portero Automático',38,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (121,'video_portero','Video Portero',38,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (122,'instalado','Instalado',39,true);
INSERT INTO "public".cnt_values(value_id, value, label, type_id, enabled) VALUES (123,'preinstalado','Preinstalado',39,true);
	 
-- Seteamos las secuencias al valor que corresponda para añadir los últimos cambios
SELECT setval('cnt_values_value_id_seq', (select max(value_id) from cnt_values));
SELECT setval('cnt_types_type_id_seq', (select max(type_id) from cnt_types));
										   
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (DEFAULT,52,'centro','Centro', true);
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (DEFAULT,52,'cataluna_aragon','Cataluña y Aragón', true);
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (DEFAULT,52,'levante_baleares','Levante y Baleares', true);
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (DEFAULT,52,'andalucia','Andalucía', true);
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (DEFAULT,52,'costa_del_sol','Costa del Sol', true);

INSERT INTO "public".cnt_types(type_id, type_value, type_label, first_level_id, enabled, data_type) VALUES (DEFAULT,'aplazamiento_pagos','Aplazamiento de pagos',null, true,1);

INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (DEFAULT,(SELECT type_id from cnt_types where type_value = 'aplazamiento_pagos'),'mensual','Mensual', true);
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (DEFAULT,(SELECT type_id from cnt_types where type_value = 'aplazamiento_pagos'),'trimestral','Trimestral', true);
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (DEFAULT,(SELECT type_id from cnt_types where type_value = 'aplazamiento_pagos'),'pago_unico','Pago único', true);

-- Nuevo valor para promociones. Lo ponemos aqui cuando se han actualizado las secuencias
INSERT INTO "public".cnt_values(value_id, type_id, value, label, enabled) VALUES (DEFAULT, 5,'promocion_vendida','Promoción vendida', true);	

update cnt_types set type_value = TRIM(type_value), type_label = TRIM(type_label);



INSERT INTO "public".url_urls(url, url_id) VALUES ('/studies',1);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/promotions',2);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/basic_data',3);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/promotion/versions',4);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/config',5);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/login',6);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/users',7);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/export',8);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/rols',9);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/second_hand_rent',10);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/next_promotions',11);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/documents',12);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/change_password',13);	
INSERT INTO "public".url_urls(url, url_id) VALUES ('/studies/changeStudyStatus',14);	


insert into  "public".url_rights(url_id, role_id, methods) values(1,1,'get, put');
insert into  "public".url_rights(url_id, role_id, methods) values(1,2,'get, put, post');
insert into  "public".url_rights(url_id, role_id, methods) values(1,3,'get, put, post, delete');
insert into  "public".url_rights(url_id, role_id, methods) values(2,1,'get, put, post');
insert into  "public".url_rights(url_id, role_id, methods) values(2,2,'get, put, post');
insert into  "public".url_rights(url_id, role_id, methods) values(2,3,'get, put, post, delete');
insert into  "public".url_rights(url_id, role_id, methods) values(3,3,'get');
insert into  "public".url_rights(url_id, role_id, methods) values(3,1,'get');
insert into  "public".url_rights(url_id, role_id, methods) values(3,2,'get');
insert into  "public".url_rights(url_id, role_id, methods) values(4,3,'get, post');
insert into  "public".url_rights(url_id, role_id, methods) values(5,3,'get, put, post, delete');
insert into  "public".url_rights(url_id, role_id, methods) values(5,1,'get');
insert into  "public".url_rights(url_id, role_id, methods) values(5,2,'get');
insert into  "public".url_rights(url_id, role_id, methods) values(7,3,'get, put, post, delete');
insert into  "public".url_rights(url_id, role_id, methods) values(8,1,'get');
insert into  "public".url_rights(url_id, role_id, methods) values(8,2,'get');
insert into  "public".url_rights(url_id, role_id, methods) values(8,3,'get');
insert into  "public".url_rights(url_id, role_id, methods) values(9,1,'get');
insert into  "public".url_rights(url_id, role_id, methods) values(9,2,'get');
insert into  "public".url_rights(url_id, role_id, methods) values(9,3,'get');
insert into  "public".url_rights(url_id, role_id, methods) values(10,1,'get, post, put');
insert into  "public".url_rights(url_id, role_id, methods) values(10,2,'get, post, put');
insert into  "public".url_rights(url_id, role_id, methods) values(10,3,'get, put, post, delete');
insert into  "public".url_rights(url_id, role_id, methods) values(11,1,'get, post, put');
insert into  "public".url_rights(url_id, role_id, methods) values(11,2,'get, post, put');
insert into  "public".url_rights(url_id, role_id, methods) values(11,3,'get, post, put, delete');
insert into  "public".url_rights(url_id, role_id, methods) values(12,1,'get, post, put');
insert into  "public".url_rights(url_id, role_id, methods) values(12,2,'get, post, put');
insert into  "public".url_rights(url_id, role_id, methods) values(12,3,'get, put, post, delete');
insert into  "public".url_rights(url_id, role_id, methods) values(14,1,'put');
insert into  "public".url_rights(url_id, role_id, methods) values(14,2,'put');
insert into  "public".url_rights(url_id, role_id, methods) values(14,3,'put');

-- Permisos para el rol D.Territorial.
insert into url_rights 
    select url_id, 4, methods from url_rights where role_id = 2;

SELECT setval('cnt_values_value_id_seq', (select max(value_id) from cnt_values));
SELECT setval('cnt_types_type_id_seq', (select max(type_id) from cnt_types));
SELECT setval('qua_first_levels_first_level_id_seq', (select max(first_level_id) from qua_first_levels));
SELECT setval('stu_market_studies_study_id_seq', (select max(study_id) from stu_market_studies));
SELECT setval('stu_next_promotions_next_promotion_id_seq', (select max(next_promotion_id) from stu_next_promotions));
SELECT setval('stu_promotions_promotion_id_seq', (select max(promotion_id) from stu_promotions));
SELECT setval('stu_second_hand_rent_shr_id_seq', (select max(shr_id) from stu_second_hand_rent));
SELECT setval('stu_versions_version_id_seq', (select max(version_id) from stu_versions));
SELECT setval('url_urls_url_id_seq', (select max(url_id) from url_urls));
SELECT setval('usr_rols_role_id_seq', (select max(role_id) from usr_rols));
SELECT setval('usr_tokens_token_id_seq', (select max(token_id) from usr_tokens));
SELECT setval('usr_users_user_id_seq', (select max(user_id) from usr_users));