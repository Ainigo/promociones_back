module.exports = {
    promotionFull: " SELECT " +
    "     pro.promotion_id, " +
    "     pgp_sym_decrypt(CAST(pro.name AS bytea), 'AES_KEY') AS name, " +
    "     pgp_sym_decrypt(CAST(pro.promoter AS bytea), 'AES_KEY') AS promoter, " +
    "     pgp_sym_decrypt(CAST(pro.marketer AS bytea), 'AES_KEY') AS marketer, " +
    "     pgp_sym_decrypt(CAST(pro.address AS bytea), 'AES_KEY') AS zona, " +
    "     v_promo_type.label     AS promotion_type, " +
    "     CASE " +
    "         WHEN pro.coordinates IS NULL THEN  " +
    "             pgp_sym_decrypt(CAST(pro.coordinates AS bytea), 'AES_KEY') " +
    "         ELSE " +
    "             pgp_sym_decrypt(CAST(pro.address AS bytea), 'AES_KEY') " +
    "     END AS ubicacion, " +
    "     v_promo_status.label   AS work_status, " +
    "     pro.sales_start_date, " +
    "     promo_vers.delivery_date, " +
    "     fin.reserve, " +
    "     fin.mortgage / 100 AS mortgage, " +
    "     bld.rooms, " +
    "     bld.bathrooms, " +
    "     bld.terrace_area, " +
    "     bld.pvp_commercial, " +
    "     v_bld_status.label as status, " +
    "     v_bld_type.label as building_type, " +
    "     bld.observations, " +
    "     single.plot_area,  " +
    "     single.sbr,  " +
    "     single.spb,  " +
    "     single.spp,  " +
    "     single.sbc,  " +
    "     single.sbr_constructed,  " +
    "     single.spb_constructed,  " +
    "     single.spp_constructed,  " +
    "     single.sbc_constructed,  " +
    "     v_single_type.label as single_type, " +
    "     v_floor.label as floor,  " +
    "     multi.util_area,  " +
    "     multi.constructed_area,  " +
    "     multi.garage,  " +
    "     multi.garages_number,  " +
    "     multi.pvp_garage,  " +
    "     multi.storage,  " +
    "     multi.pvp_storage " +
    " FROM " +
    "     stu_promotions           pro " +
    "     LEFT JOIN cnt_values               v_promo_type ON ( pro.promotion_type_id = v_promo_type.value_id ) " +
    "     LEFT JOIN stu_versions             stu_ver ON ( stu_ver.study_id = pro.study_id AND stu_ver.version_number = 0 ) " +
    "     LEFT JOIN pro_promotion_versions   promo_vers ON ( pro.promotion_id = promo_vers.promotion_id AND promo_vers.version_id = stu_ver.version_id ) " +
    "     LEFT JOIN cnt_values               v_promo_status ON ( v_promo_status.value_id = promo_vers.work_status_id ) " +
    "     LEFT OUTER JOIN pro_finances             fin ON ( fin.promotion_id = pro.promotion_id ) " +
    "     LEFT JOIN pro_buildings bld ON (bld.promotion_id = pro.promotion_id and bld.version_id = stu_ver.version_id) " +
    "     LEFT JOIN cnt_values  v_bld_status ON ( v_bld_status.value_id = bld.status_id ) " +
    "     LEFT JOIN cnt_values  v_bld_type ON ( v_bld_type.value_id = bld.building_type_id )     " +
    "     LEFT OUTER JOIN pro_building_multi multi ON ( bld.building_id = multi.building_id and bld.version_id = multi.version_id) " +
    "     LEFT OUTER JOIN pro_building_single single ON ( bld.building_id = single.building_id and bld.version_id = single.version_id) " +
    "     LEFT OUTER JOIN cnt_values  v_single_type ON ( v_single_type.value_id = single.single_type_id )     " +
    "     LEFT OUTER JOIN cnt_values  v_floor ON ( v_floor.value_id = multi.floor_id )     " +
    " WHERE " +
    "     pro.study_id = :estudio " +
    " order by promotion_id, bld.rooms asc",
    second_hand_rent: "select " +
        "    shr.shr_id," +
        "    v_transaction.label as transaction," +
        "    CASE WHEN shr.coordinates is null THEN " +
        "        PGP_SYM_DECRYPT(CAST(shr.address AS BYTEA), 'AES_KEY')         " +
        "     ELSE" +
        "        PGP_SYM_DECRYPT(CAST(shr.coordinates AS BYTEA), 'AES_KEY')      " +
        "    END as ubicacion,    " +
        "    shr.floor," +
        "    shr.rooms," +
        "    shr.bathrooms," +
        "    shr.year," +
        "    v_status.label as status," +
        "    shr.area," +
        "    shr.price "+
        "from stu_second_hand_rent shr left join cnt_values v_transaction on (shr.transaction_id = v_transaction.value_id)" +
        "    left join cnt_values v_status on (shr.second_hand_status_id = v_status.value_id) " +
        "where shr.study_id = :estudio",
    qualities: "select " +
        "    concat('Promoción ',qua.promotion_id) as promocion, " +
        "    qua.promotion_id as promocion_id, " +
        "    lev.label as first_level, " +
        "    typ.type_label as tipo," +
        "    case when val.value_id is null then " +
        "        case when qua.value then " +
        "            'Si' " +
        "        else " +
        "            'No' " +
        "        end    " +
        "    else" +
        "        val.label" +
        "    end as valor " +
        "from qua_first_levels lev " +
        "    left join cnt_types typ on (lev.first_level_id = typ.first_level_id and typ.enabled) " +
        "    left outer join pro_qualities qua on (qua.cnt_quality_type_id = typ.type_id) " +
        "    left outer join cnt_values val on (qua.cnt_quality_value_id = val.value_id and val.enabled) " +
        "where qua.promotion_id in (select promotion_id from stu_promotions where study_id = :estudio) " +
        "order by qua.promotion_id asc, lev.first_level_id asc " //--, typ.type_id asc"
}