// Mensajes genéricos

const INFO_OK = {
  code: 0,
  message: "Petición procesada correctamente"    
};

const INFO_OK_PROMO_VENDIDA_OK = {
  code: 0,
  message: "Petición procesada correctamente. El programa y los inmuebles se han modificado como vendidos."    
};
// Mensajes de Error

const ERROR_GENERICO_REGISTRO = {
  code: 100,
  message: "La autenticación ha fallado."
}

const ERROR_MAIL_DUPLICADO = {
  code: 101,
  message: "Ya existe un usuario con ese email"
}

const ERROR_USER_PASSWORD = {
  code: 102,
  message: "Usuario o password incorrectos."
}

const ERROR_VERSION_NOTEQUAL = {
  code: 103,
  message: "Las versiones informadas no coinciden."
}

const ERROR_VERSION_NO_INFORMADA = {
  code: 104,
  message: "Se ha de informar de un número de versión."
}

const ERROR_TRANSICION_ESTADO_NO_VALIDO = {
  code :105,
  message: "La transición de estados no es válida"
}

const ERROR_RECOGER_GET = {
  code: 110,
  message: "Ha ocurrido un error al procesar la solicitud. Inténtelo de nuevo más tarde."
}

const ERROR_DATOS_INCONSISTENTES = {
  code: 111,
  message: "Los datos introducidos son erróneos."
}

const ERROR_PARAMETROS_INCORRECTOS = {
  code: 112,
  message: "Los parámetros introducidos no son correctos."
}


const ERROR_USUARIO_NO_VALIDO = {
  code: 113,
  message: "El usuario introducido no es válido"
}

const ERROR_ASIGNAR_ESTUDIO = {
  code: 114,
  message: "El estudio no se ha podido asignar correctamente"
}

const ERROR_USUARIO_BASICO_UN_ESTUDIO = {
  code: 115,
  message: "Un usuario básico solo puede tener un estudio asignado"
}

const ERROR_ESTUDIO_YA_ASIGNADO = {
  code: 116,
  message: "El estudio ya está asignado al usuario"
}


const ERROR_USUARIO_NO_PUEDE_ASIGNAR_ESTUDIOS = {
  code: 117,
  message: "Al usuario no se le pueden asignar estudios"
}


const ERROR_STUDY_CODE_DUPLICADO = {
  code: 118,
  message: "Ya existe un estudio con ese código"
}

const ERROR_FILE_UPLOAD_ERROR = {
  code: 119,
  message: "El fichero no se ha subido correctamente"
}

const ERROR_STUDY_CREATED_SP_FAIL = {
  code: 120,
  message: "El estudio se ha creado pero la carpeta en sharepoint ha fallado."
}
const ERROR_MAIL_NO_AEDAS = {
  code: 121,
  message: "El email introducido no existe en el directorio de AEDAS"
}


const ERROR_GENERICO_BBDD = {
  code: 200,
  message: "Se ha producido un error en BBDD."
}

const ERROR_GENERICO_INSERT = {
  code: 201,
  message: "No se ha podido insertar el registro."
}

const ERROR_DELETE_NO_ROWS = {
  code: 202,
  message: "No se ha podido borrar el registro."
}

const ERROR_UPDATE_NO_ROWS = {
  code: 203,
  message: "No se ha podido actualizar el registro."
}

const ERROR_GENERICO_UPDATE = {
  code: 204,
  message: "La actualización ha fallado."
}

const ERROR_NO_EXISTEN_REGISTROS = {
  code: 205,
  message: "No se encuentran registros"
}

const ERROR_GENERICO = {
  code: 300,
  message: "Error genérico."    
};



module.exports = {
    INFO_OK,
    INFO_OK_PROMO_VENDIDA_OK,
    ERROR_GENERICO_REGISTRO,
    ERROR_MAIL_DUPLICADO,
    ERROR_USER_PASSWORD,
    ERROR_VERSION_NOTEQUAL,
    ERROR_VERSION_NO_INFORMADA,
    ERROR_TRANSICION_ESTADO_NO_VALIDO,
    ERROR_RECOGER_GET,
    ERROR_DATOS_INCONSISTENTES,
    ERROR_PARAMETROS_INCORRECTOS,
    ERROR_ASIGNAR_ESTUDIO,
    ERROR_USUARIO_BASICO_UN_ESTUDIO,
    ERROR_USUARIO_NO_PUEDE_ASIGNAR_ESTUDIOS,
    ERROR_STUDY_CODE_DUPLICADO,
    ERROR_FILE_UPLOAD_ERROR,
    ERROR_MAIL_NO_AEDAS,
    ERROR_STUDY_CREATED_SP_FAIL,
    ERROR_ESTUDIO_YA_ASIGNADO,
    ERROR_USUARIO_NO_VALIDO,
    ERROR_GENERICO_BBDD,
    ERROR_GENERICO_INSERT,
    ERROR_GENERICO_UPDATE,
    ERROR_DELETE_NO_ROWS,
    ERROR_UPDATE_NO_ROWS,
    ERROR_NO_EXISTEN_REGISTROS,
    ERROR_GENERICO,
};