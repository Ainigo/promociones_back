const Sequelize = require("sequelize");

var sequelize = new Sequelize(
  process.env.database,
  process.env.user,
  process.env.password,
  {
    host: process.env.host,
    port: process.env.dbPort,
    logging: false,
    dialect: "postgres",
    define: {
      timestamps: true,
      underscored: true,
      freezeTableName: true //prevent sequelize from pluralizing table names
    },
    operatorsAliases: false,
    pool: {
      max: process.env.max,
      min: process.env.min,
      acquire: 30000,
      idle: process.env.idleTimeoutMillis
    }
  }
);

require("sequelize-values")(sequelize);

module.exports = sequelize;
