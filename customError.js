function CustomError(message) {
    this.type = 'custom';
    this.message = message || '';
    var error = new Error(this.message);
    error.type = this.name;
    this.stack = error.stack;
  }
  CustomError.prototype = Object.create(Error.prototype);

function CustomError(err) {
  this.type = 'custom';
  this.message = err.message;
  this.code = err.code;
  var error = new Error(this.message);
  error.type = this.name;
  this.stack = error.stack;
}
CustomError.prototype = Object.create(Error.prototype);
  
module.exports = CustomError;