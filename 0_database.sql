/*
Created: 06/03/2019
Modified: 10/07/2019
Model: PostgreSQL 9.5
Database: PostgreSQL 9.5
*/


-- Create tables section -------------------------------------------------

-- Table public.usr_users

CREATE TABLE "public"."usr_users"(
 "user_id" Serial NOT NULL,
 "role_id" Integer NOT NULL,
 "email" Bytea NOT NULL,
 "name" Bytea NOT NULL,
 "valid" Boolean DEFAULT true,
 "territorial_id" Integer,
 "last_login" Timestamp,
 "created_at" Timestamp,
 "updated_at" Timestamp
)
WITH (
 autovacuum_enabled=true)
;

-- Create indexes for table public.usr_users

CREATE INDEX "ix_role" ON "public"."usr_users" ("role_id")
;

-- Add keys for table public.usr_users

ALTER TABLE "public"."usr_users" ADD CONSTRAINT "pk_usr_users" PRIMARY KEY ("user_id")
;

ALTER TABLE "public"."usr_users" ADD CONSTRAINT "user_id" UNIQUE ("user_id")
;

ALTER TABLE "public"."usr_users" ADD CONSTRAINT "email" UNIQUE ("email")
;

-- Table public.usr_rols

CREATE TABLE "public"."usr_rols"(
 "role_id" Serial NOT NULL,
 "role_name" Character varying(150)
)
WITH (
 autovacuum_enabled=true)
;

-- Add keys for table public.usr_rols

ALTER TABLE "public"."usr_rols" ADD CONSTRAINT "pk_usr_rols" PRIMARY KEY ("role_id")
;

ALTER TABLE "public"."usr_rols" ADD CONSTRAINT "role_id" UNIQUE ("role_id")
;

-- Table public.stu_market_studies

CREATE TABLE "public"."stu_market_studies"(
 "study_id" Serial NOT NULL,
 "code" Character varying(100) NOT NULL,
 "city" Character varying(250),
 "zone" Character varying(250),
 "territorial_id" Integer NOT NULL,
 "status_id" Integer NOT NULL,
 "created_at" Timestamp NOT NULL,
 "updated_at" Timestamp,
 "region_id" Integer NOT NULL,
 "enabled" Boolean DEFAULT true NOT NULL
)
WITH (
 autovacuum_enabled=true)
;

-- Add keys for table public.stu_market_studies

ALTER TABLE "public"."stu_market_studies" ADD CONSTRAINT "pk_stu_market_studies" PRIMARY KEY ("study_id")
;

ALTER TABLE "public"."stu_market_studies" ADD CONSTRAINT "study_id" UNIQUE ("study_id")
;

ALTER TABLE "public"."stu_market_studies" ADD CONSTRAINT "code" UNIQUE ("code")
;

-- Table public.stu_market_users

CREATE TABLE "public"."stu_market_users"(
 "user_id" Integer NOT NULL,
 "study_id" Integer NOT NULL,
 "created_on" Date
)
WITH (
 autovacuum_enabled=true)
;

-- Add keys for table public.stu_market_users

ALTER TABLE "public"."stu_market_users" ADD CONSTRAINT "pk_stu_market_users" PRIMARY KEY ("user_id","study_id")
;

-- Table public.url_urls

CREATE TABLE "public"."url_urls"(
 "url" Character varying(200) NOT NULL,
 "url_id" Serial NOT NULL
)
WITH (
 autovacuum_enabled=true)
;

-- Add keys for table public.url_urls

ALTER TABLE "public"."url_urls" ADD CONSTRAINT "pk_url_urls" PRIMARY KEY ("url_id")
;

ALTER TABLE "public"."url_urls" ADD CONSTRAINT "url" UNIQUE ("url")
;

ALTER TABLE "public"."url_urls" ADD CONSTRAINT "url_id" UNIQUE ("url_id")
;

-- Table public.url_rights

CREATE TABLE "public"."url_rights"(
 "url_id" Integer NOT NULL,
 "role_id" Integer NOT NULL,
 "methods" Character varying(50) NOT NULL
)
WITH (
 autovacuum_enabled=true)
;
COMMENT ON COLUMN "public"."url_rights"."methods" IS 'List of available methods (GET, POST, PUT, DELETE) for this role'
;

-- Add keys for table public.url_rights

ALTER TABLE "public"."url_rights" ADD CONSTRAINT "pk_url_rights" PRIMARY KEY ("url_id","role_id")
;

-- Table public.usr_tokens

CREATE TABLE "public"."usr_tokens"(
 "token" Character varying(250) NOT NULL,
 "created_at" Timestamp,
 "updated_at" Timestamp,
 "user_id" Integer NOT NULL,
 "token_id" Serial NOT NULL
)
WITH (
 autovacuum_enabled=true)
;

-- Create indexes for table public.usr_tokens

CREATE INDEX "ix_user_token" ON "public"."usr_tokens" ("user_id")
;

-- Add keys for table public.usr_tokens

ALTER TABLE "public"."usr_tokens" ADD CONSTRAINT "pk_usr_tokens" PRIMARY KEY ("token_id")
;

ALTER TABLE "public"."usr_tokens" ADD CONSTRAINT "token" UNIQUE ("token")
;

ALTER TABLE "public"."usr_tokens" ADD CONSTRAINT "token_id" UNIQUE ("token_id")
;

-- Table public.cnt_types

CREATE TABLE "public"."cnt_types"(
 "type_id" Serial NOT NULL,
 "type_value" Character varying(200) NOT NULL,
 "type_label" Character varying(100) NOT NULL,
 "first_level_id" Integer,
 "enabled" Boolean DEFAULT true NOT NULL,
 "data_type" Integer DEFAULT 1 NOT NULL
)
WITH (
 autovacuum_enabled=true)
;
COMMENT ON COLUMN "public"."cnt_types"."data_type" IS '1 - Desplegable
2 - boolean
3 - numeric'
;

-- Create indexes for table public.cnt_types

CREATE INDEX "ix_qua_first_level" ON "public"."cnt_types" ("first_level_id")
;

-- Add keys for table public.cnt_types

ALTER TABLE "public"."cnt_types" ADD CONSTRAINT "pk_cnt_types" PRIMARY KEY ("type_id")
;

ALTER TABLE "public"."cnt_types" ADD CONSTRAINT "cnt_type_id" UNIQUE ("type_id")
;

-- Table public.cnt_values

CREATE TABLE "public"."cnt_values"(
 "value_id" Serial NOT NULL,
 "type_id" Integer NOT NULL,
 "value" Character varying(250) NOT NULL,
 "label" Character varying(100) NOT NULL,
 "enabled" Boolean DEFAULT true NOT NULL
)
WITH (
 autovacuum_enabled=true)
;

-- Create indexes for table public.cnt_values

CREATE INDEX "ix_type_value" ON "public"."cnt_values" ("type_id")
;

-- Add keys for table public.cnt_values

ALTER TABLE "public"."cnt_values" ADD CONSTRAINT "pk_cnt_values" PRIMARY KEY ("value_id")
;

ALTER TABLE "public"."cnt_values" ADD CONSTRAINT "value_id" UNIQUE ("value_id")
;

ALTER TABLE "public"."cnt_values" ADD CONSTRAINT "unique_value" UNIQUE ("value","type_id")
;

-- Table public.stu_promotions

CREATE TABLE "public"."stu_promotions"(
 "promotion_id" Serial NOT NULL,
 "promotion_type_id" Integer NOT NULL,
 "protection_type_id" Integer NOT NULL,
 "promoter" Bytea NOT NULL,
 "name" Bytea NOT NULL,
 "environment_id" Integer NOT NULL,
 "address" Bytea,
 "coordinates" Bytea,
 "marketer" Bytea,
 "sales_start_date" Date NOT NULL,
 "web" Bytea,
 "telephone" Bytea,
 "architect" Bytea,
 "comparable" Boolean NOT NULL,
 "observations" Character varying(4000),
 "study_id" Integer NOT NULL,
 "created_at" Timestamp NOT NULL,
 "updated_at" Timestamp
)
WITH (
 autovacuum_enabled=true)
;

-- Create indexes for table public.stu_promotions

CREATE INDEX "ix_study_programs" ON "public"."stu_promotions" ("study_id")
;

-- Add keys for table public.stu_promotions

ALTER TABLE "public"."stu_promotions" ADD CONSTRAINT "pk_stu_promotions" PRIMARY KEY ("promotion_id")
;

ALTER TABLE "public"."stu_promotions" ADD CONSTRAINT "promotion_id" UNIQUE ("promotion_id")
;

-- Table public.pro_promotion_versions

CREATE TABLE "public"."pro_promotion_versions"(
 "work_status_id" Integer NOT NULL,
 "delivery_date" Date NOT NULL,
 "promotion_id" Integer NOT NULL,
 "version_id" Integer NOT NULL,
 "created_at" Timestamp NOT NULL,
 "updated_at" Timestamp
)
WITH (
 autovacuum_enabled=true)
;

-- Add keys for table public.pro_promotion_versions

ALTER TABLE "public"."pro_promotion_versions" ADD CONSTRAINT "pk_pro_promotion_versions" PRIMARY KEY ("version_id","promotion_id")
;

-- Table public.pro_program

CREATE TABLE "public"."pro_program"(
 "program_id" Serial NOT NULL,
 "initial" Integer,
 "stock" Integer,
 "rooms" Integer NOT NULL,
 "version_id" Integer NOT NULL,
 "promotion_id" Integer NOT NULL,
 "created_at" Timestamp NOT NULL,
 "updated_at" Timestamp
)
WITH (
 autovacuum_enabled=true)
;

-- Create indexes for table public.pro_program

CREATE INDEX "ix_promo_program" ON "public"."pro_program" ("promotion_id")
;

-- Add keys for table public.pro_program

ALTER TABLE "public"."pro_program" ADD CONSTRAINT "pk_pro_program" PRIMARY KEY ("program_id","version_id")
;

-- Table public.pro_buildings

CREATE TABLE "public"."pro_buildings"(
 "building_id" Serial NOT NULL,
 "rooms" Integer NOT NULL,
 "bathrooms" Integer NOT NULL,
 "terrace_area" Numeric NOT NULL,
 "pvp_commercial" Integer NOT NULL,
 "status_id" Integer NOT NULL,
 "promotion_id" Integer NOT NULL,
 "building_type_id" Integer NOT NULL,
 "version_id" Integer NOT NULL,
 "created_at" Timestamp NOT NULL,
 "updated_at" Timestamp,
 "observations" Character varying(4000)
)
WITH (
 autovacuum_enabled=true)
;

-- Create indexes for table public.pro_buildings

CREATE INDEX "ix_promo_buildings" ON "public"."pro_buildings" ("promotion_id")
;

CREATE INDEX "ix_version_buildings" ON "public"."pro_buildings" ("version_id")
;

-- Add keys for table public.pro_buildings

ALTER TABLE "public"."pro_buildings" ADD CONSTRAINT "pk_pro_buildings" PRIMARY KEY ("building_id","version_id")
;

-- Table public.pro_building_multi

CREATE TABLE "public"."pro_building_multi"(
 "floor_id" Integer NOT NULL,
 "util_area" Numeric,
 "constructed_area" Numeric,
 "garage" Boolean NOT NULL,
 "garages_number" Integer,
 "pvp_garage" Numeric,
 "storage" Boolean NOT NULL,
 "pvp_storage" Numeric,
 "building_id" Integer NOT NULL,
 "version_id" Integer NOT NULL
)
WITH (
 autovacuum_enabled=true)
;

-- Add keys for table public.pro_building_multi

ALTER TABLE "public"."pro_building_multi" ADD CONSTRAINT "pk_pro_building_multi" PRIMARY KEY ("building_id","version_id")
;

-- Table public.pro_building_single

CREATE TABLE "public"."pro_building_single"(
 "plot_area" Numeric NOT NULL,
 "sbr" Numeric NOT NULL,
 "spb" Numeric NOT NULL,
 "spp" Numeric NOT NULL,
 "sbc" Numeric NOT NULL,
 "sbr_constructed" Numeric NOT NULL,
 "spb_constructed" Numeric NOT NULL,
 "spp_constructed" Numeric NOT NULL,
 "sbc_constructed" Numeric NOT NULL,
 "single_type_id" Integer NOT NULL,
 "building_id" Integer NOT NULL,
 "version_id" Integer NOT NULL
)
WITH (
 autovacuum_enabled=true)
;

-- Add keys for table public.pro_building_single

ALTER TABLE "public"."pro_building_single" ADD CONSTRAINT "pk_pro_building_single" PRIMARY KEY ("building_id","version_id")
;

-- Table public.pro_finances

CREATE TABLE "public"."pro_finances"(
 "banc" Character varying(250),
 "reserve" Numeric,
 "on_sign" Integer,
 "postponed_during_construction" Integer,
 "additional_payment" Numeric,
 "mortgage" Integer,
 "endorse" Boolean,
 "promotion_id" Integer NOT NULL,
 "deferment_payments_id" Integer,
 "created_at" Timestamp NOT NULL,
 "updated_at" Timestamp
)
WITH (
 autovacuum_enabled=true)
;

-- Add keys for table public.pro_finances

ALTER TABLE "public"."pro_finances" ADD CONSTRAINT "pk_pro_finances" PRIMARY KEY ("promotion_id")
;

-- Table public.stu_second_hand_rent

CREATE TABLE "public"."stu_second_hand_rent"(
 "study_id" Integer NOT NULL,
 "shr_id" Serial NOT NULL,
 "transaction_id" Integer NOT NULL,
 "address" Bytea,
 "coordinates" Bytea,
 "year" Integer,
 "second_hand_status_id" Integer NOT NULL,
 "floor" Integer NOT NULL,
 "rooms" Integer NOT NULL,
 "bathrooms" Integer NOT NULL,
 "area" Numeric NOT NULL,
 "price" Numeric NOT NULL,
 "commercial_status_id" Integer NOT NULL,
 "url" Bytea NOT NULL,
 "comments" Character varying(4000),
 "registry" Character varying(10000),
 "created_at" Timestamp NOT NULL,
 "updated_at" Timestamp
)
WITH (
 autovacuum_enabled=true)
;

-- Create indexes for table public.stu_second_hand_rent

CREATE INDEX "ix_study_shr" ON "public"."stu_second_hand_rent" ("study_id")
;

-- Add keys for table public.stu_second_hand_rent

ALTER TABLE "public"."stu_second_hand_rent" ADD CONSTRAINT "pk_stu_second_hand_rent" PRIMARY KEY ("shr_id")
;

ALTER TABLE "public"."stu_second_hand_rent" ADD CONSTRAINT "shr_id" UNIQUE ("shr_id")
;

-- Table public.stu_next_promotions

CREATE TABLE "public"."stu_next_promotions"(
 "next_promotion_id" Serial NOT NULL,
 "promo_type_id" Integer NOT NULL,
 "promoter" Bytea NOT NULL,
 "name" Bytea NOT NULL,
 "environment_id" Integer NOT NULL,
 "address" Bytea,
 "coordinates" Bytea,
 "study_id" Integer NOT NULL,
 "created_at" Timestamp NOT NULL,
 "updated_at" Timestamp
)
WITH (
 autovacuum_enabled=true)
;

-- Create indexes for table public.stu_next_promotions

CREATE INDEX "ix_study_next_promos" ON "public"."stu_next_promotions" ("study_id")
;

-- Add keys for table public.stu_next_promotions

ALTER TABLE "public"."stu_next_promotions" ADD CONSTRAINT "pk_stu_next_promotions" PRIMARY KEY ("next_promotion_id")
;

ALTER TABLE "public"."stu_next_promotions" ADD CONSTRAINT "next_promotion_id" UNIQUE ("next_promotion_id")
;

-- Table public.qua_first_levels

CREATE TABLE "public"."qua_first_levels"(
 "first_level_id" Serial NOT NULL,
 "value" Character varying(100) NOT NULL,
 "label" Character varying(100) NOT NULL,
 "enabled" Boolean DEFAULT true NOT NULL,
 "order" Integer NOT NULL
)
WITH (
 autovacuum_enabled=true)
;

-- Add keys for table public.qua_first_levels

ALTER TABLE "public"."qua_first_levels" ADD CONSTRAINT "pk_qua_first_levels" PRIMARY KEY ("first_level_id")
;

ALTER TABLE "public"."qua_first_levels" ADD CONSTRAINT "first_level_id" UNIQUE ("first_level_id")
;

-- Table public.pro_qualities

CREATE TABLE "public"."pro_qualities"(
 "promotion_id" Integer NOT NULL,
 "cnt_quality_value_id" Integer,
 "cnt_quality_type_id" Integer NOT NULL,
 "value" Boolean
)
WITH (
 autovacuum_enabled=true)
;

-- Create indexes for table public.pro_qualities

CREATE INDEX "ix_quality_values" ON "public"."pro_qualities" ("cnt_quality_value_id")
;

-- Add keys for table public.pro_qualities

ALTER TABLE "public"."pro_qualities" ADD CONSTRAINT "pk_pro_qualities" PRIMARY KEY ("promotion_id","cnt_quality_type_id")
;

-- Table public.stu_versions

CREATE TABLE "public"."stu_versions"(
 "version_id" Serial NOT NULL,
 "created_at" Date NOT NULL,
 "updated_at" Date,
 "version_number" Integer NOT NULL,
 "study_id" Integer NOT NULL
)
WITH (
 autovacuum_enabled=true)
;

-- Create indexes for table public.stu_versions

CREATE INDEX "ix_study_verson" ON "public"."stu_versions" ("study_id")
;

-- Add keys for table public.stu_versions

ALTER TABLE "public"."stu_versions" ADD CONSTRAINT "pk_stu_versions" PRIMARY KEY ("version_id")
;

ALTER TABLE "public"."stu_versions" ADD CONSTRAINT "version_id" UNIQUE ("version_id")
;
-- Create foreign keys (relationships) section ------------------------------------------------- 

ALTER TABLE "public"."usr_users" ADD CONSTRAINT "role" FOREIGN KEY ("role_id") REFERENCES "public"."usr_rols" ("role_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."stu_market_users" ADD CONSTRAINT "users_markets" FOREIGN KEY ("user_id") REFERENCES "public"."usr_users" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."stu_market_users" ADD CONSTRAINT "markets_users" FOREIGN KEY ("study_id") REFERENCES "public"."stu_market_studies" ("study_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."url_rights" ADD CONSTRAINT "right_url" FOREIGN KEY ("url_id") REFERENCES "public"."url_urls" ("url_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."url_rights" ADD CONSTRAINT "rol_right" FOREIGN KEY ("role_id") REFERENCES "public"."usr_rols" ("role_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."usr_tokens" ADD CONSTRAINT "user_token" FOREIGN KEY ("user_id") REFERENCES "public"."usr_users" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."cnt_values" ADD CONSTRAINT "type_value" FOREIGN KEY ("type_id") REFERENCES "public"."cnt_types" ("type_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_promotion_versions" ADD CONSTRAINT "promo_versiones" FOREIGN KEY ("promotion_id") REFERENCES "public"."stu_promotions" ("promotion_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."stu_promotions" ADD CONSTRAINT "study_promotions" FOREIGN KEY ("study_id") REFERENCES "public"."stu_market_studies" ("study_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_program" ADD CONSTRAINT "promo_program" FOREIGN KEY ("promotion_id") REFERENCES "public"."stu_promotions" ("promotion_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_buildings" ADD CONSTRAINT "promo_buildings" FOREIGN KEY ("promotion_id") REFERENCES "public"."stu_promotions" ("promotion_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_building_multi" ADD CONSTRAINT "build_multi" FOREIGN KEY ("building_id", "version_id") REFERENCES "public"."pro_buildings" ("building_id", "version_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_building_single" ADD CONSTRAINT "build_single" FOREIGN KEY ("building_id", "version_id") REFERENCES "public"."pro_buildings" ("building_id", "version_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_finances" ADD CONSTRAINT "promo_finances" FOREIGN KEY ("promotion_id") REFERENCES "public"."stu_promotions" ("promotion_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."stu_second_hand_rent" ADD CONSTRAINT "study_shr" FOREIGN KEY ("study_id") REFERENCES "public"."stu_market_studies" ("study_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."stu_next_promotions" ADD CONSTRAINT "study_next_promos" FOREIGN KEY ("study_id") REFERENCES "public"."stu_market_studies" ("study_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_qualities" ADD CONSTRAINT "promo_qualities" FOREIGN KEY ("promotion_id") REFERENCES "public"."stu_promotions" ("promotion_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_qualities" ADD CONSTRAINT "quality_value" FOREIGN KEY ("cnt_quality_value_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."cnt_types" ADD CONSTRAINT "first_to_second" FOREIGN KEY ("first_level_id") REFERENCES "public"."qua_first_levels" ("first_level_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_qualities" ADD CONSTRAINT "quality_type" FOREIGN KEY ("cnt_quality_type_id") REFERENCES "public"."cnt_types" ("type_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_promotion_versions" ADD CONSTRAINT "study_version" FOREIGN KEY ("version_id") REFERENCES "public"."stu_versions" ("version_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_buildings" ADD CONSTRAINT "building_versions" FOREIGN KEY ("version_id") REFERENCES "public"."stu_versions" ("version_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."stu_versions" ADD CONSTRAINT "study_versions" FOREIGN KEY ("study_id") REFERENCES "public"."stu_market_studies" ("study_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_program" ADD CONSTRAINT "program_versions" FOREIGN KEY ("version_id") REFERENCES "public"."stu_versions" ("version_id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "public"."pro_finances" ADD CONSTRAINT "fk_pro_deferment_payments_id" FOREIGN KEY ("deferment_payments_id") REFERENCES "public"."cnt_values" ("value_id") ON DELETE NO ACTION ON UPDATE NO ACTION;


CREATE INDEX "ix_second_hand_status_id" ON "public".stu_second_hand_rent("second_hand_status_id");
CREATE INDEX "ix_commercial_status_id" ON "public".stu_second_hand_rent("commercial_status_id");
CREATE INDEX "ix_transaction_id" ON "public".stu_second_hand_rent("transaction_id");
CREATE INDEX "ix_mkt_status_id" ON "public".stu_market_studies("status_id");
CREATE INDEX "ix_region_id" ON "public".stu_market_studies("region_id");
CREATE INDEX "ix_promo_type_id" ON "public".stu_next_promotions("promo_type_id");
CREATE INDEX "ix_promotion_type_id" ON "public".stu_promotions("promotion_type_id");
CREATE INDEX "ix_protection_type_id" ON "public".stu_promotions("protection_type_id");
CREATE INDEX "ix_environment_id" ON "public".stu_promotions("environment_id");
CREATE INDEX "ix_work_status_id" ON "public".pro_promotion_versions("work_status_id");
CREATE INDEX "ix_building_status_id" ON "public".pro_buildings("status_id");
CREATE INDEX "ix_building_type_id" ON "public".pro_buildings("building_type_id");
CREATE INDEX "ix_single_type_id" ON "public".pro_building_single("single_type_id");
CREATE INDEX "ix_floor_id" ON "public".pro_building_multi("floor_id");
CREATE INDEX "ix_usr_territorial_id" ON "public".usr_users("territorial_id");
CREATE INDEX "ix_mkt_territorial_id" ON "public".stu_market_studies("territorial_id");
CREATE INDEX "ix_deferment_payments_id" ON "public".pro_finances("deferment_payments_id");
