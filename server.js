const express = require("express");
const app = express();
var cors = require("cors");
require("dotenv").config();
require("pg-promise")(/*options*/);
require("./config/config");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const logger = require("./config/logger");
const router = require("./router");
const utilModels = require("./models/shared");
const Configbbdd = require("./global/config");


// Cross Origin middleware
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
  next()
})

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  morgan("dev", {
    skip: function(req, res) {
      return res.statusCode < 400;
    },
    stream: process.stderr
  })
);
app.use(
  morgan("dev", {
    skip: function(req, res) {
      return res.statusCode >= 400;
    },
    stream: process.stdout
  })
);


app.use(function(req, res, next) {
  // Toda petición pasaría por aqui
  utilModels.encriptData(req);
  next();
});

app.get("/", function(req, res) {
  logger.info("Info statement");
  logger.error("Error statement");
  res.send("Hello World!");
});

router.set(app);

const port = process.env.PORT || Configbbdd.conexionLocal.idProd;
app.listen(port, () => logger.info("App listening on port " + port));
