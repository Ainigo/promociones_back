"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const config = require("../config/config");
const logger = require("../config/logger");
const CustomError = require("../customError");
const jwt = require("jsonwebtoken");
const userService = require("../services/user");
const message = require("../config/messages");
const azure = require("./authAzure");
//const authenticate = params => {
function authenticate(params) {
    return __awaiter(this, void 0, void 0, function* () {
        logger.debug("Autenticamos");
        var res = yield azure.authenticateAzure(params.email, params.password);
        if (!res) {
            throw new CustomError(message.ERROR_USER_PASSWORD);
        }
        else {
            logger.debug("El usuario es correcto");
        }
        return userService.getUser(params.email).then(user => {
            // Chequeamos que el usuario existe y la contraseña sea correcta
            /**
             * Esto hay que cambiarlo por una consulta al servicio de LDAP de Azure para comprobar que el usuario tiene permisos.
             */
            if (!user) {
                throw new CustomError(message.ERROR_USER_PASSWORD);
            }
            logger.info(JSON.stringify(user));
            userService.updateLastLoginDate(user);
            // Construimos el payload
            const payload = {
                login: user.login,
                id: user.user_id,
                time: new Date()
            };
            var token = jwt.sign(payload, config.jwtSecret, {
                expiresIn: config.tokenExpireTime
            });
            const ret = {
                token: token,
                rol: user.usr_rol
            };
            //Guardamos el Token en BBDD en la tabla usr_tokens
            var usrToken = {
                token: token,
                user_id: user.user_id,
                tkn_type_id: 1
            };
            userService.addToken(usrToken);
            return ret;
        });
    });
}
;
module.exports = {
    authenticate
};
