"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const config = require("../config/config.");
const msRestAzure = require('ms-rest-azure');
const request = require('request-promise');
function executeRequest(requestedMethod, targetUrl, token, body) {
    return __awaiter(this, void 0, void 0, function* () {
        logger.debug("targetUrl: " + targetUrl);
        // Set the headers
        var headers = {
            'User-Agent': 'Super Agent/0.0.1',
            json: true,
            'Authorization': 'Bearer ' + token,
        };
        // Configure the request
        var options = {
            url: targetUrl,
            method: requestedMethod,
            headers: headers
        };
        if (requestedMethod == 'POST') {
            options.headers['Content-Type'] = 'application/json';
        }
        else if (requestedMethod == 'PUT') {
            options.headers['Content-Type'] = 'application/octet-stream';
        }
        else {
            options.headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }
        if (body != null && body != undefined) {
            options.body = body;
        }
        return yield request(options);
    });
}
function getAzureToken() {
    return __awaiter(this, void 0, void 0, function* () {
        var headers = {
            'User-Agent': 'Super Agent/0.0.1',
            'Content-Type': 'application/x-www-form-urlencoded'
        };
        // Configure the request
        var options = {
            url: config.azure.LoginUrl.replace("{{directoryId}}", config.auth.tenant),
            method: 'POST',
            headers: headers,
            form: {
                grant_type: 'client_credentials',
                client_id: config.auth.clientId,
                scope: 'https://graph.microsoft.com/.default',
                client_secret: config.auth.secret
            }
        };
        var res = yield request(options);
        return JSON.parse(res).access_token;
    });
}
function exitsUserOnAzure(usuario) {
    return __awaiter(this, void 0, void 0, function* () {
        var exist = false;
        // Set the headers
        var headers = {
            'User-Agent': 'Super Agent/0.0.1',
            'Content-Type': 'application/x-www-form-urlencoded'
        };
        // Configure the request
        var options = {
            url: config.azure.LoginUrl.replace("{{directoryId}}", config.auth.tenant),
            method: 'POST',
            headers: headers,
            form: {
                grant_type: 'client_credentials',
                client_id: config.auth.clientId,
                scope: 'https://graph.microsoft.com/.default',
                client_secret: config.auth.secret
            }
        };
        // Start the request
        var result = yield request(options);
        var token = JSON.parse(result).access_token;
        var userUrl = config.azure.getUser + usuario;
        result = yield executeRequest('GET', userUrl, token, null);
        var azureUser = JSON.parse(result).userPrincipalName;
        if (usuario.toString().toUpperCase() == azureUser.toString().toUpperCase()) {
            exist = true;
        }
        return exist;
    });
}
function authenticateAzure(usuario, password) {
    return __awaiter(this, void 0, void 0, function* () {
        if (usuario.indexOf("@aedashomes.com") < -0) {
            usuario = 'notificaciones@aedashomes.com';
            password = 'usuario@0';
        }
        else {
            logger.debug('Validamos un usuario de negocio');
        }
        return yield msRestAzure.loginWithUsernamePasswordWithAuthResponse(usuario, password, {
            tokenAudience: 'graph',
            domain: config.auth.tenant
        }).then(res => {
            return true;
        });
    });
}
