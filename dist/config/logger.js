"use strict";
const winston = require("winston");
const { createLogger, format, transports } = require('winston');
const fs = require('fs');
require('winston-daily-rotate-file');
require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = 'log';
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}
const dailyRotateFileTransport = new transports.DailyRotateFile({
    filename: `${logDir}/%DATE%-results.log`,
    datePattern: 'YYYY-MM-DD'
});
const logger = createLogger({
    // change level if in dev environment versus production
    level: env === 'development' ? 'verbose' : 'info',
    format: format.combine(format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }), format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)),
    transports: [
        new winston.transports.File({
            filename: logDir + '/error.log',
            format: format.combine(format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)),
            level: 'error'
        }),
        new winston.transports.File({ filename: logDir + '/combined.log', level: 'info' }),
        new transports.Console({
            level: 'debug',
            format: format.combine(format.colorize(), format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`))
        }),
        dailyRotateFileTransport
    ]
});
module.exports = logger;
