"use strict";
module.exports = {
    auth: {
        clientId: "42a0fcdd-3be9-4ba8-a616-f781344ed8f3",
        secret: "EEVL3L0.RzeBczBML-GyYP3Vi:DHeO*1",
        tenant: '1eac135e-ccdf-4d83-8b4f-efc9038309f0'
    },
    jwtSecret: "yo-its-a-secret",
    tokenExpireTime: "80d",
    defaultStudyStatusValue: "abierto",
    defaultStudyStatusType: "Estado estudio",
    uniBuilding: "unifamiliar",
    pluriBuilding: "plurifamiliar",
    studyStatus: {
        abierto: "abierto",
        enRevision: "revision",
        cerrado: "cerrado",
        nuevaVersion: "nuevaVersion"
    },
    users: {
        basico: "1",
        gerente: "2",
        admin: "3",
        territorial: "4"
    },
    email: {
        asunto: 'AEDAS: estudio de mercado {codigo_estudio} actualizado',
        message: "Estimado usuario\n\nSe ha realizado una actualización de estado del estudio {codigo_estudio} a {estado}",
        footer: "\n\nPor favor no responda a este email"
    },
    new_user_notification_email: {
        asunto: 'AEDAS: bienvenido {username}',
        message: "Estimado {username}\n\nacaba de ser dado de alta en la aplicación Estudios de mercado de AEDAS Homes." +
            "\n\nPuede acceder al sistema haciendo click en https://estudios.abast.es" +
            "\nReciba un cordial saludo.",
        footer: "\n\nPor favor no responda a este email"
    },
    new_user_study_assign_email: {
        asunto: 'AEDAS: nuevo estudio asingado',
        message: "Estimado {username}\n\ntienes nuevos estudios de mercado asignados en AEDAS Homes: {studyCodes}" +
            "\n\nPuede acceder al sistema haciendo click en https://estudios.abast.es" +
            "\nReciba un cordial saludo.",
        footer: "\n\nPor favor no responda a este email"
    },
    azure: {
        LoginUrl: "https://login.microsoftonline.com/{{directoryId}}/oauth2/v2.0/token",
        getUser: "https://graph.microsoft.com/v1.0/users/"
    },
    sharepoint: {
        emRootFoderId: "01BMJWMURQAC5BBTFW3JHZ2YXDBGUCAN3V",
        temporalFilesDir: "./public/images/uploads",
        routes: {
            baseFolder: "02.Pricing/01.Estudios%20de%20Mercado/08.%20Documentacion%20Comercial",
            baseFolderNextPromotions: "suelo",
            folderInfo: "https://graph.microsoft.com/v1.0/sites/1ed1b03a-2b16-48a9-b8af-9b1c5a7866e7,6a32cb5c-7c76-4d4a-8147-400a2c7f7be0/drive/root:/{pathToFolder}",
            listItems: "https://graph.microsoft.com/v1.0/sites/1ed1b03a-2b16-48a9-b8af-9b1c5a7866e7,6a32cb5c-7c76-4d4a-8147-400a2c7f7be0/lists/c96dea8c-e146-4e2d-a15c-0b1047f0fe1d/drive/root:/{pathToFolder}:/children",
            createFolder: "https://graph.microsoft.com/v1.0/sites/1ed1b03a-2b16-48a9-b8af-9b1c5a7866e7,6a32cb5c-7c76-4d4a-8147-400a2c7f7be0/lists/c96dea8c-e146-4e2d-a15c-0b1047f0fe1d/drive/items/{parent-item-id}/children",
            downloadItemAttribute: "@microsoft.graph.downloadUrl",
            uploadItem: "https://graph.microsoft.com/v1.0/sites/1ed1b03a-2b16-48a9-b8af-9b1c5a7866e7,6a32cb5c-7c76-4d4a-8147-400a2c7f7be0/lists/c96dea8c-e146-4e2d-a15c-0b1047f0fe1d/drive/items/root:/{pathToFolder}/{filename}:/content"
        }
    }
};
// No modifiques este fichero si no estás completamente seguro de lo que estás haciendo
