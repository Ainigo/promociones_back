"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const usuarios_lista_1 = require("../classes/usuarios-lista");
const usuario_1 = require("../classes/usuario");
const { base64encode, base64decode } = require('nodejs-base64');
exports.usuariosConectados = new usuarios_lista_1.UsuariosLista();
exports.conectarCliente = (cliente, io) => {
    const usuario = new usuario_1.Usuario(cliente.id);
    exports.usuariosConectados.agregar(usuario);
};
exports.desconectar = (cliente, io) => {
    cliente.on('disconnect', () => {
        console.log('Cliente desconectado');
        exports.usuariosConectados.borrarUsuario(cliente.id);
        io.emit('usuarios-activos', exports.usuariosConectados.getLista());
    });
};
// Escuchar mensajes
exports.mensaje = (cliente, io) => {
    cliente.on('mensaje', (payload) => {
        console.log('Mensaje recibido', payload);
        io.emit('mensaje-nuevo', payload);
    });
};
// Configurar usuario
exports.configurarUsuario = (cliente, io) => {
    cliente.on('configurar-usuario', (payload, callback) => {
        exports.usuariosConectados.actualizarNombre(cliente.id, payload.nombre);
        io.emit('usuarios-activos', exports.usuariosConectados.getLista());
        if (payload.pass) {
            let decoded = base64decode(payload.pass);
            const prom = new Promise((resolve, rej) => {
                resolve(exports.usuariosConectados.setLogin(payload.nombre, decoded));
            });
            Promise.all([prom]).then(values => {
                if (values[0]) {
                    exports.usuariosConectados.actualizarNombre(cliente.id, payload.nombre);
                    io.emit('usuarios-activos', exports.usuariosConectados.getLista());
                    callback({
                        ok: true,
                        mensaje: `Usuario ${payload.nombre}, configurado`
                    });
                }
                else {
                    callback({
                        ok: false
                    });
                }
            });
        }
    });
};
// Obtener Usuarios
exports.obtenerUsuarios = (cliente, io) => {
    cliente.on('obtener-usuarios', () => {
        io.to(cliente.id).emit('usuarios-activos', exports.usuariosConectados.getLista());
    });
};
