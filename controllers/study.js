const studyService = require("../services/study");
const logger = require("../config/logger");
const config = require("../config/config");
const messageService = require("../services/messagesManager");
const message = require("../config/messages");
const masterService = require("../services/master");
const userService = require("../services/user");
const sequelize = require("./../db/db");
const exportExcel = require("./../utils/excel");
const sendEmail = require('../utils/mail').sendNotifications;
const studyModel = require("../models/studies").MarketStudies;
const versionsStudyModel = require("../models/studies").VersionsStudy;
const studyUsersModel = require("../models").StudyUsers
const sharepoint = require("./../controllers/sharepoint");


function getStudiesByToken(req, res) {
  
  var token = req.headers.authorization.split(" ")[1];
  userService.getUserByToken(token).then(user => {
    if (user.usr_user.usr_rol.role_name == 'administrador') {
      // Es un admin--> le doy todo 
      studyService.getAllStudies().then(data => {
        res.status(200).send(data);
      });
    }else if (user.usr_user.usr_rol.role_name == 'D. Territorial') {
      // Es un D. Territorial--> le doy los que corresponden con su territorial
      studyService.getStudiesForTerritorial(user.usr_user.territorial.value_id).then(data => {
        res.status(200).send(data);
      });
    }else{      
      studyService.getStudiesByToken(token).then(data => {
        res.status(200).send(data);
      });
    }
  })

}

async function getStudyById(req, res) {
  let data = null;
  try {
    data = await studyService.getStudiesById(req.params.study_id);
    if (data == null){      
      messageService.buildMessageError(res, message.ERROR_NO_EXISTEN_REGISTROS);
    }else {
      res.status(200).send(data);
    }    
  } catch (error) {    
    console.log("Aqui llego?")
    messageService.buildMessageError(res, error);
  }
}

function exportToXLS(req, res) {
  return studyService.getStudiesById(req.params.study_id).then(study => {
    if (study != null){
      exportExcel.exportStudy(req, res,study);
    }else{
      messageService.buildMessageError(res, message.ERROR_NO_EXISTEN_REGISTROS);
    }    
  });
}

// TODO: hay que crear un studyversion asociado al estudio.
async function createStudy(req, res) {
    var transaction = null;
        var study = studyModel.build(req.body);
        var studyExist = await studyService.getStudyByCode(study.code);
        messageService.throwIfNecessaryMessageError(studyExist == null, message.ERROR_STUDY_CODE_DUPLICADO);

        if (study.enabled == undefined) {
          study.enabled = true;
        }
        var defaultStudyStatus = await masterService.getDefaultStudyStatus();
        study.status_id = defaultStudyStatus.value_id;
      
        transaction = await sequelize.transaction();
        
        var newStudy = await studyService.createStudy(study, transaction);      
        var defaultStudyVersion = versionsStudyModel.build({version_number:0, study_id: newStudy.study_id});
        var newStudyVersion = await studyService.createStudyVersion(defaultStudyVersion, transaction);
        
        var token = req.headers.authorization.split(" ")[1];
        var user = await userService.getUserByTokenAsync(token);
        if (user.usr_user.usr_rol.role_id == config.users.gerente){
            logger.debug("Assign new study to the advanced user");
            var userStudy = studyUsersModel.build({
              user_id: user.usr_user.user_id,        
              study_id: newStudy.study_id,
              created_on: new Date()            
          });
          
          await userStudy.save({transaction});
        }

        await transaction.commit();

        try {          
          // Creamos la carpeta del estudio en sharepoint
          await sharepoint.createFoldersNewStudy(newStudy.study_id);

        } catch (error) {
          logger.error("Ha fallado la creación de la carpeta");
          logger.error(JSON.stringify(error));          
          messageService.throwMessageError(message.ERROR_STUDY_CREATED_SP_FAIL);
        }        
        

        messageService.createMessage(res, message.INFO_OK);

        try {      
    } catch (err){
        logger.error("[StudyController - createStudy]: " + JSON.stringify(err));
        if (transaction){
            await transaction.rollback();
        }
        messageService.buildMessageError(res, err);
    }
}


function updateStudy(req, res) {
  var study = req.body;

  return studyService.updateStudy(study, req.params.study_id)
    .then(data => {
      if (data == 0) {
        messageService.createMessage(res, message.ERROR_UPDATE_NO_ROWS);
      } else {
        logger.debug("Se ha actualizado el estudio de mercado");
        messageService.createMessage(res, message.INFO_OK);
      }
    }).catch(err => {
      logger.error("[StudyController - updateStudy] " + JSON.stringify(err));
      messageService.createMessage(res, message.ERROR_UPDATE_NO_ROWS);
    });
}

async function deleteStudy(req, res) {
  try {
    await masterService.callStoreProcedure(masterService.storeProcedures.PROCEDURE_DELETE_STUDY, req.params.study_id);
    return messageService.createMessage(res, message.INFO_OK);  
  } catch (error) {
    logger.error("Error al borrar un estudio: "+ JSON.stringify(error));
    messageService.buildMessageError(res, error);
  }
  
  /*
  return studyService.deleteStudy(req.params.study_id)
    .then(data => {
      if (data == 0) {
        messageService.createMessage(res, message.ERROR_DELETE_NO_ROWS);
      } else {
        logger.debug("El estudio ha sido borrado");
        messageService.createMessage(res, message.INFO_OK);
      }
    }).catch(err => {
      logger.error("[StudyController - deleteStudy] " + JSON.stringify(err));
      messageService.createMessage(res, message.ERROR_DELETE_NO_ROWS);
    });
    */
}

function getVersionsStudy(req, res) {
  return studyService.getVersionsStudy(req.params.study_id).then(data => {
    logger.debug("Las versiones del estudio son las siguientes: " + JSON.stringify(data));
    res.status(200).send(data);
  });
}

async function changeStudyStatus(req, res) {

  var valid = true;

  var study_id = req.params.study_id;
  var newStatus = req.body.newStatus;
  var oldStatus = await studyService.getCurrentStatus(study_id);
  var currentStatus = oldStatus.status.value

  // Parametro erroneo
  if (newStatus == currentStatus) {
    logger.error("El estado introducido es igual al existente.");
    valid = false;
  }
  // Parametros correcto
  else {
    var newStatusId = await masterService.getStudyStatus(newStatus);
    if (config.studyStatus.abierto == currentStatus) {
      // Si parto de abierto, solo puedo ir a --> "En revisión"
      if (config.studyStatus.enRevision == newStatus) {
        updateStatus(false, study_id, newStatus, newStatusId.value_id);
      } else {
        logger.error("El nuevo estado introducido no se corresponde con una transición válida.");
        valid = false;
      }

    }
    else if (config.studyStatus.enRevision == currentStatus) {
      // Si el estado es "En revisión" puede pasar a cerrado o abierto
      if (config.studyStatus.abierto == newStatus) {
        // Devuelvo un estudio a abierto. No se ha hecho correctamente.
        updateStatus(false, study_id, newStatus, newStatusId.value_id);
      } else if (config.studyStatus.cerrado == newStatus) {
        // Cierro un estudio
        updateStatus(false, study_id, newStatus, newStatusId.value_id);
      } else {
        logger.error("El nuevo estado introducido no se corresponde con una transición válida.");
        valid = false;
      }
      // Solicito una nueva versión de un estudio cerrado
    }
    else if (config.studyStatus.cerrado == currentStatus) {
      if (config.studyStatus.abierto == newStatus || config.studyStatus.nuevaVersion == newStatus) {
        await studyService.createNewVersion(study_id);
        updateStatus(true, study_id, newStatus, newStatusId.value_id);
        //sendNotifications([config.users.basico, config.users.gerente], study_id, newStatus);
      } else {
        logger.error("El nuevo estado introducido no se corresponde con una transición válida.");
        valid = false;
      }
    }
  }
  if (valid)
    return messageService.createMessage(res, message.INFO_OK);
  else
    return messageService.createMessageWithErrorMsg(res, valid, message.ERROR_TRANSICION_ESTADO_NO_VALIDO);
}

async function checkValidVersion(req, res) {
  // Check if, at least, one of version_id and or version_number is filled
  var informed = req.query.version_id != null || req.query.version_number != null;
  messageService.throwIfNecessaryMessageError(informed, message.ERROR_VERSION_NO_INFORMADA);

  if (req.query.version_id != null && req.query.version_number != null) {
    // ensure the version is valid.
    var isValidVersion = await studyService.isValidVersion(req.query.version_id, req.query.version_number);
    messageService.throwIfNecessaryMessageError(isValidVersion, message.ERROR_VERSION_NOTEQUAL);
  }
}

async function getVersionIdForStudyId(req) {
  var version_id = null;
  if (req.query.version_id == null) {
    await studyService.getVersionStudy(req.query.study_id, req.query.version_number)
      .then(data => {
        version_id = data[0].version_id;
      })
      .catch(err => {
        logger.error("[PromotionsController - getVersionStudyByPromotionIdError]: " + err);
        messageService.throwIfNecessaryMessageError(true, message.ERROR_RECOGER_GET);
      });

  } else {
    version_id = req.query.version_id;
  }
  return version_id;

}


/**
 * 
 * @param {*} studyId 
 * @param {*} newStatus 
 * @param {*} valueId 
 */
async function updateStatus(newVersion, studyId, newStatus, valueId) {
  if (newVersion === true){
    await studyService.createNewVersion(study_id);
  }else{
    studyService.updateStudyStatus(studyId, valueId);
  }
  var usuarios = await getUsuariosEstudios(studyId);
  var estado = await masterService.getNameStudyStatus(newStatus); 
  var arrayUsuarios = [];
  var codigo = usuarios[0].stu_market_studies[0].code;
  
  for (var idx = 0; idx < usuarios[0].usr_users.length; idx++) {
    arrayUsuarios.push(usuarios[0].usr_users[idx].email)
  }
  /*
  var adminUsers = await userService.getUsersByRoleId(config.users.admin);
  for (var idx = 0; idx < adminUsers.length; idx++) {
    arrayUsuarios.push(adminUsers[idx].email)
  }*/
  arrayUsuarios.push(config.email_admin_user);
  
  sendNotifications(arrayUsuarios, codigo, estado.label.toLowerCase());
}


/**
 * 
 * @param {*} studyId 
 */
async function getUsuariosEstudios(studyId) {
  return studyService.getUsersStudy(studyId);;
}

/**
 * @param {*} users Array con los roles de los usuarios que deben recibir una notificación.
 * @param {*} study_id  Estudio que cambia de estado.
 * @param {*} newStatus  Nuevo estado del estudio.
 */
function sendNotifications(users, studyCode, estado) {
  var subject = config.email.asunto.replace('{codigo_estudio}', studyCode)
  var texto = config.email.message.replace('{codigo_estudio}', studyCode).replace('{estado}', estado) + config.email.footer;
  return sendEmail(users, subject, texto);
}

module.exports = {
  getStudiesByToken,
  getStudyById,
  exportToXLS,
  createStudy,
  updateStudy,
  deleteStudy,
  getVersionsStudy,
  changeStudyStatus,
  checkValidVersion,
  getVersionIdForStudyId
};
