const logger = require("../config/logger");
const sequelize = require("../db/db");
const messageService = require("../services/messagesManager");
const masterService = require("../services/master");
const message = require("../config/messages");
const dataValuesModel = require("../models/master").Values;
const dataTypesModel = require("../models/master").Types;

function getBasicDataByTypeValue(req, res){
    return masterService.getBasicDataByTypeValue(req.params.type_value).then(data => {
      res.status(200).send(data);
    });
};

function getConfigDataTypes(req, res){
    return masterService.getConfigDataTypes().then(data => {
        res.status(200).send(data);
    });
};

/**
 * Funcion que inserta un nuevo tipo de calidad en base de datos
 * @param {*} req 
 * @param {*} res 
 */
async function createConfigDataTypes(req, res){
    var dataTypes = dataTypesModel.build(req.body);
    try {
        checkTypeInsert(dataTypes);            
        await masterService.createConfigDataType(dataTypes);
    
        return messageService.createMessage(res, message.INFO_OK);

    } catch (err) {
        logger.error("[MasterController - createConfigDataTypes]: " + JSON.stringify(err));
        if (err.type == 'custom') {
            return messageService.createMessage(res, message.ERROR_DATOS_INCONSISTENTES);
        } else {
            return messageService.createMessage(res, message.ERROR_GENERICO_INSERT);
        }
    }
}

function checkTypeInsert(dataType){
    if (dataType.type_id != null || dataType.first_level_id == null || (dataType.data_type!= 1 && dataType.data_type !=2)){
        messageService.throwMessageError(message.ERROR_PARAMETROS_INCORRECTOS);
    }
}


async function updateConfigDataTypes(req, res){
    var dataTypes = dataTypesModel.build(req.body);
    try {
        checkTypeUpdate(dataTypes);    
        await masterService.updateConfigDataType(dataTypes);
        
        return messageService.createMessage(res, message.INFO_OK);

    } catch (err) {
        logger.error("[MasterController - updateConfigDataTypes]: " + JSON.stringify(err));
        if (err.type == 'custom') {
            return messageService.createMessage(res, message.ERROR_DATOS_INCONSISTENTES);
        } else {
            return messageService.createMessage(res, message.ERROR_GENERICO_UPDATE);
        }
    }
}


function checkTypeUpdate(dataType){
    if (dataType.type_id == null){
        messageService.throwMessageError(message.ERROR_PARAMETROS_INCORRECTOS);
    }
}


function getConfigFirstLevels(req,res){
    return masterService.getFirstLevels().then(data=>{
        res.status(200).send(data);
    });
}

function getConfigDataValuesByTypeId(req, res){
    return masterService.getConfigDataValuesByTypeId(req.params.type_id).then(data => {
        logger.debug("Los valores para el tipo de dato por Id son: " + JSON.stringify(data));
        res.status(200).send(data);
    });
};

function updateConfigDataValues(req,res){
    var dataValue = dataValuesModel.build(req.body);
    dataValue.value_id = req.params.value_id;

    return masterService.updateConfigDataValues(dataValue)
    .then(data => {
        logger.debug("Se ha actualizado el valor en la tabla values");
        messageService.createMessage(res, message.INFO_OK);
    }).catch(err => { 
        logger.error("[MasterController - updateConfigDataValues] " + JSON.stringify(err));
        return messageService.createMessage(res, message.ERROR_GENERICO_INSERT);
    });
}


function createConfigDataValues(req, res){
    var dataValue = dataValuesModel.build(req.body);

    return masterService.createConfigDataValues(dataValue)
    .then(data => {
        logger.debug("Se ha insertado este valor en la tabla values: " + JSON.stringify(data));
        messageService.createMessage(res, message.INFO_OK);
    }).catch(err => { // TODO: El error se controla, falta cambiar el mensaje (más adelante)
        logger.error("[MasterController - CreateConfigDataValues] " + JSON.stringify(err));
        return messageService.createMessage(res, message.ERROR_GENERICO_INSERT);
    });
};

module.exports = {
    getBasicDataByTypeValue,
    getConfigDataTypes,
    createConfigDataTypes,
    updateConfigDataTypes,
    getConfigFirstLevels,
    getConfigDataValuesByTypeId,
    updateConfigDataValues,
    createConfigDataValues
};