const logger = require("../config/logger");
const config = require("../config/config");
const messageService = require("./../services/messagesManager");
const message = require("./../config/messages");
const studyService = require("./../services/study");
const request = require('request-promise');
const azure = require("../services/authAzure");
const fs = require('fs-extra');
const util = require('util');
const authService = require("../services/auth");
const Configbbdd = require("../global/config");

const PATH_TO_FOLDER = '{pathToFolder}';
const PATH_PARENT_ITEM_ID = '{parent-item-id}';
const PATH_FILENAME = '{filename}';

const DOC_TYPE_PROMOTION_DOCS = 1;
const DOC_TYPE_NEXT_PROMOS_DOCS = 2;

async function getDocument(req, res) {
    logger.debug("En el getDocument " + req.body.fileName);
    var token = await azure.getAzureToken();
    var headers = {
        'User-Agent': 'Super Agent/0.0.1',
        "Content-type": "applcation/pdf",
        'Authorization': 'Bearer ' + token,
    }

    // Configure the request
    var options = {
        url: req.body.fileName,
        method: 'GET',
        encoding: "binary",
        headers: headers
    };
    var file = await request(options);

    logger.debug("FILE: " + file);
    logger.debug("FILE: " + JSON.stringify(file));

    try {
        var study_id = req.params.study_id;
        var promotion_id = req.params.promotion_id;
        var item_id = req.params.item_id;
        messageService.checkParamInformed(study_id);
        messageService.checkParamInformed(promotion_id);
        messageService.checkParamInformed(item_id);

        var token = await azure.getAzureToken();
        var url = config.sharepoint.routes.downloadItem.replace("{item-id}", itemId);

        var azureDocument = await azure.executeRequest('GET', url, token);
        var urlToReturn = azureDocument[config.sharepoint.routes.downloadItemAttribute];

        res.status(200).send(urlToReturn);
    } catch (error) {
        messageService.buildMessageError(res, error);
    }

}


async function getNextPromotionsDocuments(req, res) {
    getDocumentsOnFolder(req, res, DOC_TYPE_NEXT_PROMOS_DOCS);
}


async function getPromotionDocuments(req, res) {
    getDocumentsOnFolder(req, res, DOC_TYPE_PROMOTION_DOCS);
}

async function getDocumentsOnFolder(req, res, docType) {
    var documents = null;
    var study_id = null;
    var promotion_id = null;
    var subFolder = null;
    try {
        var study_id = req.params.study_id;
        messageService.checkParamInformed(study_id);

        if (docType === DOC_TYPE_NEXT_PROMOS_DOCS) {
            // Buscamos carpetas de tipo nextPromotion     
            subFolder = config.sharepoint.routes.baseFolderNextPromotions;
        } else {
            promotion_id = req.params.promotion_id;
            messageService.checkParamInformed(promotion_id);
            subFolder = promotion_id;
        }


        var emFolder = await getStudyCode(study_id);
        emFolder = emFolder + "/" + subFolder;
        logger.debug("EM Folder: " + emFolder);
        //TODO: Quitar: es solo para dev
        //emFolder = '/marcos';
        var token = await azure.getAzureToken();

        var url = config.sharepoint.routes.listItems.replace(PATH_TO_FOLDER, config.sharepoint.routes.baseFolder + "/" + emFolder);
        var azureDocuments = await azure.executeRequest('GET', url, token);
        documents = getItemsFromAzure(azureDocuments);
        res.status(200).send(documents);

    } catch (error) {
        logger.error("ERROR: " + error);
        messageService.buildMessageError(res, error);
    }
}


function getItemsFromAzure(azureDocuments) {
    var documents = [];
    JSON.parse(azureDocuments).value.filter(item => {
        var doc = {
            name: item.name,
            type: item.file.mimeType,
            size: item.size,
            creationDate: item.createdDateTime,
            //user: item.createdBy.user.displayName,
            id: item.id,
            downloadUrl: item['@microsoft.graph.downloadUrl']
        };
        documents.push(doc);
    });
    return documents;

}


async function createFoldersNewPromotion(study_id, promotion_id) {
    logger.debug("Creo una carpeta para la promocion: " + promotion_id);
    // Tengo que buscar la carpeta del proyecto.
    var token = await azure.getAzureToken();
    var emCode = await getStudyCode(study_id);
    var path = config.sharepoint.routes.folderInfo.replace(PATH_TO_FOLDER, config.sharepoint.routes.baseFolder + '/' + emCode);
    var parentId = await getFolderId(path, token);
    logger.debug("Parent: " + parentId);

    // Creamos la carpeta de una nueva promoción para el Estudio de mercado
    var newFolder = await createNewFolder(promotion_id, parentId, token);
    logger.debug("Nueva carpeta creada ");

    return newFolder;

}

async function createFoldersNewStudy(study_id) {
    var path = config.sharepoint.routes.folderInfo.replace(PATH_TO_FOLDER, config.sharepoint.routes.baseFolder);
    var token = await azure.getAzureToken();
    var parentId = await getFolderId(path, token);
    var emCode = await getStudyCode(study_id);
    logger.debug("Parent: " + parentId);

    // Creamos la carpeta del Estudio de mercado
    var emFolder = await createNewFolder(emCode, parentId, token);
    logger.debug("Nueva carpeta creada " + emFolder);

    // Preparamos para crear la carpeta de suelo
    path = config.sharepoint.routes.folderInfo.replace(PATH_TO_FOLDER, config.sharepoint.routes.baseFolder + "/" + config.sharepoint.routes.baseFolderNextPromotions);
    await createNewFolder(config.sharepoint.routes.baseFolderNextPromotions, JSON.parse(emFolder).id, token);
    return emFolder;

}

async function createNewFolder(newFolderName, parentId, tkn) {
    var token = tkn;
    const driveItem = {
        name: newFolderName + '',
        folder: {}
        //,'@microsoft.graph.conflictBehavior': "rename"
    };

    if (token == null || token != undefined) {
        token = await azure.getAzureToken();
    }
    var url = config.sharepoint.routes.createFolder.replace(PATH_PARENT_ITEM_ID, parentId);
    var newDocument = await azure.executeRequest('POST', url, token, JSON.stringify(driveItem));
    logger.debug("Nueva carpeta " + JSON.parse(newDocument).id + " creada");
    return newDocument;
}

async function getFolderId(folderUrl, token) {
    logger.debug("FOLDER: " + folderUrl);
    var folder = await azure.executeRequest('GET', folderUrl, token, null);
    return JSON.parse(folder).id;
}

async function getStudyCode(study_id) {
    var study = await studyService.getStudy(study_id);
    return study.code;
}

async function uploadDocument(req, res, docType) {
    var subFolder = null;
    var targetURL = null;

    try {
        var study_id = req.params.study_id;
        messageService.checkParamInformed(study_id);

        // En función del tipo, monto la ruta de destino        
        if (docType === DOC_TYPE_NEXT_PROMOS_DOCS) {
            // Buscamos carpetas de tipo nextPromotion     
            subFolder = config.sharepoint.routes.baseFolderNextPromotions;
        } else {
            promotion_id = req.params.promotion_id;
            messageService.checkParamInformed(promotion_id);
            subFolder = promotion_id;
        }
        var file = config.sharepoint.temporalFilesDir + '/' + req.file.filename;

        if (req.file && req.file != undefined) {
            var token = await azure.getAzureToken();
            var study_code = await getStudyCode(study_id);
            subFolder = study_code + '/' + subFolder;
            targetURL = config.sharepoint.routes.uploadItem
                .replace(PATH_TO_FOLDER, config.sharepoint.routes.baseFolder + '/' + subFolder)
                .replace(PATH_FILENAME, req.file.filename);

            const readFile = util.promisify(fs.readFile);
            var data = await readFile(file);
            var response = await azure.executeRequest('PUT', targetURL, token, data);
            deleteFile(req.file.filename);

            messageService.createMessageWithErrorMsg(res, JSON.parse(response).id != null, message.ERROR_FILE_UPLOAD_ERROR);

        } else {
            res.status("409").json("No hay fichero para subir");
        }
    } catch (error) {
        messageService.buildMessageError(res, error);
    }
}


function uploadNextPromotionDocument(req, res) {
    uploadDocument(req, res, DOC_TYPE_NEXT_PROMOS_DOCS);
}

function uploadPromotionDocument(req, res) {
    uploadDocument(req, res, DOC_TYPE_PROMOTION_DOCS);
}

function deleteFile(filename) {
    fs.remove(config.sharepoint.temporalFilesDir + '/' + filename)
        .then(() => {
            logger.debug('Fichero borrado correctamente')
        })
        .catch(err => {
            logger.error("Ha ocurrido un error al borrar el fichero" + JSON.stringify(err));
        })
}


function uploadDocumentFile(req, res) {
    logger.debug("Entrando en UploadDocument");

    try{
        const file = req.file;
        logger.debug('El fichero es: '+file);
        if (!file) {
            res.status(400).json({
                "status": "failed",
                "code" : "400",
                "message" : "Please upload file"
            });
        }

        res.status(200).json({
            "status": "success",
            "code" : "200",
            "message" : "file uploaded successfully"
        });
    }catch(err){
        logger.error("Ha ocurrido un error al subir el fichero" + JSON.stringify(err));
        res.status(200).json({
            "status": "failed",
            "code" : "500",
            "message" : error.message
        });
    }
}

function removeDocumentFile(req, res) {

    logger.debug("Eliminando Productos");
    
    
     var out = req.body.urlRemove.replace(/\s/g, "");
     logger.debug(config.sharepoint.urlLocalDelete+"/"+out);

        fs.remove(config.sharepoint.urlLocalDelete+"/"+out)
        .then(() => {
            res.status(200).json({
                "status": "success",
                "code" : "200",
                "message" : "file delete successfully"
            });
            logger.debug('Fichero borrado correctamente')
        })
        .catch(err => {
            logger.error("Ha ocurrido un error al borrar el fichero" + JSON.stringify(err));
        })


}


module.exports = {
    getDocument,
    getPromotionDocuments,
    getNextPromotionsDocuments,
    uploadNextPromotionDocument,
    uploadPromotionDocument,
    createFoldersNewStudy,
    createFoldersNewPromotion,
    uploadDocumentFile,
    removeDocumentFile
};