const sequelize = require("../db/db");
const logger = require("../config/logger");
const messageService = require("../services/messagesManager");
const message = require("../config/messages");
const CustomError = require("../customError");
const qualitiesService = require("../services/qualities");
const masterService = require("../services/master");
const qualitiesModel = require("../models/promotions").Qualities;

function getQualities(req, res) {
    qualitiesService.getQualities(req.params.promotion_id)
        .then(data => {
            logger.debug("Las Calidades de la promocion son las siguientes: " + JSON.stringify(data));
            res.status(200).send(data);
        })
}


async function insertOrUpdateQualities(req, res) {
    var qualities = qualitiesModel.build(req.body);
    var transaction = null;
    try {

        await checkQualitiesStatus(qualities, req.params.promotion_id);

        transaction = await sequelize.transaction();
        for (var i = 0; i < qualities.length; i++) {
            await qualitiesService.insertOrUpdateQualities(qualities[i], transaction);
        }
        await transaction.commit();

        return messageService.createMessage(res, message.INFO_OK);

    } catch (err) {
        logger.error("[QualitiesController - insertOrUpdateQualitiesError]: " + JSON.stringify(err));
        if (transaction) {
            await transaction.rollback();
        }
        if (err.type == 'custom') {
            return messageService.createMessage(res, message.ERROR_DATOS_INCONSISTENTES);
        } else {
            return messageService.createMessage(res, message.ERROR_GENERICO_UPDATE);
        }
    }
}


async function checkQualitiesStatus(qualities, promotion_id) {
    var dataType;

    for (var i = 0; i < qualities.length; i++) {
        qualities[i].promotion_id = promotion_id;
        if (qualities[i].cnt_quality_value_id != undefined) {
            var typeId = await masterService.getTypeByValue(qualities[i].cnt_quality_value_id);
            if (qualities[i].cnt_quality_type_id != typeId.type_id) {
                throw new CustomError(message.ERROR_DATOS_INCONSISTENTES);
            }
        }
        dataType = await masterService.getDataType(qualities[i].cnt_quality_type_id);
        if ((dataType.data_type == 1 && (qualities[i].cnt_quality_value_id == undefined)) ||
            (dataType.data_type == 2 && (qualities[i].value == undefined))) {
            throw new CustomError(message.ERROR_DATOS_INCONSISTENTES);
        } else if (qualities[i].cnt_quality_value_id != undefined && qualities[i].value != undefined) {
            throw new CustomError(message.ERROR_DATOS_INCONSISTENTES);
        }
    };
}

module.exports = {
    getQualities,
    insertOrUpdateQualities
}