const logger = require("../config/logger");
const financesService = require("../services/finances");
const financeModel = require("../models/promotions").Finances;
const messageService = require("../services/messagesManager");
const message = require("../config/messages");

function getFinance(req, res){
    return financesService.getFinance(req.params.promotion_id)
    .then(data => {
        logger.debug("Las finanzas de la promocion son las siguientes: " + JSON.stringify(data));
        res.status(200).send(data);
    })
}

function createFinance(req, res){
    var finance = financeModel.build(req.body);
    finance.promotion_id = req.params.promotion_id;

    return financesService.createFinance(finance)
    .then(data => {
        logger.debug("Se ha insertado la siguiente financiación: " + JSON.stringify(data));
        messageService.createMessage(res, message.INFO_OK);
    })
    .catch(err => {
        logger.error("[FinanceController - CreateFinance] " + JSON.stringify(err));
        messageService.createMessage(res, message.ERROR_GENERICO_INSERT);
    });
}

function updateFinance(req, res){
    var finance = req.body;

    return financesService.updateFinance(finance, req.params.promotion_id)
    .then(data => {
        if (data == 0){
            messageService.createMessage(res, message.ERROR_UPDATE_NO_ROWS);
        } else {
          logger.debug("Se ha actualizado la financiacion de la promocion");
          messageService.createMessage(res, message.INFO_OK);
        }
    }).catch(err => {
        logger.error("[FinanceController - UpdateFinance] " + JSON.stringify(err));
        return messageService.createMessage(res, message.ERROR_GENERICO_UPDATE);
  });
}

module.exports = {
    getFinance,
    createFinance,
    updateFinance
}