const nextPromotionsService = require("../services/nextPromotions");
const logger = require("../config/logger");
const messageService = require("../services/messagesManager");
const message = require("../config/messages");

const NextPromotion = require("../models/studies").NextPromotions;


function getNextPromotion(req, res){
    return nextPromotionsService.getNextPromotions(req.params.next_promotion_id)
    .then(data => {
        logger.debug("La promocion futura es: " + JSON.stringify(data));
        res.status(200).send(data);
    });
}

function getNextPromotions(req, res){
    var study_id = req.query.study_id;
    if (study_id == null || study_id == undefined){
        messageService.createMessage(res, message.ERROR_PARAMETROS_INCORRECTOS);
    }else{        
        return nextPromotionsService.getNextPromotions(study_id)
        .then(data => {
            logger.debug("La promocion futura es: " + JSON.stringify(data));
            res.status(200).send(data);
        });
    }
}

function createNextPromotion(req, res){
    var nextPromotion = NextPromotion.build(req.body);

    return nextPromotionsService.createNextPromotion(nextPromotion)
    .then(data => {
        logger.debug("Se ha insertado la siguiente promocion futura: " + JSON.stringify(data));
        messageService.createMessage(res, message.INFO_OK);
    })
    .catch(err => {
        logger.error("[NextPromotionsController - createNextPromotion] " + JSON.stringify(err));
        messageService.createMessage(res, message.ERROR_GENERICO_INSERT);
    });
}
function deleteNextPromotion(req, res){
    return nextPromotionsService.deleteNextPromotion(req.params.next_promotion_id)
    .then(data => {
        if (data == 0){
          messageService.createMessage(res, message.ERROR_DELETE_NO_ROWS)
        }else {
          logger.debug("La promocion futura ha sido borrada");
          messageService.createMessage(res, message.INFO_OK);
        }
    }).catch(err => {
        logger.error("[NextPromotionsController - deleteNextPromotion] " + JSON.stringify(err));
        messageService.createMessage(res, message.ERROR_DELETE_NO_ROWS);
    });
}

module.exports = {
    getNextPromotion,
    getNextPromotions,
    createNextPromotion,
    deleteNextPromotion
}