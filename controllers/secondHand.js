const secondHandService = require("../services/secondHand");
const studyService = require("../services/study");
const logger = require("../config/logger");
const messageService = require("../services/messagesManager");
const message = require("../config/messages");

const SecondHand = require("../models/studies").SecondHand;

function getAllSecondHandRent(req, res){
    var study_id = req.query.study_id;
    if (study_id == null || study_id == undefined){
        messageService.createMessage(res, message.ERROR_PARAMETROS_INCORRECTOS);  
    }else{
        // Si todo va bien, hago la llamada
        return studyService.getSecondHand(study_id)
        .then(data => {
            logger.debug("El inmueble de segunda mano es: " + JSON.stringify(data));
            res.status(200).send(data);
        });        
    }
}

function getSecondHandRent(req, res){
    return secondHandService.getSecondHandRent(req.params.shr_id)
    .then(data => {
        logger.debug("El inmueble de segunda mano es: " + JSON.stringify(data));
        res.status(200).send(data);
    });
}

function createSecondHandRent(req,res){
    var secondHand = SecondHand.build(req.body);

    return secondHandService.createSecondHandRent(secondHand)
    .then(data => {

        logger.debug("Se ha insertado el siguiente inmueble de segunda mano: " + JSON.stringify(data));
        messageService.createMessage(res, message.INFO_OK);
    })
    .catch(err => {
        logger.error("[SecondHandController - createSecondHandRent] " + JSON.stringify(err));
        messageService.createMessage(res, message.ERROR_GENERICO_INSERT);
    });
}

async function updateSecondHandRent(req, res){
    var secondHand = req.body;

    if (req.body.second_hand_status_id!= null && req.body.second_hand_status_id != undefined){
        // Ha cambiado el second_hand_status_id... Lo añado al registro.        
        var shr = await secondHandService.getSecondHandRent(req.params.shr_id);
        secondHand.registry = (shr.registry|| '') + 'Updated on '+ new Date() + ' to the status: '+ req.body.second_hand_status_id +'\n';
    }
    return secondHandService.updateSecondHandRent(secondHand, req.params.shr_id)
    .then(data => {
        if (data == 0){
            messageService.createMessage(res, message.ERROR_UPDATE_NO_ROWS)
          }else{
            logger.debug("Se ha actualizado el inmueble de segunda mano");
            messageService.createMessage(res, message.INFO_OK);
          }
        }).catch(err => {
            logger.error("[SecondHandController - updateSecondHandRent] " + JSON.stringify(err));
            messageService.createMessage(res, message.ERROR_UPDATE_NO_ROWS);
    });
}

function deleteSecondHandRent(req, res){
    return secondHandService.deleteSecondHandRent(req.params.shr_id)
    .then(data => {
        if (data == 0){
          messageService.createMessage(res, message.ERROR_DELETE_NO_ROWS)
        }else{
          logger.debug("El inmueble de segunda mano ha sido borrado");
          messageService.createMessage(res, message.INFO_OK);
        }
      }).catch(err => {
        logger.error("[SecondHandController - deleteSecondHandRent] " + JSON.stringify(err));
        messageService.createMessage(res, message.ERROR_DELETE_NO_ROWS);
    });
}

module.exports = {
    getAllSecondHandRent,
    getSecondHandRent,
    createSecondHandRent,
    updateSecondHandRent,
    deleteSecondHandRent
}