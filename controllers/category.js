const config = require("../config/config");
const jwt = require("jsonwebtoken");
const categoryService = require("../services/category");
const authService = require("../services/auth");
const userService = require("../services/user");
const logger = require("../config/logger");
const message = require("../config/messages");
const messageService = require("../services/messagesManager");

const sendEmail = require('../utils/mail').sendNotifications;
const azure = require("../services/authAzure");

const ERRORS = {
    /** User cannot see the resource */
    ERROR_CODE_NOT_AUTHENTICATED: {
        CODE: 401,
        MSG: "Sus credenciales no son válidas"
    },
    /** The user is no logged in the application */
    ERROR_CODE_NOT_AUTHORIZED: {
        CODE: 403,     
        MSG: "No tiene permisos para acceder al recurso"
    }
}

function getCategory(req, res) {
    
    return categoryService
    .nameCategory(req.body)
        .then(token => {
            logger.debug(token);
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error en la obtención de la categoría');
        });
}




function getMethodRequested(method) {
    var reqMethod = "";
    if (method.get == true) {
        reqMethod = 'get';
    } else if (method.post) {
        reqMethod = 'get';
    } else if (method.put) {
        reqMethod = 'put';
    } else if (method.delete) {
        reqMethod = 'delete';
    }
    return reqMethod;
}

function sendResponseNotAllowed(res, error = ERRORS.ERROR_CODE_NOT_AUTHORIZED) {
    logger.warn("No se puede acceder al recurso"+ JSON.stringify(error));
    return res.status(error.CODE).send({ auth: false, message: error.MSG });
}


module.exports = {
   getCategory
};