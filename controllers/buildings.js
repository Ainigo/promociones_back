const sequelize = require("../db/db");
const logger = require("../config/logger");
const config = require("../config/config");
const messageService = require("../services/messagesManager");
const message = require("../config/messages");
const buildingsService = require("../services/buildings");
const studyController = require("../controllers/study");
const studyService = require("../services/study");
const promotionsService = require("../services/promotions");
const masterService = require("../services/master");
const buildingModel = require("../models/promotions").Buildings;
const uniBuildingModel = require("../models/promotions").BuildingSingle;
const pluriBuildingModel = require("../models/promotions").BuildingMulti;


async function getBuildings(req, res){
    try {
        var versionId = await getVersionId(req, res);
        //return buildingsService.getBuildings(req.params.promotion_id, versionId)
        return promotionsService.getBuildings(req.params.promotion_id, versionId)
        .then(data => {
            logger.debug("Los Inmuebles de la promocion son los siguientes: " + JSON.stringify(data));
            var result = {promotion_id: req.params.promotion_id, version: versionId, buildings: data};
            res.send(result);
        });    
    } catch (error) {
        messageService.buildMessageError(res, error);
    }
    
};

async function getBuilding(req, res){
    var versionId = null;
    try {    
        versionId = await getVersionId(req, res);
        
        return buildingsService.getBuilding(req.params.promotion_id, req.params.building_id, versionId)
        .then(data => {
            logger.debug("El inmueble seleccionado es el siguiente: " + JSON.stringify(data));
            var result = {promotion_id: req.params.promotion_id, version: versionId, buildings: data};
            res.send(result);
        });
        
    } catch (error) {
        return messageService.buildMessageError(res, error);
    }
    
};

async function createBuilding(req, res){
    var building = buildingModel.build(req.body);
    var transaction = null;
    var msg = null;
    try {
        var version = await studyService.getVersionStudyByPromotionId(req.params.promotion_id);
        
        var defaultVersion = version[0].version_id;
        
        var buildingType = await masterService.getCntValue(building.building_type_id);
        building.promotion_id = req.params.promotion_id;
        building.version_id = defaultVersion;
            
        transaction = await sequelize.transaction();
        await buildingsService.createBuilding(building, transaction);

        if (buildingType.value == config.pluriBuilding){
            var multiBuilding = pluriBuildingModel.build(req.body);
            multiBuilding.version_id = building.version_id;
            multiBuilding.building_id = building.building_id;

            await buildingsService.createPluriBuilding(multiBuilding, transaction);
            
            await transaction.commit();
            msg = messageService.createMessage(res, message.INFO_OK);

        } else {
            var uniBuilding = uniBuildingModel.build(req.body);
            uniBuilding.version_id = building.version_id;
            uniBuilding.building_id = building.building_id;

            await buildingsService.createUniBuilding(uniBuilding, transaction);

            await transaction.commit();
            msg = messageService.createMessage(res, message.INFO_OK);
        }
    } catch (err){
        logger.error("[BuildingController - CreateBuilding] " + JSON.stringify(err));
        if (transaction){
            await transaction.rollback();
        }
        if (err.type == 'custom'){
            msg = messageService.createMessage(res, message.ERROR_DATOS_INCONSISTENTES);
        }
        else {
            msg = messageService.createMessage(res, message.ERROR_GENERICO_INSERT);
        }
    }
    return msg;
};

async function updateBuilding(req, res){
    var building = req.body;
    var transaction = null;
    var msg = null;

    try {
        var versionStudy = await studyService.getVersionStudyByPromotionId(req.params.promotion_id);
        
        var version = versionStudy[0].version_id;

        var buildingTypeId = await buildingsService.getBuildingType(req.params.building_id,version);
        var buildingTypeValue = await masterService.getCntValue(buildingTypeId.building_type_id);

        transaction = await sequelize.transaction();

        if (building.building_type_id == undefined){
            await updateSingleOrMulti(req, buildingTypeValue.value, version, transaction);
        } else if (buildingTypeId.building_type_id == building.building_type_id){
            await updateSingleOrMulti(req, buildingTypeValue.value, version, transaction);
        } else {
            buildingTypeValue = await masterService.getCntValue(building.building_type_id);
            if (buildingTypeValue.value == config.pluriBuilding){
                
                var buildingMulti = pluriBuildingModel.build(req.body);
                buildingMulti.building_id = req.params.building_id;
                buildingMulti.version_id = version;
                await buildingsService.createPluriBuilding(buildingMulti, transaction)
                await buildingsService.deleteBuildingSingle(req.params.building_id, version, transaction)
            } else {
                
                var buildingUni = uniBuildingModel.build(req.body);
                buildingUni.building_id = req.params.building_id;
                buildingUni.version_id = version;
                await buildingsService.createUniBuilding(buildingUni, transaction)
                await buildingsService.deleteBuildingMulti(req.params.building_id, version, transaction)
            }
        }
        await buildingsService.updateBuilding(building, req.params.building_id, version, transaction)
        
        await transaction.commit();
        msg = messageService.createMessage(res, message.INFO_OK);

    } catch (err){
        logger.error("[BuildingController - UpdateBuilding] " + JSON.stringify(err));
        if (transaction){
            transaction.rollback();
        }
        if (err.type == 'custom'){
            msg = messageService.createMessage(res, message.ERROR_DATOS_INCONSISTENTES);
        }
        else {
            msg = messageService.createMessage(res, message.ERROR_GENERICO_UPDATE);
        }
    }
    return msg;
}

async function updateSingleOrMulti(req, buildingTypeValue, version, transaction){
    if (buildingTypeValue == config.pluriBuilding){
        var buildingPluri = req.body;

        return await buildingsService.updatePluriBuilding(buildingPluri, req.params.building_id, version, transaction)
    } else {
        var buildingUni = req.body;

        return await buildingsService.updateUniBuilding(buildingUni, req.params.building_id, version, transaction)
    }
};

async function deleteBuilding(req, res){    
	await masterService.callStoreProcedure(masterService.storeProcedures.PROCEDURE_DELETE_BUILDING, req.params.building_id);
    return messageService.createMessage(res, message.INFO_OK);    
}

async function getVersionId(req, res){
    var version_id;

    await studyController.checkValidVersion(req,res);
    
    if (req.query.version_id == null) {
        await studyService.getVersionStudyByPromotionId(req.params.promotion_id)
        .then(data => {
            version_id = data[0].version_id;
        })
        .catch(err => {
            logger.error("getVersionStudyByPromotionId Error: " + err);
            return messageService.createMessage(res, message.ERROR_RECOGER_GET);
        });    
    } else {
        version_id = req.query.version_id;
    }
    return version_id;
}

module.exports = {
    getBuildings,
    getBuilding,
    createBuilding,
    updateBuilding,
    deleteBuilding
}