const config = require("../config/config");
const authService = require("../services/auth");
const userService = require("../services/user");
const logger = require("../config/logger");
const message = require("../config/messages");
const messageService = require("../services/messagesManager");
const messages = require("../config/messages");

const CustomError = require("../customError");
const sendEmail = require('../utils/mail').sendNotifications;
const azure = require("../services/authAzure");
const request = require('request');
const fs = require('fs')
const AWS = require('aws-sdk');
const Configbbdd = require("../global/config.js");
//const path = require('path');
var os = require("os");
const mysql = require('mysql');
const { promisify } = require('util')
var os = require("os");
var hostname = os.hostname();


var AuthenticationContext = require('adal-node').AuthenticationContext;

var cookieParser = require('cookie-parser');
var crypto = require('crypto');
const jsforce = require('jsforce');

const ERRORS = {
    /** User cannot see the resource */
    ERROR_CODE_NOT_AUTHENTICATED: {
        CODE: 401,
        MSG: "Sus credenciales no son válidas"
    },
    /** The user is no logged in the application */
    ERROR_CODE_NOT_AUTHORIZED: {
        CODE: 403,     
        MSG: "No tiene permisos para acceder al recurso"
    }
}


async function register(req, res) {
    var email = req.body.email;
    var exist = false;
    try {
        try {
            exist = await azure.exitsUserOnAzure(email);
            messageService.throwIfNecessaryMessageError(exist, message.ERROR_MAIL_NO_AEDAS);

        } catch (error) {
            messageService.throwMessageError(message.ERROR_MAIL_NO_AEDAS);
        }
        return userService.getUserByLogin(email || "").then(exists => {
            if (exists) {
                return messageService.createMessage(res, message.ERROR_MAIL_DUPLICADO);
            }
            var user = {
                email: email,
                valid: true,
                name: req.body.name,
                role_id: req.body.role_id,
                last_access: new Date()
            };
            
            var username = user.name.args[0] || 'usuario';
            if (username){
                username.replace("\"","");
            }

            return userService.addUser(user).then(() => {                
                sendEmailNewUser(user.email, username);
                messageService.createMessage(res, message.INFO_OK);
            });
        });

    } catch (error) {
        console.log("ERRR: " + error);
        messageService.buildMessageError(res, error);
    }
}


function registrarEjecuciones(ip, originalURL, user,  obs) {
    let diccionario = {};
    diccionario = {
        Ipremota: ip,
        pagina: originalURL,
        email: user,
        observaciones: obs,
    }
  
    //console.log(diccionario);
    return insertarEjecuciones(diccionario);
    /*return insertarEjecuciones(diccionario)
    .then(data => {
        console.log(data);
    });*/
  }
  
  ///Regsitra las peticiones REST realizadas en la BBDD log_peticionesREST
  ///Nombre antiguo insertBBDD
  async function insertarEjecuciones(params) {
    let result;
  
    try {
        //console.log(JSON.stringify(params));
        console.log("Inicio de loggingPeticionRest: " + params.pagina);
        const pool = mysql.createPool(Configbbdd.bbdd);
        const promiseQuery = promisify(pool.query).bind(pool);
        const promisePoolEnd = promisify(pool.end).bind(pool);
        const query = "INSERT INTO gothamaedashomescom_db.log_peticionesGOTHAM (Pagina, Email, Pagina_Anterior, Ipremota, Obs) VALUES ('"+params.pagina+"','"+params.email+"','','"+params.observaciones+"','' )";
        //console.log("La query es: " + query);
        result = await promiseQuery(query)
        
        pool.end();
        promisePoolEnd();
  
        return result;
    }
    catch (error) {
        result = error;
        console.log("Error al insertar las trazas: " + error);
  
        return result;
    }
  }


const checkAuth = (req, res, next) => {
    // Busco la URL y el método para buscar los permisos que tiene el usuario.
    var method = JSON.parse(JSON.stringify(req.route['methods']));
    var originalURL = req.route["path"];
    var methodReq = getMethodRequested(method);
    originalURL = originalURL.substring(0, originalURL.indexOf(':') - 1).replace("/api/v1", '');

    if (req.headers.authorization == null  || req.headers.authorization.split(" ").length < 1) {
        return res.status(ERRORS.ERROR_CODE_NOT_AUTHENTICATED.CODE).send({ auth: false, message: 'No token provided.' });
    }
    var token = req.headers.authorization.split(" ")[1];
    if (!token) {
        return res.status(ERRORS.ERROR_CODE_NOT_AUTHENTICATED.CODE).send({ auth: false, message: 'No token provided.' });
    }
   remoteIP=""
   const protocol = req.protocol;
    const host = req.hostname;
    const url = req.originalUrl;
    user=req.params.emailUsuario
    obs= protocol+ " -- " + host + " --" + url
   
    registrarEjecuciones(remoteIP, originalURL, user,  obs);

    /*
        * Hay que comprobar que el token de usuario sigue siendo válido. Ningún admin le ha revocado permisos.
        */
    userService.getUserByToken(token).then(user => {
        if (user == null || user == undefined){                            
            userService.removeExpiredTokens();
            return sendResponseNotAllowed(res, ERRORS.ERROR_CODE_NOT_AUTHENTICATED);
        }else if (user != null && user.usr_user.valid === true) {
            var userRoleId = user.usr_user.usr_rol.role_id;

            if (userRoleId == config.users.admin) {
                // Si el usuario es administrador, puede ver y hacer todas las acciones                        
                next();
            } else {
                // El usuario es válido. Ahora comprobamos que tenga permiso de acceso al recurso
                userService.checkAuth(originalURL, methodReq, token).then(exist => {
                    if (exist != null && exist.length > 0) {
                        logger.debug("El usuario tiene permisos para ejecutar la accion solicitada");
                        // Ahora falta comprobar que el usuario tiene permiso de acceso al recurso solicitado.
                        userService.checkAuthContent(token, req.params, userRoleId).then(valid => {
                            if (valid != null && valid.length > 0 && valid[1].rowCount > 0) {
                                logger.debug("El usuario puede acceder al dato solicitado");
                                next();
                            } else {
                                return sendResponseNotAllowed(res);
                            }
                        });
                    } else {
                        return sendResponseNotAllowed(res);
                    }
                });
            }
        } else {
            logger.error("El usuario "+ JSON.stringify(user) + " no tiene permisos de acceso con el token: "+ token );
            return sendResponseNotAllowed(res);
        }
    });
}


/**
 * AUHT User 
 */
function createAuthorizationUrl(state){
    var authorizationUrl = templateAuthzUrl.replace('<client_id>', config.auth.clientId);
    authorizationUrl = authorizationUrl.replace('<redirect_uri>',config.auth.redirectUri);
    authorizationUrl = authorizationUrl.replace('<state>', state);
    authorizationUrl = authorizationUrl.replace('<resource>', config.auth.login_resource);
    console.log("authorizationUrl: "+authorizationUrl);
    return authorizationUrl;
  };
  
var authorityUrl = config.auth.authorityHostUrl + '/' + config.auth.tenant;
var templateAuthzUrl = config.auth.authorityHostUrl + 
            config.auth.tenant + 
            '/oauth2/authorize?response_type=code&client_id=<client_id>&redirect_uri=<redirect_uri>&state=<state>&resource=<resource>';



function authUser(req, res){
    crypto.randomBytes(48, function(ex, buf) {
        var token = buf.toString('base64').replace(/\//g,'_').replace(/\+/g,'-');    
        res.cookie('authstate', token);
        var authorizationUrl = createAuthorizationUrl(token);        
        res.send(authorizationUrl); 
    });
}

function getTokenAzure(req, res){    
    if (req.cookies != null && req.cookies.authstate !== req.query.state) {
      res.send('error: state does not match');
    }
    logger.debug("Todo correcto");
    var authenticationContext = new AuthenticationContext(authorityUrl);
    logger.debug("1 " +config.auth.redirectUri);
    logger.debug("2 " +config.auth.login_resource);
    logger.debug("3 " +config.auth.clientId);
    logger.debug("4 " +config.auth.secret);
    logger.debug("5 " +req.query.code);


    authenticationContext.acquireTokenWithAuthorizationCode(req.query.code, 
            config.auth.redirectUri, 
            config.auth.login_resource, 
            config.auth.clientId, 
            config.auth.secret, 
            function(err, response) {
      if (err) {
        logger.debug("Error en el login");
        console.log('req.query.code: ' + req.query.code);
        console.log('error: ' + err.message);
        res.send('error: ' + err.message + '\n');
      }else{
          // Comprobamos que el usuario es válido en EM
          logger.debug("Loggin Correctamente");
          authService.authenticate(response).then(message=>{
              console.log("message: " + JSON.stringify(message));
              res.send(message);
            }).catch(error =>{
                logger.error("El usuario no existe"+ error);
                messageService.createMessage(res, new CustomError(messages.ERROR_USER_PASSWORD));
                
            });
        }    
    });
}

async function logout(req, res){    
    // var token = req.headers.authorization.split(" ")[1];
    // if (token){
    //     userService.removeToken(token);
    // }    
    const url = {
        url: 'https://login.microsoftonline.com/'+ config.auth.tenant +'/oauth2/logout'
    }
    //await azure.executeRequest('GET', url, null, null);
    res.status(200).send(url);
}

function ip(req, res)
{
    //var ipAddress = req.socket.address().address; //REMOTE
    var ipAddress = req.header('x-forwarded-for') || req.connection.remoteAddress;//req.socket.localAddress; //LOCAL
    //var ipAddress = req.ip;

    logger.debug('la ip obtenida es'+ipAddress);   
    return res.status(200).send( { result: ipAddress});
}



function getMethodRequested(method) {
    var reqMethod = "";
    if (method.get == true) {
        reqMethod = 'get';
    } else if (method.post) {
        reqMethod = 'get';
    } else if (method.put) {
        reqMethod = 'put';
    } else if (method.delete) {
        reqMethod = 'delete';
    }
    return reqMethod;
}

function sendResponseNotAllowed(res, error = ERRORS.ERROR_CODE_NOT_AUTHORIZED) {
    logger.warn("No se puede acceder al recurso"+ JSON.stringify(error));
    return res.status(error.CODE).send({ auth: false, message: error.MSG });
}


/**
 * @param {*} dest Email del usuario que va a recibir la notificacion.
 * @param {*} study_id  Estudio que se le asigna.
 * @param {*} newStatus  Nuevo estado del estudio.
 */
function sendEmailNewUser(dest, username) {
    var email = config.new_user_notification_email;
    var subject = email.asunto.replace('{username}', username)
    var texto = email.message.replace('{username}', username) + email.footer;
    return sendEmail(dest, subject, texto);
  }

/**
 * @param {*} dest Email del usuario que va a recibir la notificacion.
 * @param {*} study_id  Estudio que se le asigna.
 * @param {*} newStatus  Nuevo estado del estudio.
 */
function sendEmailNewStudyAssign(dest, studyCode, estado) {
    var subject = config.email.asunto.replace('{codigo_estudio}', studyCode)
    var texto = config.email.message.replace('{codigo_estudio}', studyCode).replace('{estado}', estado) + config.email.footer;
    return sendEmail(users, subject, texto);
  }

  async function loginAzure(req, res) {
    azure.authenticateAzure(req.body.email, req.body.password)
        .then(token => {

            res.send(token);
            //res.send(JSON.stringify({ token: token}));
        })
        .catch(err => {
            if (err.type === "custom") {
                logger.error("err: " + err);
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, message.ERROR_USER_PASSWORD);
        });
    //azure.authenticateAzure2();
}


function login(req, res) {
    console.log('pasa');
    return authService
        .authenticate(req.body)
        .then(token => {
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, message.ERROR_USER_PASSWORD);
        });
}

function perfil(req, res) {
    return authService
        .perfil(req.body)
        .then(token => {
            console.log('el result del perfil es: '+token);
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, message.ERROR_USER_PASSWORD);
        });
}

function perfilUserAPP(req, res){
    console.log("CCCCC: "+req.params.email)
    return authService.perfilUser(req.params.email)
    .then(data => {
        res.status(200).send({ result: data})
    })
    .catch(err => {
        logger.error("err: " + err);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, message.ERROR_USER_PASSWORD);
    });
}

function pintarTrazasUser(req, res){
    console.log("DDDD: "+req.params.emailUsuario)
    var method = JSON.parse(JSON.stringify(req.route['methods']));
    var originalURL = req.route["path"];
    var methodReq = getMethodRequested(method);
    originalURL = originalURL.substring(0, originalURL.indexOf(':') - 1).replace("/api/v1", '');
    const protocol = req.protocol;
    const host = req.hostname;
    const url = req.originalUrl;
    console.log(protocol)
    console.log(host)
    console.log(url)
    //console.log(req)
    //originalURL = req.get('host')
    remoteIP=""
    user=req.params.emailUsuario
    obs= protocol+ " -- " + host + " --" + url
    registrarEjecuciones(remoteIP, originalURL, user,  obs);
    return authService.pintarLogs(req.params.emailUsuario)
    .then(data => {
        res.status(200).send({ result: data})
    })
    .catch(err => {
        logger.error("err: " + err);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, message.ERROR_USER_PASSWORD);
    });
}



function getCategory(req, res) {
    
    return authService.nameCategory(req.params.idCategorias)
        .then(data => {
            res.status(200).send( { result: data})
            //res.status(200).send(data);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error en la obtención de la categoría');
        });
}


function connectSalesForceSelects(req, res) {
    
    return authService.connectSalesForce(req.params.codigoprinex)
        .then(data => {
            console.log('el result nuevo ahora es: '+data);
            res.status(200).send( { result: data});
            
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al conectar con SalesForce: ' + err.message);
        });
}

function getcodigoUser(req, res) {
    console.log('req: ' + req);
    console.log('req.params: ' + req.params);
    console.log('req.params.dni: ' + req.params.dni);
    return authService.getcodigoUserSalesForce(req.params.dni)
        .then(data => {
            console.log('el result nuevo ahora es: '+data);
            res.status(200).send( { result: data});
            
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al conectar con SalesForce: ' + err.message);
        });
}

function getdatosUserSalesForce(req, res) {
    
    console.log("EL VALOR ES: "+req.params.id);
    return authService.getidDatosUserSalesForce(req.params.id)
        .then(data => {
            console.log('el result nuevo ahora es: '+data);
            res.status(200).send( { result: data});
            
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al conectar con SalesForce: ' + err.message);
        });
}



function getcodigoInmueble(req, res) {
    
    return authService.getcodigoInmuebleSalesForce(req.params.idUser)
        .then(data => {
            console.log('el result nuevo ahora es: '+data);
            res.status(200).send( { result: data});
            
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al conectar con SalesForce: ' + err.message);
        });
}

function getnamePromotion(req, res){
    return authService.getPromotionName(req.params.idPromotion)
        .then(data => {
            console.log('el result nuevo ahora es: '+data);
            res.status(200).send( { result: data});
            
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al conectar con SalesForce: ' + err.message);
        });
}

function getdatosUsuariosOportunidad(req, res) {
    return authService.getdatosUsuariosOportunidadSalesForce(req.params.oportunidad)
    .then(data => {
        console.log('el result nuevo ahora es: '+data);
        res.status(200).send( { result: data});
        
    })
    .catch(err => {
        logger.error("err: " + err);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error al obtener los datos de SalesForce');
    });
}

function getnamesUsuariosOportunidad(req, res) {
    return authService.getnamesUsuariosOportunidadSalesForce(req.params.codUsersOportunidad)
    .then(data => {
        console.log('el result nuevo ahora es: '+data);
        res.status(200).send( { result: data});
        
    })
    .catch(err => {
        logger.error("err: " + err);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error al obtener los datos de SalesForce');
    });
}

function getpromotionSalesForce(req, res){
    return authService.getValuePromotions()
    .then(data => {
        console.log('el result nuevo ahora es: '+data);
        res.status(200).send( { result: data});
        
    })
    .catch(err => {
        logger.error("err: " + err.type);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        else
        {
            console.log('EL ERROR ES: '+err);
        }
        return messageService.createMessage(res, 'Error al obtener los datos de SalesForce');
    });
}

function getIDoportunidadesganadas(req, res) {
    return authService.getoportunidadesganadasSalesForce(req.params.idPromocion)
    .then(data => {
        console.log('Las oportunidades ganadas son: '+data);
        res.status(200).send( { result: data});
        
    })
    .catch(err => {
        logger.error("err: " + err.message);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error al obtener los datos de SalesForce');
    });
}

function getoportunidadesbyuser(req, res) {
    return authService.getoportunidadesbyuserSalesForce(req.params.accountId)
    .then(data => {
        console.log('Las oportunidades del usuario son: '+data);
        res.status(200).send( { result: data});
        
    })
    .catch(err => {
        logger.error("err: " + err.message);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error al obtener los datos de SalesForce');
    });
}

function getencuestasOportunidad(req, res) {
    return authService.getencuestasoportunidadSalesForce(req.params.accountId, req.params.oportinidadId)
    .then(data => {
        console.log('Las encuestas de la oporunidad ' + req.params.oportinidadId + ' y el usuario ' + req.params.accountId + ' son: ' + data);
        res.status(200).send( { result: data});
        
    })
    .catch(err => {
        logger.error("err: " + err.message);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error al obtener los datos de SalesForce');
    });
}


function getAlloportunidadesganadasSalesForce(req, res){
    return authService.getIdgetAlloportunidadesganadasSalesForce()
    .then(data => {
        console.log('Las oportunidades ganadas son: '+data);
        res.status(200).send( { result: data});
        
    })
    .catch(err => {
        logger.error("err: " + err);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error al obtener los datos de SalesForce');
    });

}

function getEmailsoportunidadesganadas(req, res) {
    return authService.getEmailsSalesForce(req.params.accountId)
    .then(data => {
        console.log('Los Emails son: '+data);
        res.status(200).send( { result: data});
        
    })
    .catch(err => {
        logger.error("err: " + err);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error al obtener los datos de SalesForce');
    });
}

function postEmails(req, res){
    console.log(req.body);

        return authService
        .sendEmail(req.body)
        .then(token => {
            console.log('el result del perfil es: '+token);
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
        });
}

// function perfil(req, res) {
//     return authService
//         .perfil(req.body)
//         .then(token => {
//             console.log('el result del perfil es: '+token);
//             res.send(token);
//         })
//         .catch(err => {
//             logger.error("err: " + err);
//             if (err.type === "custom") {
//                 return res.status(401).send(err.message);
//             }
//             return messageService.createMessage(res, message.ERROR_USER_PASSWORD);
//         });
// }



function getEmailUsuariossoportunidadesganadas(req, res) {
    return authService.getEmailsUsuariosSalesForce(req.params.accountId)
    .then(data => {
        console.log('Los Emails son: '+data);
        res.status(200).send( { result: data});
        
    })
    .catch(err => {
        logger.error("err: " + err);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error al obtener los datos de SalesForce');
    });
}



function getIdoportunidadesganadas(req,res){
    return authService.getIdsSalesForce(req.params.id)
    .then(data => {
        console.log('Los ids son: '+data);
        res.status(200).send( { result: data});
        
    })
    .catch(err => {
        logger.error("err: " + err);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error al obtener los datos de SalesForce');
    });
}

function getsecundariocodigoInmueble(req, res) {
    
    return authService.getcodigoInmuebleSecundarioSalesForce(req.params.idUser)
        .then(data => {
            console.log('el result nuevo ahora es: '+data);
            res.status(200).send( { result: data});
            
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al conectar con SalesForce: ' + err.message);
        });
}

function getProductCode(req, res) {
    
    return authService.getProductCodeSalesForce(req.params.productcode)
        .then(data => {
            console.log('el result del product code ahora es: '+data);
            res.status(200).send( { result: data});
            
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al conectar con SalesForce: ' + err.message);
        });
}

function getdatosInmueble(req, res) {
    
    return authService.getdatosInmuebleSalesForce(req.params.idInmueble)
        .then(data => {
            console.log('el result nuevo ahora es: '+data);
            res.status(200).send( { result: data});
            
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al conectar con SalesForce: ' + err.message);
        });
}



function updatesalesforce(req, res) {
    console.log('el req es: '+req.body);
    return authService.updatesalesforceBBDD(req.body)
        .then(token => {
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            console.log("EL ERROR es"+err.message);
            return messageService.createMessage(res, 'Error al actualizar un usuario en salesforce');
        });

}

function updateoportunitysalesforce(req, res) {
    console.log('el req es: '+req.body);
    return authService.updateoportunitysalesforceBBDD(req.body)
        .then(token => {
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            console.log("EL ERROR es"+err.message);
            return messageService.createMessage(res, 'Error al actualizar un usuario en salesforce');
        });

}





function updatealtaportales(req, res){
    console.log('el req es: '+req.body);
    return authService.updateAltaPortalesBBDD(req.body)
        .then(token => {
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al actualizar un usuario en salesforce');
        });

}

function updatesalesforceAreaPrivada(req, res) {
    console.log('el req es: '+req.body);
    return authService.updatesalesforceAreaPrivadaBBDD(req.body)
        .then(token => {
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al actualizar un usuario en salesforce');
        });

}









function getAllCategories(req, res){
    return authService.allnameCategory()
        .then(data => {
            res.status(200).send( { result: data})
            //res.status(200).send(data);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error en la obtención del nombre de las categoría');
        });

}

function getAllUsers(req, res){
    return authService.allusersBBDD()
        .then(data => {
            res.status(200).send( { result: data})
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error en la obtención del nombre de los usuarios');
        });

}

function getaltaportales(req, res){
    return authService.altaportalesBBDD(req.params.idusuario)
        .then(data => {
            res.status(200).send( { result: data})
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error en la obtención del nombre de los usuarios');
        });
}

function getrutafiletalkdesk(req, res){

    var ruta = ""


    return authService.rutafiletalkdeskBBDD(req.params.idTalkdesk)
        .then(data => {
            console.log(data)
            res.status(200).send( { result: data})
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return res.status(401).send({ result: " "});
        });
}

function getmensajesspam(req, res){
    return authService.mensajesspamalta(req.params.fecha)
    .then(data => {
        res.status(200).send( { result: data})
    })
    .catch(err => {
        logger.error("err: " + err);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error en la obtención de los mensajes');
    });
}

function getaltaleads(req, res){
    return authService.altaLeadsBBDD(req.params.idportal)
        .then(data => {
            res.status(200).send( { result: data})
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error en la obtención del nombre de los usuarios');
        });
}

function getProducts(req, res) {
    
    return authService.nameProducts(req.params.idCategorias)
        .then(data => {
            res.status(200).send( { result: data})
            //res.status(200).send(data);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error en la obtención de los productos');
        });
}

function getAllProductos(req, res) {
    
    return authService.nameAllProducts()
        .then(data => {
            res.status(200).send( { result: data})
            //res.status(200).send(data);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error en la obtención de los productos');
        });
}



function addUsers(req, res) {
    return authService.addUsersBBDD(req.body)
        .then(token => {
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al dar de alta un usuario');
        });
}

function addUsersPortales(req, res){
    return authService.addUsersPortalesBBDD(req.body)
    .then(token => {
        res.send(token);
    })
    .catch(err => {
        logger.error("err: " + err);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error al dar de alta un usuario');
    });
}

function addProducts(req, res) {
    logger.debug("Dando de alta productos");
    return authService.addProductsBBDD(req.body)
        .then(token => {
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al dar de alta un producto nuevo');
        });
}


function addMailing(req, res) {
    logger.debug("Dando de alta Mailing");
    return authService.addMailingBBDD(req.body)
        .then(token => {
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al dar de alta un producto nuevo');
        });
}



function addChanges(req, res) {
    logger.debug("Dando de alta un cambio nuevo");
    return authService.addChangesBBDD(req.body)
        .then(token => {
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al dar de alta un cambio nuevo');
        });
}



function deleteProducts(req, res) {
    logger.debug("Eliminando productos de BBDD");
    return authService.deleteProductsBBDD(req.body)
        .then(token => {
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al eliminar un producto en bbdd');
        });
}



function updateUsers(req, res) {
    return authService.updateUsersBBDD(req.body)
        .then(token => {
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al actualizar de alta un usuario');
        });
}

function updateSpam(req, res){
    return authService.updateSpamBBDD(req.body)
    .then(token => {
        res.send(token);
    })
    .catch(err => {
        logger.error("err: " + err);
        if (err.type === "custom") {
            return res.status(401).send(err.message);
        }
        return messageService.createMessage(res, 'Error al actualizar los spam');
    });
}

function altaCurl(req, res){

    var valor = req.params.email;
    console.log(req.params.email);
    var exito = false;

    var options = {
          uri: 'https://aedassfdc.cysnet.net/ws/createUserPrivateArea',
          method: 'POST',
          headers: {'content-type' : 'application/json'}, 
          json: {
              email: valor
          }
        };
        
        request(options, function (error, response, body) {
            
          if (!error && response.statusCode == 200) {
            console.log(response.statusCode);
            console.log(body.exito); // Print the shortened url.
            exito = body.exito;
            console.log(exito);
            return res.send(exito);
          }
        
        });
        
          
}

function createLead(req, res){

    var json = req.params.json;
    // var arrayDeCadenas = json.split(',');
    // var primerelemento = arrayDeCadenas[0].split(':');
    // console.log("EL PRIMER ELEMENTO ES: "+primerelemento);
    console.log("EL JSON ES: "+json);

    // var email = req.params.email;
    // var name = req.params.name;
    // var idioma = req.params.idioma;
    // var comentario = req.params.comentario;
    // var ip = req.params.ip;
    // var navegador = req.params.navegador;
    // var url = req.params.url;
    // var idportal = req.params.idportal;
    // var parametersLeads = "'Email': '"+email+"','LastName': '"+name+"','FirstName': '','Tipo__c': 'Persona física','Status': 'Nuevo','Idioma__c': '"+idioma+"','Promocion__C': '"+idportal+"','Origen__C': 'a0T0Y000002qWjZUAU','Estado__c': 'Pendiente de firma','URL__c': '"+url+"','DireccionIP__c': '"+ip+"','Navegador__c': '"+navegador+"','Comentario':'"+comentario+"','valorAcceso':'Enero2020'";
    // var valoracceso = "Enero2020";
    // console.log(req.params.email);
    // console.log("Los parametros son: "+parametersLeads);
    // var exito = false;

    // var options = {
    //       uri: 'https://apisalesforce3.aedashomes.com/v1/query/leadSalesForce',
    //       method: 'POST',
    //       headers: {'content-type' : 'application/json'}, 
    //       json: {
    //           parameters: parametersLeads
    //       }
    //     };
        
    //     request(options, function (error, response, body) {
            
    //       if (!error && response.statusCode == 200) {
    //         console.log(response.statusCode);
    //         console.log(body.exito); // Print the shortened url.
    //         exito = body.exito;
    //         console.log(exito);
    //         return res.send(exito);
    //       }
        
    //     });
        
    return res.send("exito");   
}



function getUserToken(req, res){

    var valor = req.params.token;
    console.log(req.params.token);
    var exito = false;

    var options = {
          uri: 'https://api-uat.aedashomes.com/auth/me',
          method: 'GET',
          headers: {'content-type' : 'application/json',
          'X-Application-Name' : 'Gotham',
          'User-Agent': 'Mozilla/5.0',
          'Authorization': 'Bearer '+valor},
          json: {
              token: valor
          }
        };
        
        request(options, function (error, response, body) {
          console.log(response.statusCode) 
          if (!error && response.statusCode == 200) {
            console.log(response);
            //console.log(body.exito); // Print the shortened url.
            // exito = body.exito;
            // console.log(exito);
            return res.send(response);
          }
        
        });
        
          
}

function getUsersSalesForce(req, res){
    logger.debug("Obteniendo usuarios SalesForce");
    var fechainicial;
    var fechafinal;
    var json = req.params.json;
    var arrayDeCadenas = json.split(',');
    var primerelemento = arrayDeCadenas[0].split(':');
    fechainicial = primerelemento[1];

    var segundoelemento = arrayDeCadenas[1].split(':');
    fechafinalcaracter = segundoelemento[1];
    var fechainicial2 = fechainicial.replace(/['"]+/g, '');
    console.log(fechainicial2);
    var fechafinal = fechafinalcaracter.replace("}", "");
    var fechafinal2 = fechafinal.replace(/['"]+/g, '');
    console.log(fechafinal2);

    return authService.getUserSALESFORCE(fechainicial2,fechafinal2)
        .then(token => {
            console.log(token);
            res.send(token);
        })
        .catch(err => {
            logger.error("err: " + err);
            if (err.type === "custom") {
                return res.status(401).send(err.message);
            }
            return messageService.createMessage(res, 'Error al obtener usuarios SalesForce');
        });
        
          
}

function checkEmail(req, res){
    var valor = req.params.email;
    console.log(req.params.email);
    var exito = false;

    var options = {
          uri: 'https://smtp3.aedashomes.com/hello/' + valor,
          method: 'GET',
          headers: {'content-type' : 'application/json'}, 
        };
        
        request(options, function (error, response, body) {
          console.log('EL RESULT ES: ' +response.statusCode)
            
          if (!error && response.statusCode == 200) {
            console.log(response.statusCode);
            var resultadofinal = JSON.parse(body)
            console.log(body); // Print the shortened url.
            console.log(resultadofinal);
            exito = resultadofinal['Resultado'];
            console.log(exito);
            return res.send(exito);
          }
        
        });
}

function downloadrutafiletalkdesk(req, res){
    var ruta = "";
    var valorRuta = req.params.json;
    var valorRutaFinal = valorRuta.replace(/_/g, '/');//"python/2018/12/20/REd3dfd61f08c5718fa3efa69ed0907c7c.mp3"
    console.log("La ruta final2 del fichero es: "+valorRutaFinal)

    const ACCESS_KEY_ID = Configbbdd.bucket.ACCESS_KEY_ID;
    const SECRET_ACCESS_KEY = Configbbdd.bucket.SECRET_ACCESS_KEY; 
    const BUCKET_NAME = Configbbdd.bucket.BUCKET_NAME;

    var valorFichero = valorRutaFinal.split('/')
    var titleFile = valorFichero[valorFichero.length - 1]

    
    var s3 = new AWS.S3({
        accessKeyId: ACCESS_KEY_ID,
        secretAccessKey: SECRET_ACCESS_KEY,
    });
    console.log("RUTA FINAL:"+valorRutaFinal)
    var params = {Bucket: BUCKET_NAME, Key: valorRutaFinal};

    s3.getObject(params, function(err, data) {
        if (err)
        {
            console.log(err, err.stack); // an error occurred
            return res.status(401).send(err.message);
        }
        else
        {

            var url2 =  Configbbdd.bucket.URLFINAL;//"/var/webs/gotham.aedashomes.com/promociones/promociones/src/assets/mp3/"
            fs.writeFileSync(url2+titleFile, data.Body)
            ruta = titleFile;
            console.log('file downloaded successfully')
            data = ruta;
            return res.status(200).send( [{ result: data}])
            
        } 
      });


      
      

    /*

    const downloadFromS3 = async () => {
        const params = {
          Bucket: BUCKET_NAME,
          Key: KEY,
        }
      
        const { Body } = await s3.getObject(params).promise()
        await fs.writeFile('\\..\\test.mp3', Body)
        console.log("TEST")
    }

    

    //res.attachment(valorRuta);
    //var file2 = require('fs').createWriteStream('\\..\\test.mp3');
    //s3.getObject(params).
    //on('httpData', function(chunk) { file2.write(chunk); }).
    //on('httpDone', function() { file2.end(); }).
    //send();
    //console.log('YAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
    
    /*
    
    var file = s3.getObject({
      Bucket: "taldesk-aedas",
      Key: valorRuta
      }).createReadStream()
        .on("error", error => {
            console.log("ERROR")
        });
     file.pipe(file2);
   

    */

    
    /*

    const downloadFile = (valorRuta, BUCKET_NAME, ACCESS_KEY_ID) => {
    const params = {
        Bucket: BUCKET_NAME,
        Key: valorRuta
    };
    s3.getObject(params, (err, data) => {
        if (err) console.error(err);
        fs.writeFileSync(valorRuta, data.Body.toString());
        console.log(`${valorRuta} has been created!`);
        data = true;
        res.status(200).send( { result: data})

    });
    };

    downloadFile(valorRuta, BUCKET_NAME, ACCESS_KEY_ID);
    

    

    var s3 = new AWS.S3({
        accessKeyId: "AKIAIPT33KQMQA7Z2RRA",
        secretAccessKey: "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"
    })

    var params = {
        Bucket: BUCKET_NAME,
        Key: valorRuta
       
    }


    res.attachment(valorRuta);
    var fileStream = s3.getObject(params).createReadStream();
    fileStream.pipe(res);

    /*s3.getObject(params, function(err, data) {
        if (err) {
            console.log(err)
            res.status(401).send(err.message);
        }
        else
        {
            fs.writeFileSync(valorRuta, data.Body)
            console.log('file downloaded successfully')
            data = true;
            res.status(200).send( { result: data})
        }
   
        
    
})

*/
     

}







  

module.exports = {
    register,
    checkAuth,
    authUser,
    getTokenAzure,
    logout,
    ip,
    loginAzure,
    login,
    perfil,
    perfilUserAPP,
    pintarTrazasUser,
    getCategory,
    getAllCategories,
    getProducts,
    getAllUsers,
    getaltaportales,
    getrutafiletalkdesk,
    downloadrutafiletalkdesk,
    getmensajesspam,
    getaltaleads,
    addUsers,
    addUsersPortales,
    updateUsers,
    updateSpam,
    getAllProductos,
    addProducts,
    addMailing,
    addChanges,
    deleteProducts,
    connectSalesForceSelects, 
    updatesalesforce,
    updateoportunitysalesforce,
    updatealtaportales,
    updatesalesforceAreaPrivada,
    getcodigoUser,
    getdatosUserSalesForce,
    getcodigoInmueble,
    getnamePromotion,
    getdatosInmueble,
    getProductCode,
    getsecundariocodigoInmueble,
    getdatosUsuariosOportunidad,
    getnamesUsuariosOportunidad,
    getpromotionSalesForce,
    getIDoportunidadesganadas,
    getoportunidadesbyuser,
    getencuestasOportunidad,
    getEmailsoportunidadesganadas,
    postEmails,
    getEmailUsuariossoportunidadesganadas,
    getIdoportunidadesganadas,
    getAlloportunidadesganadasSalesForce,
    altaCurl,
    createLead,
    getUserToken,
    getUsersSalesForce,
    checkEmail
};
