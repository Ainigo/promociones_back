const sequelize = require("./../db/db");
const azure = require("../services/authAzure");
const userService = require('../services/user');
const masterService = require('../services/master');
const studyService = require('../services/study');
const messageService = require("../services/messagesManager");
const logger = require('../config/logger');
const message = require('../config/messages');


function getUser(req, res) {
    return userService.getUser(req.params.email)
        .then(data => {
            logger.debug("Datos enviados: " + JSON.stringify(data));
            res.status(200).send(data);
        });
};


function getUserByToken(req, res) {
    return userService.getUserByToken(req.params.getUserByToken)
        .then(data => {
            logger.debug("Datos enviados: " + JSON.stringify(data));
            res.status(200).send(data);
        });
};


function getUsers(req, res) {
    return userService.getUsers().then(data => {
        res.status(200).send(data);
    });
};

function getUsersStudies(req, res) {
    var currentStudies = [];
    userService.getUserWithToken(req.params.email).then(user => {
        studyService.getStudiesByUserId(user.user_id).then(userStudies => {
            userStudies.filter(value => {
                if (currentStudies.indexOf(value.study_id) < 0){
                    currentStudies.push(value.study_id);
                }                
            });
            studyService.getStudiesExcept(currentStudies).then(freeStudies => {
                res.status(200).send({ userStudies, freeStudies });
            });
        });
    });
}

async function addUser(req, res) {

    var res = await azure.exitsUserOnAzure(email);
    messageService.throwIfNecessaryMessageError(res, message.ERROR_USUARIO_NO_VALIDO);

    userService.addUser(req.body)
        .then(data => {
            logger.debug("Se ha insertado el siguiente usuario¡: " + JSON.stringify(data));
            messageService.createMessage(res, message.INFO_OK);
        })
        .catch(err => {
            logger.error("[UserController - addUser] " + JSON.stringify(err));
            messageService.createMessage(res, message.ERROR_GENERICO_INSERT);
        });
}



async function updateUser(req, res) {

    var user_id = req.params.user_id;

    var updated = await userService.updateUser(req.body, user_id)
    if (updated == 0) {
        messageService.createMessage(res, message.ERROR_UPDATE_NO_ROWS);
    } else {
        logger.debug("Se ha actualizado el usuario");
        messageService.createMessage(res, message.INFO_OK);
    }
}

function assignUserStudies(req, res) {
    userService.assignStudies(res, req.params.user_id, req.body.studies);    
}


function unAssignUserStudies(req, res) {
    userService.unAssignStudies(res, req.params.user_id, req.params.study_id)
        .then(data => {
            if (data == 0) {
                messageService.createMessage(res, message.ERROR_DELETE_NO_ROWS);
            } else {
                messageService.createMessage(res, message.INFO_OK);
            }
        }).catch(err => {
            logger.error("[UserController - assignUserStudies] " + JSON.stringify(err));
            messageService.createMessage(res, message.ERROR_DELETE_NO_ROWS);
        });
}


async function deleteUser(req, res) {
    await masterService.callStoreProcedure(masterService.storeProcedures.PROCEDURE_DELETE_USER, req.params.user_id);
    return messageService.createMessage(res, message.INFO_OK);
}

module.exports = {
    getUsers,
    getUsersStudies,
    getUser,
    getUserByToken,
    addUser,
    updateUser,
    assignUserStudies,
    unAssignUserStudies,
    deleteUser
}