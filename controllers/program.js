const sequelize = require("../db/db");
const programService = require("../services/program");
const promotionService = require("../services/promotions");
const studyService = require("../services/study");
const programModel = require("../models/promotions").Program;
const messageService = require("../services/messagesManager");
const message = require("../config/messages");
const logger = require("../config/logger");
const studyController = require("../controllers/study");

async function getProgram(req, res){
    var versionId;
    var notequal = false;

    studyController.checkValidVersion(req,res);

    if (req.query.version_id == null && req.query.version_number == null) {
        return messageService.createMessage(res, message.ERROR_VERSION_NO_INFORMADA);
    } else if (req.query.version_id != null && req.query.version_number != null) {
        await studyService.compareVersions(req.query.version_id, req.query.version_number)
            .then(data => {
                if (data == "") {
                    notequal = true;
                }
            });
    }
    if (notequal) {
        return messageService.createMessage(res, message.ERROR_VERSION_NOTEQUAL);
    }
    if (req.query.version_id == null) {
        await studyService.getVersionStudyByPromotionId(req.params.promotion_id)
        .then(data => {
            versionId = data[0].version_id
        })
        .catch(err => {
            logger.error("[ProgramController - getVersionStudyByPromotionIdError] " + err);
            return messageService.createMessage(res, message.ERROR_RECOGER_GET);
        });
    
    } else {
        versionId = req.query.version_id;
    }
    return programService.getProgram(req.params.promotion_id, versionId)
    .then(data => {
        logger.debug("El programa es: " + JSON.stringify(data));
        var result = {promotion_id: req.params.promotion_id, version_id: versionId, program: data};
        res.send(result);
    })
    .catch(err => {
        logger.error("[ProgramController - GetProgramError] " + JSON.stringify(err));
        return messageService.createMessage(res, message.ERROR_RECOGER_GET);
    });
}

async function insertOrUpdatePrograms(req, res){
    var programs = programModel.build(req.body);
    var version_id = null;
    var transaction = null;
    
    try {
        var versionId = await studyService.getVersionStudyByPromotionId(req.params.promotion_id);
        logger.debug("Version_id: " + JSON.stringify(versionId[0].version_id));

        version_id = versionId[0].version_id;
       
        await checkPrograms(programs, req.params.promotion_id, version_id);
        
        transaction = await sequelize.transaction();
        
        for (var i = 0; i < programs.length; i++){
            programs[i].promotion_id = req.params.promotion_id;
            await programService.insertOrUpdatePrograms(programs[i],
                version_id,
                transaction);
        }
        await transaction.commit();

        return messageService.createMessage(res, message.INFO_OK);

    } catch (err){
        logger.error("[ProgramController - InsertOrUpdatePrograms]: " + JSON.stringify(err));
        if (transaction){
            await transaction.rollback();
        }
        if (err.type == 'custom'){
            return messageService.createMessage(res, message.ERROR_DATOS_INCONSISTENTES);
        }
        else {
            return messageService.createMessage(res, message.ERROR_GENERICO_UPDATE);
        }
    }
}

async function checkPrograms(programs, promotion_id, version_id){

    var programsId = [];
    var rooms = [];
    programs.filter(program => {
        if (program.program_id != null){
            programsId.push(program.program_id);
        } else {
            rooms.push(program.rooms);
        }
    })

    if (programsId.length > 0){
        var programRows = await programService.getNumberOfPrograms(programsId, promotion_id, version_id);
        messageService.throwIfNecessaryMessageError(programRows == programsId.length, message.ERROR_DATOS_INCONSISTENTES);        
    }
    if (rooms.length > 0){
        var roomsRows = await programService.getNumberOfRooms(rooms, promotion_id, version_id);
        messageService.throwIfNecessaryMessageError(roomsRows, message.ERROR_DATOS_INCONSISTENTES);
    }
}

module.exports = {
    getProgram,
    insertOrUpdatePrograms
}