const sequelize = require("../db/db");
const promotionsService = require("../services/promotions");
const masterService = require("../services/master");
const studiesService = require("../services/study");
const logger = require("../config/logger");
const messageService = require("../services/messagesManager");
const message = require("../config/messages");
const studyController = require("../controllers/study");

const promotionModel = require("../models/studies").Promotions;
const promotionVersionModel = require("../models/promotions").PromotionVersions;
const sharepoint = require("./../controllers/sharepoint");

async function getPromotions(req,res) {
    var version_id = null;
    try {        
        await messageService.checkParamInformed(req.query.study_id);

        await studyController.checkValidVersion(req, res);
        version_id = await studyController.getVersionIdForStudyId(req);

        return promotionsService.getPromotionByStudyId(req.query.study_id, version_id)
            .then(data => {
                res.status(200).send(data);
            })
            .catch(err => {
                logger.error("[PromotionController - GetPromotionError] " + JSON.stringify(err));
                return messageService.throwMessageError(message.ERROR_PARAMETROS_INCORRECTOS);
            });        
    } catch (error) {      
        messageService.buildMessageError(res, error);
    }    
};


async function getPromotion(req, res) {
    var version_id;
    try {
        await studyController.checkValidVersion(req, res);
        
        if (req.query.version_id == null) {
            await studiesService.getVersionStudyByPromotionId(req.params.promotion_id, req.query.version_number)
            .then(data => {
                 version_id = data[0].version_id;
            })
            .catch(err => {
                logger.error("[PromotionsController - getVersionStudyByPromotionIdError]: " + err);
                return messageService.createMessage(res, message.ERROR_RECOGER_GET);
            });
        } else {
            version_id = req.query.version_id;
        }

        return promotionsService.getPromotion(req.params.promotion_id, version_id)
            .then(data => {
                res.status(200).send(data);
            })
            .catch(err => {
                logger.error("[PromotionController - GetPromotionError] " + JSON.stringify(err));
                return messageService.createMessage(res, message.ERROR_RECOGER_GET);
            });        

    } catch (error) {
        messageService.buildMessageError(res, error);
    }    
    
};


async function createPromotion(req, res) {
    var promotion = promotionModel.build(req.body);
    var transaction = null;
    try {
        var defaultVersion = await studiesService.getDefaultVersion(promotion.study_id);

        transaction = await sequelize.transaction();
        await promotionsService.createPromotion(promotion, transaction);

        var promotionVersion = promotionVersionModel.build(req.body);
        promotionVersion.promotion_id = promotion.promotion_id;
        promotionVersion.version_id = defaultVersion.version_id;

        await promotionsService.createPromotionVersion(promotionVersion, transaction);

        // Creamos la carpeta de la promoción en sharepoint
        await sharepoint.createFoldersNewPromotion(promotion.study_id, promotion.promotion_id);

        await transaction.commit();
        
        return messageService.createMessage(res, message.INFO_OK);

    } catch (err){
        logger.error("[PromotionsController - CreatePromotion] " + JSON.stringify(err));
        if (transaction){
            await transaction.rollback();
        }
        if (err.type == 'custom'){
            return messageService.createMessage(res, message.ERROR_DATOS_INCONSISTENTES);
        }
        else {
            return messageService.createMessage(res, message.ERROR_GENERICO_INSERT);
        }
    }
};


async function updatePromotion(req, res){ 
    var promotion = req.body;
    var transaction = null;
    var msg = null;
    var messageToReturn = message.INFO_OK;
    
    try {
        transaction = await sequelize.transaction();
        if (promotion.work_status_id != undefined || promotion.delivery_date != undefined) {
            var version = await studiesService.getVersionStudyByPromotionId(req.params.promotion_id);
            var defaultVersion = version[0].version_id;
            await promotionsService.updatePromotionVersion(
                {work_status_id: promotion.work_status_id, work_status_id:promotion.work_status_id}, 
                req.params.promotion_id, defaultVersion, transaction);
            
            // Si el valor de status_id ha cambiado, hay que comprobar si el usuario ha seleccionado el valor Promocion vendida. 
            // En ese caso, hay que poner a 0 los programas y marcar las viviendas como vendidas.
            if (promotion.work_status_id!= null && promotion.work_status_id!= undefined){
                let currentWorkStatus = await masterService.getCntValue(promotion.work_status_id);
                if (currentWorkStatus.value == 'promocion_vendida'){
                    await promotionsService.updatePromotionSold(transaction, req.params.promotion_id, defaultVersion);
                    messageToReturn = message.INFO_OK_PROMO_VENDIDA_OK;
                }
            }
        }        
        await promotionsService.updatePromotion(promotion, req.params.promotion_id, transaction);

        await transaction.commit();
        msg = messageService.createMessage(res, messageToReturn);        
    } catch (err){
        logger.error("[PromotionController - UpdatePromotion] " + JSON.stringify(err));
        if (transaction){  
            transaction.rollback();
        }
        if (err.type == 'custom'){
            msg = messageService.createMessage(res, message.ERROR_DATOS_INCONSISTENTES);
        }
        else {
            msg = messageService.createMessage(res, message.ERROR_GENERICO_UPDATE);
        }
    }
    return msg;
};

async function deletePromotion(req, res) {
    await masterService.callStoreProcedure(masterService.storeProcedures.PROCEDURE_DELETE_PROMOTION, req.params.promotion_id);
    return messageService.createMessage(res, message.INFO_OK);
}

module.exports = {
    getPromotion,
    getPromotions,    
    createPromotion,
    updatePromotion,
    deletePromotion
}